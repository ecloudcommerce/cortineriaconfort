<?php

//    extends Bl_Nps_Model_Payments extends Mage_Core_Model_Abstract

class Bl_Promopsp_Model_Payments {

    const VISA_PRODUCT_CODE = '14';
    const AMEX_PRODUCT_CODE = '1';

    public function getBanks() {

        $bancos = Mage::getModel('crudbcc/bancos')->getCollection();
        $result = array();

        foreach ($bancos as $banco) {
            $key = $banco->getId();
            $result[$key]['id'] = $banco->getId();
            $result[$key]['name'] = $banco->getNombre();
            $result[$key]['credit_cards'] = array();
            $tarjetas = $banco->getTarjetas();

            if ($tarjetas) {
                foreach ($tarjetas as $tarjeta) {
                    $tarjeta = Mage::getModel('crudbcc/tarjetas')->load($tarjeta);
                    // Filtro pa las que no tienen ni nombre ni Cuotas Cargadas
                    // Ver que onda si no se cargaron cuotas pero si promos... supongo q no muestra una chota
//                    var_dump($tarjeta->getCuotas()); die();

                    if ($tarjeta->getNombre() != "" && $tarjeta->getData("cuotas")) {
                        $arrTarjeta['id'] = $tarjeta->getId();
                        $arrTarjeta['value'] = $tarjeta->getId();
                        $arrTarjeta['name'] = $tarjeta->getNombre();
                        $arrTarjeta['label'] = $tarjeta->getNombre();
                        $arrTarjeta['code_nps'] = $tarjeta->getCodigoNps();
                        $result[$key]['credit_cards'][$tarjeta->getId()] = $arrTarjeta;
                    }
                }
            }
            // Limpieza de Bancos sin Tarjetas
            if (!count($result[$key]['credit_cards']) > 0) {
                unset($result[$key]);
            }
        }
        return $result;
    }

    public function getBankCreditCards($bankId) {
        $banco = Mage::getModel('crudbcc/bancos')->load($bankId);
        $tarjetas = $banco->getTarjetas();
        $result = array();

        foreach ($tarjetas as $key => $tarjeta) {
            $tarjeta = Mage::getModel('crudbcc/tarjetas')->load($tarjeta);
            if (!$tarjeta->getCuotas()) {
                
            }
            $result[$key]['id'] = $tarjeta->getId();
            $result[$key]['name'] = $tarjeta->getNombre();
            $result[$key]['code_nps'] = $tarjeta->getCodigoInterno();
        }

        return $result;
    }

    public function getCreditCardBy($field, $value) {
        switch ($field) {
            case 'code_nps':
                $field = 'codigo_interno';
                break;
            case 'id':
                $field = 'id';
                break;
            case 'name':
                $field = 'nombre';
                break;
            default:
                return null;
        }

        $tarjetas = Mage::getModel('crudbcc/tarjetas')->getCollection();
        $tarjetas->getSelect()->where($field . ' = ?', $value);

        $result = null;
        foreach ($tarjetas as $tarjeta) {
            $result['id'] = $tarjeta->getId();
            $result['name'] = $tarjeta->getNombre();
            $result['code_nps'] = $tarjeta->getCodigoInterno();
        }
        return $result;
    }

    public function getFees($bankId, $ccId, $productId = null, $showAhora = false) {
        
        $result     = array();
        $banco      = Mage::getModel('crudbcc/bancos')->load($bankId);
        $tarjetas   = $banco->getTarjetas();
        $notprom    = 1;

        // Validacion Combinación Banco/Tarjeta
        if (!in_array($ccId, $tarjetas)) {
            throw new Exception("Combinacion Banco/Tarjeta erronea");
        }
        
        $tarjeta = Mage::getModel('crudbcc/tarjetas')->load($ccId);

        if (!$tarjeta->getData("cuotas")) {
            return array();
        }

        /* Busco las cuotas primero en Banco/Tarjeta */
        foreach ($tarjeta->getData("cuotas") as $key => $cuota) {
            $ncuotas                        = $cuota['n_cuota'];
            $result[$ncuotas]['id']         = $key;
            $result[$ncuotas]['payments']   = $ncuotas;
            $result[$ncuotas]['coef']       = $cuota['coeficiente'];
            $result[$ncuotas]['cft']        = $cuota['cft'];
            $result[$ncuotas]['tea']        = $cuota['tea'];
            $result[$ncuotas]['tna']        = $cuota['tna'];
            $result[$ncuotas]['tem']        = $cuota['tem'];
            $result[$ncuotas]['sin_interes']= $cuota['sin_interes'];
            $result[$ncuotas]['nps_code']   = $tarjeta['codigo_nps'];
        }

        //Mage::log('Cuotas Obtenidas de Banco/Tarjeta: '.print_r($result,true),null,"cuotas.log");

        if (!$tarjeta instanceof Mage_Core_Model_Abstract) {
            $tarjeta = Mage::getModel('crudbcc/tarjetas')->load($ccId);
        }
        
        // Se instancia un Quote Falso para Validar las Promociones
        $quoteAddress = Mage::getModel('sales/quote_address')
                ->setBanco($banco->getNombre())
                ->setTarjeta($tarjeta->getNombre());

        $quote = Mage::getModel('sales/quote');
        $quoteAddress->setQuote($quote);
        
        $rules = Mage::getModel('salesrule/rule')->getCollection();
        $rules = $this->_setActiveFilter($rules);

        foreach ($rules as $rule) {            
            $rule->afterLoad();
        
            /* START FIX RULE BY WEBSITE*/
            $websiteId = Mage::app()->getStore(true)->getWebsiteId();

            //Obtengo el nro de cuotas de esta promocion (rule)
            $conditions = $rule->getConditions()->asArray();
            foreach ($conditions["conditions"] as $key => $value) {
                if($value["attribute"] == "cuotas") {
                    $nroCuotas = explode(",", $value["value"]);
                    //Seteo las cuotas que tiene esta promo activa.
                    if(!empty($nroCuotas)) {
                        $quoteAddress->setCuotas($nroCuotas);
                    }
                    break;
                }
            }

            
            if (!in_array($websiteId, $rule->getWebsiteIds())) {
                $rule->setIsValidForAddress($quoteAddress, false);
                continue;
            }
            /* END FIX RULE BY WEBSITE*/

            $xcuotas = $rule->getData('hasta_x_cuotas');
            //Mage::log('Cuotas sin interes obtenidas de la promo: '.$xcuotas,null,"cuotas.log");

            /*if($rule->getConditions()->validate($quoteAddress) && $xcuotas == null){
                
                Mage::log('Entra por el if de xcuotas igual a NULL',null,"cuotas.log");

                foreach ($tarjeta->getData("cuotas") as $key => $cuota) {
                    $ncuotas                        = $cuota['n_cuota'];
                    $result[$ncuotas]['id']         = $key;
                    $result[$ncuotas]['payments']   = $ncuotas;
                    $result[$ncuotas]['coef']       = $cuota['coeficiente'];
                    $result[$ncuotas]['cft']        = $cuota['cft'];
                    $result[$ncuotas]['tea']        = $cuota['tea'];
                    $result[$ncuotas]['tna']        = $cuota['tna'];
                    $result[$ncuotas]['tem']        = $cuota['tem'];
                    $result[$ncuotas]['nps_code']   = $tarjeta['codigo_nps'];
                }
                
                $notprom = 0;
            }*/

            $promoDay = true;
            
            if(!(bool)$rule->getIsEveryday()) {
                $days = explode(',', $rule->getWeekdays());
                $current_day = date("w", Mage::getModel('core/date')->timestamp(time()));
                $promoDay = in_array($current_day, $days);
            }


            if ($rule->getConditions()->validate($quoteAddress) && $promoDay) {

                $cuotas = unserialize($rule->getData('cuotas'));

                //Mage::log('Cuotas obtenidas de la promo: '.print_r($cuotas,true),null,"cuotas.log");

                foreach ($cuotas as $cuota) {

                    $n_cuota = $cuota['n_cuota'];

                    /* Si la cuota no existe en tarjetas, agrego a partir de la cuota de la promo */
                    if (!isset($result[$n_cuota])) {
                        $result[$n_cuota]['id'] = "x-$n_cuota";
                        $result[$n_cuota]['payments'] = $n_cuota;
                        $result[$n_cuota]['nps_code'] = $tarjeta['codigo_nps'];
                    }

                    // Sobreescribo los valores de coeficiente, cft, tea, tna y tem
                    // cargados en la tarjeta con los cargados en la promo
                    $result[$n_cuota]['coef']           = $cuota['coeficiente'];
                    $result[$n_cuota]['cft']            = $cuota['cft'];
                    $result[$n_cuota]['tea']            = $cuota['tea'];
                    $result[$n_cuota]['tna']            = $cuota['tna'];
                    $result[$n_cuota]['tem']            = $cuota['tem'];
                    $result[$n_cuota]['sin_interes']    = $cuota['sin_interes'];

                    // Promo Data
                    $result[$n_cuota]['promo']['id']            = $rule->getId();
                    $result[$n_cuota]['promo']['name']          = $rule->getName();
                    $result[$n_cuota]['promo']['label']         = $rule->getLabel();
                    $result[$n_cuota]['promo']['promo_code']    = $rule->getPromoCode();
                    $result[$n_cuota]['promo']['merchant_id']   = $rule->getMerchantId();
                    $result[$n_cuota]['promo']['description']   = $rule->getDescription();
                    $result[$n_cuota]['promo']['bank_discount'] = $rule->getDescbco();
                    $result[$n_cuota]['promo']['max_refund']    = $rule->getTope();
                    $result[$n_cuota]['promo']['show_refund']   = $rule->getShowRefund();
                    // Rules
                    $result[$n_cuota]['rules']['is_everyday']   = (bool)$rule->getIsEveryday();
                    $result[$n_cuota]['rules']['weekdays']      = $rule->getWeekdays();
                    $result[$n_cuota]['rules']['discount']      = $rule->getDiscountAmount();
                    $result[$n_cuota]['rules']['action']        = $rule->getSimpleAction();

                }
                
                $notprom = 0; 
                
                if((bool)$rule->getData('ahora_12')){    
                    
                    $result[7]['id']       = "ahora12";
                    $result[7]['payments'] = 12;                    
                    $result[7]['nps_code'] = $tarjeta['codigo_nps'];

                }

                if((bool)$rule->getData('ahora_18')){    

                    $result[8]['id']       = "ahora18";
                    $result[8]['payments'] = 18;                    
                    $result[8]['nps_code'] = $tarjeta['codigo_nps'];
                    
                }

            }
            
            if($rule->getConditions()->validate($quoteAddress) && !$promoDay){
                $notprom = 1;
            }

            
        }

        //Mage::log("notprom: ".$notprom,null,"cuotas.log");          

        /* Ordeno el array */
        sort($result);   

        //Mage::log("result de getFees en Promopsp: ".print_r($result,true),null,"cuotas.log");

        return $result;
    }

    /**
     * ... Limpiamos posibles cagadas de la interfaz teclado-silla
     * @param string $xcuotas
     * @return array
     */
    public function getCleanCuotas($xcuotas) {
        $result = array();
        $cuotas = explode(",", $xcuotas);
        foreach ($cuotas as $cuota) {
            if (trim($cuota) != '' && strval(intval($cuota)) === $cuota) {
                $result[] = $cuota;
            }
        }
        return $result;
    }

    /**
     *
     * @param <type> $bankId
     * @param <type> $ccId
     * @param <type> $payments
     * @param <type> $productId
     * @return <type>
     */
    public function getFee($bankId, $ccId, $payments, $productId = null) {          
        $fees = $this->getFees($bankId, $ccId, $productId, true);
        
        foreach ($fees as $fee) {             
            if ($fee['id'] === "ahora12" && $payments === "7"){
                $fee['payments'] = 7;
                return $fee;
            }
            if ($fee['id'] === "ahora18" && $payments === "8"){
                $fee['payments'] = 8;
                return $fee;
            }
            if ($fee['payments'] == $payments)
                return $fee;
        }
        return (isset($fees[$payments])) ? $fees[$payments] : null;
    }

    /**
     *
     * @param <type> $id
     * @return <type>
     * 
     * @todo agregarle los parametros Nuevos
     */
    public function getPromo($id) {
        $rule = Mage::getModel('salesrule/rule')->load($id);
        return ($rule) ? $rule->getData() : null;
    }

    protected function _setActiveFilter($collection) {
        $now = date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
        
        $customerId = $this->getCustomerGroupId();        
        
        $collection->getSelect()
            ->join(array('t' => 'salesrule_customer_group'),
                'main_table.rule_id = t.rule_id', array('t.customer_group_id'))
            ->where("main_table.is_active=1")
            ->where("t.customer_group_id = '$customerId'")
            ->where("(main_table.from_date is null or 
                main_table.from_date <= '$now') and 
                (main_table.to_date is null or main_table.to_date > '$now')");

        return $collection;
    }

    protected function getCustomerGroupId(){
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
          // Get group Id
          return Mage::getSingleton('customer/session')->getCustomerGroupId();
        }
        return 0;
    } 

    /**
     * Devuelve el helper Data o el hijito que le pidas
     *
     * @return mixed Bl_Promopsp_Helper_Data
     */
    protected function _helper($key = null) {
        if ($key) {
            return Mage::helper("promopsp/$key");
        }
        return Mage::helper("promopsp");
    }

    /**
     * Eso... loguea
     * 
     * @param string $data
     */
    public function log($data) {
        Mage::log($data, 7, "bl_promopsp.log");
    }
}
