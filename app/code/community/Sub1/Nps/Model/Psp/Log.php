<?php

class Bl_Nps_Model_Psp_Log extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('nps/psp_log');
    }

//    public function getParameters()
//    {
//        $this->_unserializeParameters();
//        return $this->getParameters();
//    }

    protected function _unserializeParameters() {
//        if($this->hasData('parameters')) {
//            if (!$this->getParameters() instanceof stdClass
//                    && !is_array($this->getParameters())) {
//                $this->setData('parameters', unserialize($this->getParameters()));
//            }
//        }
        return;
    }

    public function saveCall($type, $description, $data) {
        try {
            if ($data instanceof stdClass) {
                $this
                        ->setOrderIncrementId((isset($data->psp_MerchOrderId)) ? $data->psp_MerchOrderId : '')
                        ->setReferenceId((isset($data->psp_MerchTxRef)) ? $data->psp_MerchTxRef : '')
                        ->setDate((isset($data->psp_PosDateTime)) ? $data->psp_PosDateTime : date("Y-m-d"))
                        ->setParameters(serialize($data))
                        ->setDescription($description)
                        ->setType($type)
                        ->save();
            } else {
                $this
                        ->setOrderIncrementId((isset($data['psp_MerchOrderId'])) ? $data['psp_MerchOrderId'] : '')
                        ->setReferenceId((isset($data['psp_MerchTxRef'])) ? $data['psp_MerchTxRef'] : '')
                        ->setDescription($description)
                        ->setParameters(serialize($data))
                        ->setDate($data['psp_PosDateTime'])
                        ->setType($type)
                        ->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
            return;
        }
    }

    public function getMethod() {
        return Mage::getModel("nps/nps");
    }

}