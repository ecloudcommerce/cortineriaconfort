<?php
/** @var $this Hd_Bccp_Model_Resource_Setup */

$this->_initSetup()
    ->_createModuleTables()
    ->_endSetup();
