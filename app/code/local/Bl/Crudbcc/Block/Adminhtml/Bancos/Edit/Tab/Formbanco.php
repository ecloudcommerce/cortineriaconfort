<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Bancos_Edit_Tab_Formbanco extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset_banco = $form->addFieldset('crudbcc_form_banco', array('legend' => Mage::helper('crudbcc')->__('Información General')));

        $fieldset_banco->addField('nombre', 'text', array(
            'label' => Mage::helper('crudbcc')->__('Nombre'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'nombre',
        ));

        if (Mage::getSingleton('adminhtml/session')->getCrudbccData()) {
            
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCrudbccData());
            Mage::getSingleton('adminhtml/session')->setCrudbccData(null);
            
        } elseif (Mage::registry('crudbcc_data')) {
            $form->setValues(Mage::registry('crudbcc_data')->getData());
        }
        
        return parent::_prepareForm();
    }

}