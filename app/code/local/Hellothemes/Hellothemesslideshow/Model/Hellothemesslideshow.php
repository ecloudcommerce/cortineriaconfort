<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_Hellothemesslideshow_Model_Hellothemesslideshow extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('hellothemesslideshow/hellothemesslideshow');
    }

}