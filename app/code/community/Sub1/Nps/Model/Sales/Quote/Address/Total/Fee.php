<?php

class Sub1_Nps_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    /**
     * Identificador del Modelo del metodo de Pago
     * @var string
     */
    protected $_paymentMethodCode = "nps/nps";

    /**
     * 
     */
    public function __construct() {
        $this->setCode('npsfee');
    }

    /**
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Bl_Nps_Model_Quote_Address_Total_Fee
     */
    public function collect(Mage_Sales_Model_Quote_Address $address) {
        
        parent::collect($address);
        // Reset Nps Address Fields
        $this->_resetFields($address);

        if ($this->_isCollectable($address)) {

            // Payment Models
            $paymentMethod = $this->_getPaymentMethodModel();
            $paymentsModel = $this->_getPaymentsModel();

            // Totals Amounts
            $totals = array_sum($address->getAllTotalAmounts());
            $baseTotals = array_sum($address->getAllBaseTotalAmounts());

            // Nps Charge Amounts
            $baseAmount = $paymentsModel->calculateChargeAmount($baseTotals);
            $amount = $paymentsModel->calculateChargeAmount($totals);
            
            // Set Amounts
            $this->_setBaseAmount($baseAmount);
            $this->_setAmount($amount);

            // Set Amounts in QuoteAddres
            $address->setBaseNpsfee($baseAmount);
            $address->setNpsfee($amount);

            // Description @todo -> ver de hacer algo mas explicativo
            $address->setNpsfeeDescription(Mage::helper("nps")->__("Costo de Financiacion:"));
            // Set Payment Method Data to Quote
            $address->setData(
                    $paymentMethod->getDataNamespace(), $paymentMethod->getDataForSaveQuote()
            );
        }
        return $this;
    }

    /**
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Bl_Nps_Model_Quote_Address_Total_Fee 
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        if ($address->getNpsfee()) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $address->getNpsfeeDescription(),
                'value' => $address->getNpsfee()
            ));
        }
        return $this;
    }

    /**
     * Determina si hay que aplicar los intereses
     * 
     * @param Mage_Sales_Model_Quote_Address $address
     * @return <type>
     */
    protected function _isCollectable(Mage_Sales_Model_Quote_Address $address) {
        $quote = $address->getQuote();
        if ($quote->getPayment()->hasMethodInstance()
                && $quote->getPayment()->getMethodInstance()->getCode() == $this->_getPaymentCode()
        ) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param <type> $address
     *
     * @todo Cambiar los atributos a:
     *
     * nps_charge
     * base_nps_charge o nps_base_charge
     * nps_charge_description // Texto Informativo
     * nps_data (Queda Igual)
     *
     */
    protected function _resetFields($address) {
        // Reset Fee Amount
        $address->unsetData('npsfee');
        $address->unsetData('base_npsfee');
        $address->unsetData('npsfee_description');
        $address->unsetData('nps_data');
        return $this;
    }

    protected function _getPaymentCode() {
        return $this->_getPaymentMethodModel()->getCode();
    }

    /**
     * @return Bl_Nps_Model_Payments
     */
    protected function _getPaymentsModel() {
        return $this->_getPaymentMethodModel()->getPaymentsModel();
    }

    /**
     * Devuelve el Modelo del Metodo de pago "Nps Standard"
     * @return Bl_Nps_Model_Standard
     */
    protected function _getPaymentMethodModel() {
        return Mage::getModel($this->_paymentMethodCode);
    }

    /**
     * Devuelve modelo de session del checkout
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

}
