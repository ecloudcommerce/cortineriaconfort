<?php

class Bl_Promopsp_Block_Widget_Chooser_Banco extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct($arguments = array()) {

        parent::__construct($arguments);

        if ($this->getRequest()->getParam('current_grid_id')) {
            $this->setId($this->getRequest()->getParam('current_grid_id'));
        } else {
            $this->setId('BancoChooserGrid_' . $this->getId());
        }

        $form = $this->getJsFormObject();
        $this->setRowClickCallback("$form.chooserGridRowClick.bind($form)");
        $this->setCheckboxCheckCallback("$form.chooserGridCheckboxCheck.bind($form)");
        $this->setRowInitCallback("$form.chooserGridRowInit.bind($form)");
        $this->setDefaultSort('nombre');
        $this->setUseAjax(true);

        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }

    protected function _addColumnFilterToCollection($column) {
        if ($column->getId() == 'in_bancos') { //ver
            $selected = $this->_getSelectedProducts();
            if (empty($selected)) {
                $selected = '';
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('nombre', array('in' => $selected));
            } else {
                $this->getCollection()->addFieldToFilter('nombre', array('nin' => $selected));
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('crudbcc/bancos')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('in_bancos', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_bancos',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'nombre',
            'use_index' => true,
        ));

        $this->addColumn('id', array(
            'header' => Mage::helper('sales')->__('ID'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'id'
        ));

        $this->addColumn('nombre', array(
            'header' => Mage::helper('crudbcc')->__('Nombre'),
            'align' => 'left',
            'index' => 'nombre',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/chooser', array(
                    '_current' => true,
                    'current_grid_id' => $this->getId(),
                    'collapse' => null
                ));
    }

    protected function _getSelectedProducts() {
        $products = $this->getRequest()->getPost('selected', array());
        return $products;
    }

}
