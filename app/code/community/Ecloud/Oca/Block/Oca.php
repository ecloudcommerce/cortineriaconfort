<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?>
<?php
class Ecloud_Oca_Block_Oca
    extends Mage_Core_Block_Abstract
    implements Mage_Widget_Block_Interface
{

    protected function _toHtml()
    {
		$html ='';
        $html .= 'oca parameter1 = '.$this->getData('parameter1').'<br/>';
        $html .= 'oca parameter2 = '.$this->getData('parameter2').'<br/>';
        $html .= 'oca parameter3 = '.$this->getData('parameter3').'<br/>';
        $html .= 'oca parameter4 = '.$this->getData('parameter4').'<br/>';
        $html .= 'oca parameter5 = '.$this->getData('parameter5').'<br/>';
        return $html;
    }
	
}
?>