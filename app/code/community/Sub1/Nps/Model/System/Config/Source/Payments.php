<?php

class Sub1_Nps_Model_System_Config_Source_Payments {

    public function toOptionArray() {
        return array(
            array('value' => Sub1_Nps_Model_Payments::SOURCE_TYPE_CC, 'label' => Mage::helper('nps')->__("Tarjetas de Crédito")),
            array('value' => Sub1_Nps_Model_Payments::SOURCE_TYPE_BCC, 'label' => Mage::helper('nps')->__("Bancos/Tarjetas de Crédito")),
        );
    }

}