<?php
/* @var $this Hd_Nps_Model_Resource_Setup */

$this->_initSetup()
    ->setPaymentAttributes()
    ->setStatuses()
    ->_endSetup();