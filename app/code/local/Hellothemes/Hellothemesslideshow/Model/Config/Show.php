<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_Hellothemesslideshow_Model_Config_Show
{
    public function toOptionArray()
    {
	    $options = array();
	    $options[] = array(
            'value' => 'home',
            'label' => 'HomePage Only',
        );
        $options[] = array(
            'value' => 'all',
            'label' => 'All Pages',
        );

        return $options;
    }

}
