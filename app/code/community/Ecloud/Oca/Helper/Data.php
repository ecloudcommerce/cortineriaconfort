<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?>
<?php
class Ecloud_Oca_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getTrackingpopup($tracking) {

        $trackingEvents = Mage::getModel('oca/apicall')->getTrackingPieza($tracking);

        return $trackingEvents;
	}

	/**
	* Devuelve los datos de la orden segun el metodo elegido en checkout
	* Si no es metodo oca, devuelve false
	*/
	public function getOcaData($method){
		if(preg_match('/ocaestandardomicilio/',$method)){
			$datos 		= Mage::getSingleton('core/session')->getOcaEstandarDomicilio();
		}elseif(preg_match('/ocaestandarsucursal/',$method)){
			$datos 		= Mage::getSingleton('core/session')->getOcaEstandarSucursal();
            $datos['sucursal_retiro']      =  $this->_getSucursalRetiro($method);
		}elseif(preg_match('/ocaprioritariodomicilio/',$method)){
			$datos 		= Mage::getSingleton('core/session')->getOcaPrioritarioDomicilio();
		}elseif(preg_match('/ocaprioritariosucursal/',$method)){
			$datos 		= Mage::getSingleton('core/session')->getOcaPrioritarioSucursal();
            $datos['sucursal_retiro']      =  $this->_getSucursalRetiro($method);
		}else{
			return false;
		}
		return $datos;
	}

    /**
    * Devuelve el id de la sucursal seleccionada en el checkout (si hubiera, si no devuelve null)
    */
    protected function _getSucursalRetiro($string){
        $regex = "/sucursal(\d+)/";
        $code = preg_match($regex, $string, $matches);
        if($code > 0){
            return $matches[1];
        }
        return null;
    }
}
?>