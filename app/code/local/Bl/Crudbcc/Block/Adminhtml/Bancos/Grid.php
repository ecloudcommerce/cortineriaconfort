<?php

class Bl_Crudbcc_Block_Adminhtml_Bancos_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        
        parent::__construct();
        
        $this->setId('bancosGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        
    }

    protected function _prepareCollection() {
        
        $collection = Mage::getModel('crudbcc/bancos')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
        
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('crudbcc')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('nombre', array(
            'header' => Mage::helper('crudbcc')->__('Nombre'),
            'align' => 'left',
            'index' => 'nombre',
        ));

        /*
          $this->addColumn('status', array(
          'header'    => Mage::helper('crudbcc')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
          1 => 'Enabled',
          2 => 'Disabled',
          ),
          ));
         */
        $this->addColumn('action', array(
            'header' => Mage::helper('crudbcc')->__('Accion'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('crudbcc')->__('Editar'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('crudbcc');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('crudbcc')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('crudbcc')->__('Está seguro?')
        ));

        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}