<?php
$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('bl_nps_psp_log')};
    CREATE TABLE IF NOT EXISTS {$this->getTable('bl_nps_psp_log')} (
        `id` int(11) NOT NULL auto_increment,
        `type` varchar(40) NOT NULL,
        `reference_id` varchar(40) NOT NULL,
        `order_increment_id` varchar(40),
        `parameters` text,
        `description` varchar(200) ,
        `date` datetime,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//die(get_class($this));
$installer->addAttribute('quote_address', 'npsfee',             array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('quote_address', 'base_npsfee',        array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('quote_address', 'npsfee_description', array('type' => 'varchar',   'grid' => true));
$installer->addAttribute('quote_address', 'nps_data',           array('type' => 'text',      'grid' => false));

$installer->addAttribute('order', 'npsfee',             array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('order', 'base_npsfee',        array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('order', 'npsfee_description', array('type' => 'varchar',   'grid' => true));
$installer->addAttribute('order', 'nps_data',           array('type' => 'text',      'grid' => false));

$installer->addAttribute('invoice', 'npsfee',             array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('invoice', 'base_npsfee',        array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('invoice', 'npsfee_description', array('type' => 'varchar',   'grid' => true));
$installer->addAttribute('invoice', 'nps_data',           array('type' => 'text',      'grid' => false));

$installer->addAttribute('creditmemo', 'npsfee',             array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('creditmemo', 'base_npsfee',        array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('creditmemo', 'npsfee_description', array('type' => 'varchar',   'grid' => true));
$installer->addAttribute('creditmemo', 'nps_data',           array('type' => 'text',      'grid' => false));

$installer->run("
    DELETE FROM `{$this->getTable('core_config_data')}`
    WHERE path LIKE 'payment/nps_standard/%';");

$installer->endSetup();
