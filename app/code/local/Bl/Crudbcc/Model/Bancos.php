<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Model_Bancos extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('crudbcc/bancos');
    }

    public function _beforeSave() {
        if ($this->hasData('tarjetas')) {
            $this->setData('tarjetas', serialize($this->getData('tarjetas')));
        }
        parent::_beforeSave();
    }

    public function _afterLoad() {
        parent::_afterLoad();
        if ($this->hasData('tarjetas')) {
            $this->setData('tarjetas', unserialize($this->getData('tarjetas')));
        }
    }

    public function getTarjetas() {
        if ($this->hasData('tarjetas')) {
            if (!is_array($this->getData('tarjetas'))) {
                return unserialize($this->getData('tarjetas'));
            }
            return $this->getData('tarjetas');
        }
    }

    public function toOptionArray() {
        $banco = array(array('value' => '', 'label' => ''));
        $tmp_banco = $this->getCollection();
        foreach ($tmp_banco as $x) {
            $banco[] = array(
                'value' => $x->getId(),
                'label' => $x->getNombre()
            );
        }
        return $banco;
    }

    public function getName(){
        return $this->getNombre();
    }

    public function hasTarjeta($idCual) {
        if (array_search($idCual, (array) $this->getTarjetas()) === false) {
            return false;
        }
        return true;
    }

}