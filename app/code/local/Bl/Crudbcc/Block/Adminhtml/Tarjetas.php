<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Tarjetas extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        
        $this->_controller = 'adminhtml_tarjetas';
        $this->_blockGroup = 'crudbcc';
        $this->_headerText = Mage::helper('crudbcc')->__('Tarjetas');
        $this->_addButtonLabel = Mage::helper('crudbcc')->__('Agregar Tarjeta');
        
        parent::__construct();
        
    }

}
