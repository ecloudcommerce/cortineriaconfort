<?php

class Sub1_Nps_Block_Standard_Form_Cc extends Sub1_Nps_Block_Form {

    protected $_templatePath = 'nps/standard/form-cc.phtml';

    public function getCreditCards() {
        $data = $this->getPaymentsModel()->getCreditCardsData();
        return (count($data) > 0) ? $data : null;
    }

    public function getFeesUrl() {
        return $this->getBaseUrl('nps/standard_cc/getFees/', true);
    }

}