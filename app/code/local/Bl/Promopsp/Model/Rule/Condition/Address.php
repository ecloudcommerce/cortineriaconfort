<?php

class Bl_Promopsp_Model_Rule_Condition_Address extends Mage_SalesRule_Model_Rule_Condition_Address {

    protected $_bankName;
    protected $_ccName;

    public function loadAttributeOptions() {
        parent::loadAttributeOptions();
        $defaultAttributes = $this->getAttributeOption();
        $attributes = array_merge(
                $this->getAttributeOption(), array(
            'tarjeta' => Mage::helper('salesrule')->__('Tarjeta'),
            'banco' => Mage::helper('salesrule')->__('Banco'),
            'cuotas' => Mage::helper('salesrule')->__('Cuotas')
                )
        );
        $this->setAttributeOption($attributes);
        return $this;
    }

    public function getInputType() {
        switch ($this->getAttribute()) {
            case 'base_subtotal': case 'weight': case 'total_qty':
                return 'numeric';
            case 'shipping_method': case 'payment_method': case 'country_id': case 'region_id':
                return 'select';
            case 'banco': case 'tarjeta':
                return 'multiselect';
        }
        return 'string';
    }

    public function getValueElementType() {
        switch ($this->getAttribute()) {
            case 'shipping_method': case 'payment_method': case 'country_id': case 'region_id':
                return 'select';
            case 'banco': case 'tarjeta': case 'cuotas' :
                return "text";
        }
        return 'text';
    }

    public function getExplicitApply() {
        switch ($this->getAttribute()) {
            case 'banco': case 'tarjeta': case 'cuotas' :
                return true;
        }
        return false;
    }

    public function getValueAfterElementHtml() {
        $html = '';

        switch ($this->getAttribute()) {
            case 'banco': case 'tarjeta':
                $image = Mage::getDesign()->getSkinUrl('images/rule_chooser_trigger.gif');
                break;
        }

        if (!empty($image)) {
            $html = '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="' . $image . '" alt="" class="v-middle rule-chooser-trigger" title="' . Mage::helper('rule')->__('Open Chooser') . '" /></a>';
        }
        return $html;
    }

    public function getValueElementChooserUrl() {
        $url = false;
        switch ($this->getAttribute()) {
            case 'banco': case 'tarjeta': case 'cuotas' :
                $url = 'adminhtml/widget/chooser'
                        . '/attribute/' . $this->getAttribute();
                if ($this->getJsFormObject()) {
                    $url .= '/form/' . $this->getJsFormObject();
                }
                break;
        }
        return $url !== false ? Mage::helper('adminhtml')->getUrl($url) : '';
    }

    public function getDefaultOperatorInputByType() {
        if (null === $this->_defaultOperatorInputByType) {
            $this->_defaultOperatorInputByType = array(
                'string' => array('==', '!=', '>=', '>', '<=', '<', '{}', '!{}', '()', '!()'),
                'numeric' => array('==', '!=', '>=', '>', '<=', '<', '()', '!()'),
                'date' => array('==', '>=', '<='),
                'select' => array('==', '!='),
                'boolean' => array('==', '!='),
                'multiselect' => array('==', '!=', '()', '!()'),
                'grid' => array('()', '!()'),
            );
        }
        return $this->_defaultOperatorInputByType;
    }

    /**
     * Devuelve el nombre del Banco seleccionado
     * 
     * @param int $id
     * @return string
     */
    protected function _getBankName($id) {
        if (!$this->_bankName) {
            $bank = Mage::getModel('crudbcc/bancos')->load($id);
            if (!$bank->getId()) {
                Mage::throwException("El Banco seleccionado no existe.");
            }
            $this->_bankName = $bank->getNombre();
        }
        return $this->_bankName;
    }

    /**
     * Devuelve el nombre de la Tarjeta seleccionada
     * 
     * @param int $id
     * @return string
     */
    protected function _getCcName($id) {
        if (!$this->_ccName) {
            $cc = Mage::getModel('crudbcc/tarjetas')->load($id);
            if (!$cc->getId()) {
                Mage::throwException("La Tarjeta seleccionada no existe.");
            }
            $this->_ccName = $cc->getNombre();
        }
        return $this->_ccName;
    }

    /**
     * Devuelve un Array con la data del metodo de pago y los nombres de banco y tarjeta
     * 
     * @return array
     */
    protected function _getPaymentData() {
        // Validacion de form data
        $data = Mage::app()->getRequest()->getParam("payment");
        if (!$data || !is_array($data)) {
            return null;
//            Mage::throwException(Mage::helper("promopsp")->__("No se recibieron datos del metodo de pago."));
        }
        // Validacion del Metodo de Pago
        //if (!isset($data['nps']) || $data['method'] != "nps_standard") {
        if (!isset($data['nps']) || $data['method'] != "nps") {
            return null;
        }
        $data = $data['nps'];
        $data['banco'] = $this->_getBankName($data['bank_id']);
        $data['tarjeta'] = $this->_getCcName($data['cc_id']);
        $data['cuotas'] = $data['cc_fee'];
        return $data;
    }

    /**
     * Valida si la cantidad de cuotas seleccionada esta habilidas en la promocion
     * 
     * @param Varien_Object $paymentData
     * @return Boolean
     */
    protected function _validatePayments($paymentData) {
        return true;
        // Verifico si la cuota cc_fee
        // esta en el array de cuotas de las promos
        //TODO: En ST/UAT no esta funcionando esta funcion
        $cuotas = unserialize($this->getRule()->getData('cuotas'));
        $key    = array_search($paymentData['cc_fee'], array_column($cuotas, 'n_cuota'));
        if(!$key) return false;
        return true;
    }

    /**
     * Override del metodo validate de la clase padre
     * 
     * @param Varien_Object $object
     * @return Boolean
     */
    public function validate(Varien_Object $object) {

        $action = Mage::app()->getRequest()->getActionName();
        // Solo reemplazamos validacion para los attributos custom "banco" ó "tarjeta"
        if ($this->getAttribute() != 'banco' && $this->getAttribute() != 'tarjeta' && $this->getAttribute() != 'cuotas') {
            return parent::validate($object);
        }
        // En los requests anteriores no debe aplicar
        if ($action != "savePayment" && $action != 'saveOrder') {
            return parent::validate($object);
        }
        // Data
        $data = $this->_getPaymentData();

        // Validamos las cuotas (si estan definidas)
        if (!$this->_validatePayments($data)) {
            return false;
        }

        $object->setBanco($data['banco'])
                ->setTarjeta($data['tarjeta'])
                ->setCuotas($data['cuotas']);

        return parent::validate($object);

        $key = $this->getAttribute(); // banco o tarjeta
        $value = $this->getValueParsed();

        switch ($this->getOperator()) {
            case "==":
                return ($data[$key] == $value) ? true : false;
            case "!=":
                return ($data[$key] != $value) ? true : false;
            case "()":
                return in_array($data[$key], $value);
            case "!()":
                return !in_array($data[$key], $value);
        }
        return false;
    }

}