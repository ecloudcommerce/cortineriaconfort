<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */
 
class Hellothemes_Hellothemesslideshow_Block_Slideshow extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
    public function getSlideshow()
    {
        if (!$this->hasData('hellothemesslideshow')) {
            $this->setData('hellothemesslideshow', Mage::registry('hellothemesslideshow'));
        }
        return $this->getData('hellothemesslideshow');
        
    }

	public function getSlides()
    {
        $slides  = Mage::getModel('hellothemesslideshow/hellothemesslideshow')->getCollection()
            ->addStoreFilter(Mage::app()->getStore())
        	->addFieldToSelect('*')
        	->addFieldToFilter('status', 1)
            ->setOrder('sort_order', 'asc');
        return $slides;
    }

}