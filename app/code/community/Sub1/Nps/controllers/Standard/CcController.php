<?php

class Sub1_Nps_Standard_CcController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        die('Access Denied');
    }

    public function getFeesAction() {
        
        $ccid = (int) $this->getRequest()->getParam('cc_id', false);
        try {
            if ($ccid) {
                $data = $this->_getStandard()->getPaymentsModel()->getPaymentsData(array(
                    'cc_id' => $ccid,
                        ));
                $data['status'] = 'ok';
            } else {
                throw new Exception(Mage::helper('nps')->__('You must specify a Credit Card'));
            }
        } catch (Exception $e) {
            $data = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
        }
        $data['type'] = 'json';
        $this->_responseToJson($data);
    }

    public function testAction() {
        echo "sQuery Request:";
        print_r(unserialize('a:6:{s:14:"psp_MerchantId";s:12:"laperfumerie";s:15:"psp_PosDateTime";s:19:"2011-09-08 20:13:49";s:17:"psp_QueryCriteria";s:1:"T";s:19:"psp_QueryCriteriaId";s:6:"746078";s:14:"psp_SecureHash";s:32:"1c08bd89d9541b95037a295c4f1903c3";s:11:"psp_Version";s:1:"1";}'));

        echo "sQuery Response:";
        print_r(unserialize('O:8:"stdClass":7:{s:15:"psp_ResponseCod";s:1:"2";s:15:"psp_ResponseMsg";s:32:"Transacción de consulta exitosa";s:14:"psp_MerchantId";s:12:"laperfumerie";s:17:"psp_QueryCriteria";s:1:"T";s:19:"psp_QueryCriteriaId";s:6:"746078";s:15:"psp_PosDateTime";s:19:"2011-09-08 20:13:49";s:15:"psp_Transaction";O:8:"stdClass":19:{s:15:"psp_ResponseCod";s:2:"12";s:15:"psp_ResponseMsg";s:15:"Error en el MOP";s:20:"psp_ResponseExtended";s:36:"MOP GPAY: 130 - Comercio Inexistente";s:17:"psp_TransactionId";s:6:"746078";s:14:"psp_MerchantId";s:12:"laperfumerie";s:14:"psp_MerchTxRef";s:39:"1000000188d93f1c853c727bd43bd60924e8b88";s:16:"psp_MerchOrderId";s:9:"100000018";s:10:"psp_Amount";s:5:"11000";s:15:"psp_NumPayments";s:1:"1";s:12:"psp_Currency";s:3:"032";s:11:"psp_Country";s:3:"ARG";s:11:"psp_Product";s:1:"5";s:18:"psp_CardNumber_LFD";s:4:"7785";s:18:"psp_CardNumber_FSD";s:6:"532362";s:16:"psp_CustomerMail";s:21:"mnigrele@alto-net.com";s:16:"psp_MerchantMail";s:22:"mnigrele@brandlive.net";s:11:"psp_ClTrnId";s:7:"4622370";s:15:"psp_PosDateTime";s:22:"2011-09-08 20:13:25-03";s:23:"psp_PurchaseDescription";s:15:"Default Store V";}}'));

        die();
        $this->norouteAction();
    }

    public function logsAction() {
        if (!$this->_getStandard()->isDebug()) {
            $this->norouteAction();
            die('Access Denied');
        }

        $collection = Mage::getModel('nps/psp_log')->getCollection();
        if ($collection) {
            $html = '';
            foreach ($collection as $log) {

                $html .= '<div>';
                $html .= '   <table>';
                $html .= '       <tr><td>Type:</td><td>' . $log->getType() . '</td></tr>';
                $html .= '       <tr><td>Descripcion:</td><td>' . $log->getDescription() . '</td></tr>';
                $html .= '       <tr><td>Referencia:</td><td>' . $log->getReferenceId() . '</td></tr>';
                $html .= '       <tr><td>Orden:</td><td>' . $log->getOrderIncrementId() . '</td></tr>';
                $html .= '       <tr><td>Fecha:</td><td>' . $log->getDate() . '</td></tr>';
                $html .= '       <tr><td colspan="2">Detalles:</td></tr>';
                @$html .= '       <tr><td colspan="2"><pre>' . print_r(unserialize($log->getParameters()), true) . '</pre></td></tr>';
                $html .= '   </table>';
                $html .= '</div>';
            }
            echo $html;
        }

        // print_r($log->getData());
        die();
    }

    public function squeryAction() {
        $result = $this->_getStandard()->getTransactionResult();
        print_r($result);
        die();
    }

    /**
     *
     * @param <type> $data
     * @return Bl_Nps_Standard_CcController 
     */
    protected function _responseToJson($data) {
        $this->getResponse()
                ->setBody(Mage::helper('core')->jsonEncode($data));
        return $this;
    }

    /**
     *
     * @return Bl_Nps_Model_Standard
     */
    protected function _getStandard() {
        return Mage::getModel('nps/nps');
    }

    protected function _getSource() {
        return $this->_getStandard()->getPaymentsModel();
    }

    public function preDispatch() {
        parent::preDispatch();
        if ($this->_isDebugCall()) {
            echo '<pre style="border: 1px solid #CCC; background: #EEE; color: #444;">';
        }
    }

    protected function _isDebugCall() {
        return (bool) $this->getRequest()->getParam('debug', false);
    }

}