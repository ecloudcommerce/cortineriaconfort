<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Bancos_Edit_Tab_Formtarjeta extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset_banco = $form->addFieldset('crudbcc_form_banco', array('legend' => Mage::helper('crudbcc')->__('Tarjetas')));

        $fieldset_banco->addField('tarjetas', 'multiselect', array(
            'label' => Mage::helper('crudbcc')->__('Tarjetas'),
            'name' => 'tarjetas',
            'values' => $this->getTarjetasForm(),
            'class' => 'requried-entry'
        ));

        if (Mage::getSingleton('adminhtml/session')->getCrudbccData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCrudbccData());
            Mage::getSingleton('adminhtml/session')->setCrudbccData(null);
        } elseif (Mage::registry('crudbcc_data')) {
            $form->setValues(Mage::registry('crudbcc_data')->getData());
        }

        return parent::_prepareForm();
    }

    public function getTarjetasForm() {

        $tmp_sale = Mage::getModel('crudbcc/tarjetas')->getCollection();
        $sale = array();

        foreach ($tmp_sale->getData() as $x) {
            $sale[] = array(
                "label" => $x['nombre'],
                "value" => $x['id']
            );
        }

        return $sale;
    }

}