<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2014 ecloud solutions ®
 */
?>
<?php 
class Ecloud_Oca_Adminhtml_OrdersController extends Mage_Adminhtml_Controller_Action
{

	public function massImponerAction(){
		$orderIds = $this->getRequest()->getParam('order_ids');

		// Seteamos que estamos haciendo imposicion masiva para no usar el observer
		Mage::register('imposicion_masiva', 1);

		$ordersOk = array();

		foreach ($orderIds as $orderId) {
			$order = Mage::getModel('sales/order')->load($orderId);
			$metodo = $order->getShippingMethod();
			if(preg_match('/oca/',$metodo)) {
				if(!$order->getShipmentsCollection()->count()){
					$ordersOk[] = $orderId;
				}else{
					Mage::getSingleton('adminhtml/session')->addError('La orden n° '.$order->getIncrementId().' ya ha sido enviada previamente.');
				}
			}else{
				Mage::getSingleton('adminhtml/session')->addError('La orden n° '.$order->getIncrementId().' no corresponde a OCA');
			}
		}


		$data = array('orders' => $ordersOk);

		if(count($ordersOk) > 0){
			try{
				Mage::dispatchEvent('oca_generar_retiro_masivo', $data);
			}catch(Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}			
		}

		Mage::unregister('imposicion_masiva');

		$this->_redirect('adminhtml/sales_order/index');

	}

	public function envioImpresoAction(){

	}
}

?>