<?php
/**
 * @autor W3ITSolutions
 */
$installer = $this;
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('crudbcc_banco')};
CREATE TABLE `{$this->getTable('crudbcc_banco')}` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `nombre` varchar(255) DEFAULT NULL,
     `tarjetas` text,
     `update_time` datetime DEFAULT NULL,
     `created_time` datetime DEFAULT NULL,
     PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   
DROP TABLE IF EXISTS {$this->getTable('crudbcc_tarjeta')};
CREATE TABLE `{$this->getTable('crudbcc_tarjeta')}` (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `nombre` varchar(255) DEFAULT NULL,
       `codigo_nps` varchar(255) DEFAULT NULL,
       `cuotas` text,
       PRIMARY KEY (`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('crudbcc_tarjeta')}
    (`codigo_nps`, `nombre`)
VALUES
    (1,'American Express'),
    (2,'Diners'),
    (5,'Mastercard'),
    (8,'Cabal'),
    (9,'Naranja'),
    (14,'Visa'),
    (21,'Nevada'),
    (29,'Visa Naranja'),
    (43,'Italcred'),
    (48,'Mas'),
    (49,'Naranja MO'),
    (50,'Pyme Nación'),
    (55,'Visa Débito'),
    (63,'Nativa'),
    (65,'Argencard'),
    (72,'Consumax');

");

$installer->endSetup();