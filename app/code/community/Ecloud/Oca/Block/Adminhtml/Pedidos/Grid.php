<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?>
<?php
class Ecloud_Oca_Block_Adminhtml_Pedidos_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('oca_order');
        $this->setDefaultSort('order_increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('oca/order')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        /*
        $this->addColumn('id', array(
            'header' => Mage::helper('oca')->__('ID'),
            'sortable' => true,
            'width' => '5',
            'index' => 'id'
        ));*/
 
        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('oca')->__('# Pedido'),
            'sortable' => true,
            'width' => '5',
            'index' => 'order_increment_id',
            'type'  => 'text'
        ));

        $this->addColumn('nombre', array(
            'header' => Mage::helper('oca')->__('Nombre'),
            'sortable' => true,
            'width' => '5',
            'index' => 'nombre',
            'type'  => 'text'
        ));

        $this->addColumn('apellido', array(
            'header' => Mage::helper('oca')->__('Apellido'),
            'sortable' => true,
            'width' => '5',
            'index' => 'apellido',
            'type'  => 'text'
        ));

        $this->addColumn('sucursal_retiro', array(
            'header' => Mage::helper('oca')->__('Sucursal'),
            'sortable' => false,
            'width' => '5',
            'index' => 'sucursal_retiro',
            'type'  => 'text'
        ));
        
        $this->addColumn('nro_cuenta', array(
            'header' => Mage::helper('oca')->__('Nro Cuenta OCA'),
            'sortable' => true,
            'width' => '60',
            'index' => 'nro_cuenta',
            'type'  => 'text'
        ));
 
        $this->addColumn('operatoria', array(
            'header' => Mage::helper('oca')->__('Operatoria'),
            'sortable' => true,
            'width' => '60',
            'index' => 'operatoria',
            'type'  => 'text'
        ));

        $this->addColumn('cod_tracking', array(
            'header' => Mage::helper('oca')->__('Nro Oca - Tracking'),
            'sortable' => true,
            'width' => '5',
            'index' => 'cod_tracking',
            'type'  => 'text'
        ));

        $this->addColumn('impresion', array(
			'header'=> Mage::helper('catalog')->__('Imprimir Constancia'),
			'sortable'  => false,
			'target' => '_blank',
            'width' => '5',
			'renderer'  => 'oca/adminhtml_Pedidos_Edit_Renderer_button'
        ));

        $this->addColumn('entrega', array(
            'header' => Mage::helper('oca')->__('Fecha de entrega'),
            'sortable' => true,
            'width' => '1',
            'index' => 'entrega',
            'type'  => 'text'
        ));

        $this->addColumn('estado', array(
            'header'    => Mage::helper('oca')->__('Estado'),
            'sortable'  => false,
            'width'     => '5',
            'index'     => 'estado',
            'type'      => 'options',
            'sortable'  => false,
            'options'   => array(
                'Enviado'   => 'Enviado',
                'Eliminada'  => 'Eliminada',
                'Entregado' => 'Entregado',
                'Pendiente' => 'Pendiente'
            )
        ));
 
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('eliminar', array(
            'label'=> Mage::helper('oca')->__('Eliminar'),
            'url'  => $this->getUrl('*/*/massEliminar', array('' => '')),
            'confirm' => Mage::helper('oca')->__('¿Seguro que quieres eliminar la orden?')
        ));
        $this->getMassactionBlock()->addItem('entregado', array(
            'label'=> Mage::helper('oca')->__('Entregado'),
            'url'  => $this->getUrl('*/*/massEntregado', array('' => '')),
            'confirm' => Mage::helper('oca')->__('¿Seguro que quieres modificar el estado de la orden?')
        ));
        $this->getMassactionBlock()->addItem('pendiente', array(
            'label'=> Mage::helper('oca')->__('Pendiente'),
            'url'  => $this->getUrl('*/*/massPendiente', array('' => '')),
            'confirm' => Mage::helper('oca')->__('¿Seguro que quieres modificar el estado de la orden?')
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
         return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
	
}
?>