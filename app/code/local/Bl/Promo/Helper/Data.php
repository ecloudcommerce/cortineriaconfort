<?php

/**
 * @author w3itsolutions
 */
class Bl_Promo_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * @return array()
     */
    public function getWeekdays() {
        return Mage::app()->getLocale()->getOptionWeekdays();
    }

    public function mergeIds($arr1, $arr2, $asString = true) {
        if (!is_array($arr1)) {
            $arr1 = empty($arr1) ? array() : explode(',', $arr1);
        }
        if (!is_array($arr2)) {
            $arr2 = empty($arr2) ? array() : explode(',', $arr2);
        }
        $arr = array_unique(array_merge($arr1, $arr2));
        if ($asString) {
            $arr = implode(',', $arr);
        }
        return $arr;
    }

}
