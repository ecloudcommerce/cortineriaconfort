<?php
class Ecloud_Cuotas_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCuotasInfo($precio, $prod_cuotas = null){
		$store_id  = Mage::app()->getStore()->getStoreId();
		
	    $enabled   = Mage::getStoreConfig('ecloudcuotas/ecloudconfig/enabled',$store_id);
	    if (!$enabled){
	        return false;
	    }

	    //$prod_cuotas = Mage::getResourceModel('catalog/product')->getAttributeRawValue($id, 'cuotas', $store_id);
	    if (!empty($prod_cuotas)){
	    	$cuotas  = $prod_cuotas;
	    }else{
	    	$cuotas  = Mage::getStoreConfig('ecloudcuotas/ecloudconfig/cuotas',$store_id);
	    }
	    
	    $interes_enabled    = Mage::getStoreConfig('ecloudcuotas/ecloudconfig/interes_enabled',$store_id);
	    if ($interes_enabled){
	    	$label 			= '';
	        $interes        = Mage::getStoreConfig('ecloudcuotas/ecloudconfig/interes',$store_id) / 100;
	        $multiplicando  = 1 + $interes;
	        $cuota_precio 	= number_format((float)(($precio * $multiplicando) / $cuotas), 2, '.', '');
	    }else{
	    	$label          = Mage::getStoreConfig('ecloudcuotas/ecloudconfig/cerointeres_label',$store_id);
	        $cuota_precio   = $precio / $cuotas;
	        //Redondeo a 2 decimales.
	        $cuota_precio = number_format((float)$cuota_precio, 2, '.', '');
	    }

	    return array(
	    			'cuotas' 		=> $cuotas,
	    			'cuota_precio' => $cuota_precio,
	    			'label'			=> $label
	    	);		
	}

}
	 