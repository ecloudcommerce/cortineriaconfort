<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Model_Tarjetas extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('crudbcc/tarjetas');
    }

    public function _beforeSave() {
        $cuotas_tmp = array();
        if ($this->hasData('cuotas')) {
            foreach ($this->getData('cuotas') as $tc) {
                if (!$tc['delete']) {
                    unset($tc['delete']);
                    $cuotas_tmp[] = $tc;
                    if (is_numeric($tc['n_cuota'])) {
                        if (strpos($tc['n_cuota'], ".") !== false) {
                            Mage::throwException("El campo número de cuota no debe contener decimales.");
                        }
                    } else {
                        Mage::throwException("El campo número de cuota sólo acepta números enteros.");
                    }
                }
            }
            $this->setData('cuotas', serialize($cuotas_tmp));
        }
        parent::_beforeSave();
    }

    public function _afterLoad() {
        parent::_afterLoad();
        if ($this->hasData('cuotas')) {
            $this->setData('cuotas', unserialize($this->getData('cuotas')));
        }
    }

    public function getCuotas() {
        if ($this->hasData('cuotas')) {
            return unserialize($this->getData('cuotas'));
        }
    }

    public function getName(){
        return $this->getNombre();
    }

    public function toOptionArray() {
        $tarjetas = array(array('value' => '', 'label' => ''));
        $tmp_tarjetas = $this->getCollection();
        foreach ($tmp_tarjetas as $x) {
            $tarjetas[] = array(
                'value' => $x->getId(),
                'label' => $x->getNombre()
            );
        }
        return $tarjetas;
    }

}