<?php


/** * * NOTICE OF LICENSE * * This source file is subject to the Open Software License (OSL). 
*  It is also available through the world-wide-web at this URL: *
*  http://opensource.org/licenses/osl-3.0.php * 
*  @category    Payment Gateway * @package    	MercadoPago 
*  @author      André Fuhrman (andrefuhrman@gmail.com) | Edited: Gabriel Matsuoka (gabriel.matsuoka@gmail.com)
*  @copyright  Copyright (c) MercadoPago [http://www.mercadopago.com] 
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0) 
*/
class Mpexpress_IpnController extends Mage_Core_Controller_Front_Action{
    
    protected $_return = null;
    protected $_order = null;
    protected $_order_id = null;
    protected $_mpcartid = null;
    protected $_sendemail = false;
    protected $_hash = null;
    protected $_ipn = null;
    protected $_config;
    const IPN_LOG_FILE = 'MP_IPN.log';
    
    public function indexAction(){

    	$this->_config = Mage::getModel('mpexpress/Express');                         
		$ipnLogEnabled = (bool)$this->_config->getConfigData('ipn_log');


		$params = $this->getRequest()->getParams();		

		if ($ipnLogEnabled) Mage::log('IPN Request: ' . print_r($params, true) , Zend_Log::INFO, self::IPN_LOG_FILE);
		

		if (isset($params['id']) && isset($params['topic'])){
		    try {

				$this->_ipn = Mage::getModel('mpexpress/Checkout');    

				$this->_return = $this->_ipn->GetStatus($params['id']);

				if ($ipnLogEnabled) Mage::log('IPN Get Status: ' . print_r($this->_return, true) , Zend_Log::INFO, self::IPN_LOG_FILE);

				if ((int)$this->_return['collection']['id'] === (int)$params['id']) {
				    $this->_process_order();
				}else{
				    echo 'Order not valid';
				}
			
		    } catch (Exception $e) {
			    Mage::logException($e);
			    die();
		    }
		}
    
    } 
    
    private function _process_order(){
    	
	
		//  $standard = new MercadoPago_Model_Standard();
		//$standard = Mage::getModel('mpexpress/Express');  
		$this->_get_order();
	    $current_status = $this->_order->getStatus();
		

		//Esto cambia los datos de la orden
		/*
		if ($this->_return['collection']['payer']['first_name'])
		    $this->_order->setCustomerFirstname($this->_return['collection']['payer']['first_name']);
		if ($this->_return['collection']['payer']['last_name'])
		    $this->_order->setCustomerLastname($this->_return['collection']['payer']['last_name']);
		if ($this->_return['collection']['payer']['email'])
		    $this->_order->setCustomerEmail($this->_return['collection']['payer']['email']);
		$this->_order->save();*/
		
		/*if($this->_sendemail){
		    $name = $this->_return['collection']['payer']['first_name'].' ' .$this->_return['collection']['payer']['last_name'];
		    $this->notify($name,$this->_return['collection']['payer']['email']);
		}*/

		switch ( $this->_return['collection']['status']) {
	    
			case 'approved':
				if ( $this->_return['collection']['status_detail'] == 'partially_refunded'){
					$status = $this->_config->getConfigData('order_status_partial_refunded');
					if ($status){
						$message = 'Devolucion parcial de dinero.';
						$state = $this->_get_new_order_state($status);
						$this->_order->setState($state, $status, $message);
					}
					break;	
				} 
				$order_approved = true;
				
				//si esta habilitado el split de pagos tengo que buscar todos los pagos de esa orden y verificar el monto
				$split_enabled = $this->_config->getConfigData('payment_split_enabled');

				if ((bool)$split_enabled){

					$order_approved = false;
		    		try{
		    			$merchant_order_info = $this->_ipn->GetMerchantOrder($this->_return['collection']['merchant_order_id']);

		    		}catch (Exception $e) {
				    	Mage::logException($e);
				    	die();
			    	}
		    		
			    	$transaction_amount_payments = 0;
					$transaction_amount_order = $merchant_order_info["total_amount"];
	    			$payments = $merchant_order_info["payments"];
	    			foreach ($payments as  $payment) {    				
	    				if($payment['status'] == 'approved'){
		    				$transaction_amount_payments += $payment['transaction_amount'];
		    			}	
	    			}
	    			//http://php.net/manual/en/language.types.float.php	
	    			if( round($transaction_amount_payments,2) >= round($transaction_amount_order,2) ){    	    		
	    				$order_approved = true;
					    
					}else{
						$status = $this->_config->getConfigData('order_status_in_process');
				    	$message = 'Pago parcial.';
				    	//$this->_order->addStatusToHistory($status, $message);
				    	
				    	$state = $this->_get_new_order_state($status);
						$this->_order->setState($state, $status, $message);
				    	
					}
				}

				if ($order_approved){
					$createinvoice = $this->_config->getConfigData('auto_create_inovice');				    
				    $status = $this->_config->getConfigData('order_status_approved');


				    if ($createinvoice == 1){  
						// Geração automatica de invoice    
						// checa para ver se já tem i nvoice    
						if(!$this->_order->hasInvoices()){
						    $invoice = $this->_order->prepareInvoice();   
						    $invoice->register()->pay();
						    Mage::getModel('core/resource_transaction')
						    ->addObject($invoice)
						    ->addObject($invoice->getOrder())
						    ->save();
						    
						    
						    $message = 'El pago '.$invoice->getIncrementId().' fue creado. MercadoPago ha confirmado automáticamente el pago de esta orden.';						    
						    //$this->_order->addStatusToHistory($status, $message,true);
						    $state = $this->_get_new_order_state($status);
							$this->_order->setState($state, $status, $message, true);
						    $invoice->sendEmail(true, $message);
						}
				    } else {

				    	if ($status != $current_status){
							
                            	
							// Geração não automática de invoice    
							$message = 'MercadoPago automáticamente confirmó el pago de ésta orden.';							
							//$this->_order->addStatusToHistory($status, $message,true);

							$state = $this->_get_new_order_state($status);
							$this->_order->setState($state, $status, $message, true);
							
							//$this->_order->sendOrderUpdateEmail(true, $message);
							$this->_order->sendNewOrderEmail();
						}
					}
				}
			    break;
			case 'refunded':
			    $status = $this->_config->getConfigData('order_status_refunded');
			    $message = 'El pago fue devuelto. El vendedor devolvió el valor ​​de esta operación.';	
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    $this->_order->cancel();
			    
			    
			    $this->_order->sendOrderUpdateEmail(true, $message);
			    break;
			case 'pending':
			    $status = $this->_config->getConfigData('order_status_in_process');
			    $message = 'El usuario todavía no ha realizado el pago.';
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    
			    $this->_order->sendOrderUpdateEmail(true, $message);			    
			    break;
			case 'in_process':
			    $status = $this->_config->getConfigData('order_status_in_process');
			    $message = 'El pago está siendo analizado.';
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    
			    $this->_order->sendOrderUpdateEmail(true, $message);
			    
			    break;
			case 'in_mediation':
			    $status = $this->_config->getConfigData('order_status_in_mediation');
			    $message = 'Se ha comenzado una disputa por el pago.';
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    
			    $this->_order->sendOrderUpdateEmail(true, $message);
			    break;
			case 'cancelled':    

				$no_cancel = (bool)$this->_config->getConfigData('order_no_cancel');

				if (! $no_cancel){
				    $status = $this->_config->getConfigData('order_status_cancelled');
				    $message = 'El pago fue cancelado.';
				    //$this->_order->addStatusToHistory($status, $message);
				    $state = $this->_get_new_order_state($status);
					$this->_order->setState($state, $status, $message);
				    
				    $this->_order->sendOrderUpdateEmail(true, $message);
				    $this->_order->cancel();
				}
				else{
					$this->_order->addStatusToHistory($current_status, 'Pago cancelado por MP');					
				}
			    break;
			case 'rejected':              
			    $status = $this->_config->getConfigData('order_status_rejected');
			    $message = 'El pago fue rechazado.';
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    
			    $this->_order->sendOrderUpdateEmail(true, $message);
			    break;
			case 'authorized':              
			    $status = $this->_config->getConfigData('order_status_in_process');
			    $message = 'Pago spliteado. Pago autorizado.';
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);
			    
			    break;
		    default:
			    $status = $this->_config->getConfigData('order_status_in_process');
			    $message = "";    
			    //$this->_order->addStatusToHistory($status, $message);
			    $state = $this->_get_new_order_state($status);
				$this->_order->setState($state, $status, $message);			    
			    $this->_order->sendOrderUpdateEmail(true, $message);
		}
		
		
		$this->_order->save();
		echo "Actualizado Correctamente";
	}
	    
	private function _get_order(){
		if ( empty($this->_order) || $this->_order == null ) {
		    $idr = $this->_return['collection']['external_reference'];
		    $ida = explode('-',$idr);
		    $this->_hash  = $ida[1];           
		    /// if is normal checkout (order is already created)

		    if ($ida[0] == 'mpexpress'){

				$preorder = Mage::getModel('sales/order')->loadByIncrementId($this->_hash); 
				if (isset($preorder['increment_id'])){
				    $this->_order = $preorder;
				}else{
				    echo 'Orden no encontrada.';
				    die;            
				}
			// else, if is checkout express, maybe order is not created		    
		    } else {
				$mpcart = Mage::getModel('mpexpress/mpcart')->load($this->_hash,'hash');  
				$this->_order_id = $mpcart->getOrderId();
				$this->_mpcartid = $mpcart->getMpexpressCartId();
				// If don´t have order, generate a order and send email
				if(is_null($this->_order_id) || empty($this->order_id)){
				    $this->_order_id = $mpcart->generateEmptyOrder($this->_mpcartid);  
				    $preorder = Mage::getModel('sales/order')->loadByIncrementId($this->_order_id); 
				    if (isset($preorder['increment_id'])){
						$this->_order = $preorder;
				    }else{
						echo 'Orden no encontrada.';
						die;            
				    }
				    $this->_sendemail = true;
				}else{
				    $preorder = Mage::getModel('sales/order')->loadByIncrementId($this->_order_id); 
				    if (isset($preorder['increment_id'])){
						$this->_order = $preorder;
				    }else{
						echo 'Orden no encontrada.';
						die;            
				    }
				}
		    }
		}
	}
	    
	    
	    
	/*public function notify($sendToName, $sendToEmail) {
		$store = Mage::app()->getStore();
		$store->getName();
		$link = '<a href="'. Mage::getBaseUrl() . 'mpexpress/information/address/hash/'.$this->_hash.'">'.Mage::getBaseUrl().'mpexpress/information/address/hash/'.$this->_hash.'</a>';
		$name = Mage::getStoreConfig('general/store_information/name');
		$from = Mage::getStoreConfig('trans_email/ident_general/email');
		$subject = Mage::helper('mpexpress')->__('Complete your order shipping information');
		$charset = '<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />';
		$dear = Mage::helper('mpexpress')->__('Thank you for your purchase.');
		$linha = ' <br /> ';        
		$information = Mage::helper('mpexpress')->__('To complete your order, if is not done yet, fill the address information at the address below.');        
		$finalmessage = $charset.$dear.$linha.$linha.$information.$linha.$linha.$link;
		$mail = Mage::getModel('core/email');
		$mail->setToName($sendToName);
		$mail->setToEmail($sendToEmail);
		$mail->setBody($finalmessage);
		$mail->setSubject('=?utf-8?B?'.base64_encode($subject).'?=');
		$mail->setFromEmail($from);
		$mail->setFromName($name);
		$mail->setType('html');
		
		try {
		    $mail->send();
		}
		catch (Exception $e) {
		    Mage::logException($e);
		    return false;
		}
		
		return true;
    }*/

    protected function _get_new_order_state($status){
    	$statuses  = Mage::getResourceModel('sales/order_status_collection')->joinStates()->addFieldToFilter('main_table.status', $status)->getFirstItem();                         
    	$state = $statuses->getState();
    	//esto esta porque no se puede cambiar por codigo al estado complete
    	if ($state == Mage_Sales_Model_Order::STATE_COMPLETE) $state = Mage_Sales_Model_Order::STATE_CANCELED;
        return $state;
    }

}
