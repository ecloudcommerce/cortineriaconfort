<?php

class Bl_Promopsp_Adminhtml_WidgetController extends Mage_Adminhtml_Controller_Action {

    public function chooserAction() {

        $request = $this->getRequest();

        switch ($request->getParam('attribute')) {
            case 'banco':
                $block = $this->getLayout()->createBlock(
                        'promopsp/widget_chooser_banco', 'promo_widget_chooser_banco', array('js_form_object' => $request->getParam('form'),
                        ));
                break;

            case 'tarjeta':
                $block = $this->getLayout()->createBlock(
                        'promopsp/widget_chooser_tarjeta', 'promo_widget_chooser_tarjeta', array('js_form_object' => $request->getParam('form'),
                        ));
                break;
            default:
                $block = false;
                break;
        }

        if ($block) {
            $this->getResponse()->setBody($block->toHtml());
        }
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('promo/catalog');
    }

}

