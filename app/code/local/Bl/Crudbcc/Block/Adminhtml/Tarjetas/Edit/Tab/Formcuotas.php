<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Tarjetas_Edit_Tab_Formcuotas extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset_banco = $form->addFieldset('crudbcc_form_cuota', array('legend' => Mage::helper('crudbcc')->__('Cuotas')));

        /* $fieldset_banco->addField('cuotas', 'text', array(
          'label' => Mage::helper('crudbcc')->__('Cuotas'),
          'name' => 'cuotas',
          )); */

        $fieldset_banco->addField('cuotas', 'text', array(
            'name' => 'cuotas',
            'label' => Mage::helper('crudbcc')->__('Cuotas'),
            'class' => 'requried-entry',
        ));

        $form->getElement('cuotas')->setRenderer(
                $this->getLayout()->createBlock('crudbcc/adminhtml_tarjetas_edit_tab_cuota_tier')
        );

        if (Mage::getSingleton('adminhtml/session')->getCrudbccData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCrudbccData());
            Mage::getSingleton('adminhtml/session')->setCrudbccData(null);
        } elseif (Mage::registry('crudbcc_data')) {
            $form->setValues(Mage::registry('crudbcc_data')->getData());
        }

        return parent::_prepareForm();
    }

}