<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute('order_address', 'npsfee',             array('type' => 'decimal',   'grid' => false));
$installer->addAttribute('order_address', 'base_npsfee',        array('type' => 'decimal',   'grid' => false));
$installer->endSetup();