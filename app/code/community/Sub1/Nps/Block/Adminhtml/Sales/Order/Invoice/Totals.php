<?php
class Sub1_Nps_Block_Adminhtml_Sales_Order_Invoice_Totals
    extends Mage_Adminhtml_Block_Sales_Order_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();
        
        $address    = $this->getSource()->getShippingAddress();
        $amount     = $address->getData('npsfee');
        $baseAmount = $address->getData('base_npsfee');
        if ($amount != 0) {
            $this->addTotal(new Varien_Object(
                array(
                    'code'       => 'npsfee',
                    'value'      => $amount,
                    'base_value' => $baseAmount,
                    'label'      => 'Costo de Financiación',
                ), 
                array('shipping'))
            );
        }
        return $this;
    }
}