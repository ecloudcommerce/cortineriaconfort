<?php

interface Sub1_Nps_Model_Payments_Interface {

    /**
     * Devuelve la coleccione de "Cuotas"
     */
    public function getFees(array $params);

    /**
     * Devuelve el indice de ajuste para el pago en x "Cuotas"
     */
    public function getFeeIndex(array $params);

    /**
     * Devuelve un array con las opciones de las tarjetas de credito
     */
    public function getCreditCardsOptions();

    public function preparePaymentData($data);
}
