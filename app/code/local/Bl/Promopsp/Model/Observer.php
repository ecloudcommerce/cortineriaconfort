<?php

class Bl_Promopsp_Model_Observer
{

    /**
     * Process sales rule before save
     *
     * @param   Varien_Event_Observer $observer
     */
    
    // Utilizo este observer para serializar las cuotas 
    //antes de guardarlas en la tabla salesrule
    public function saveBefore($observer)
    {
        $controllerAction = $observer->getRule()->getData();

        $data   = $observer->getRule()->getData();
        $cuotas = $observer->getRule()->getData('cuotas');

        $cuotas_tmp = array();

        /* Entra solamente si cuotas es un array (no aplica para massEnable o massDisabled) 
        *  Con esto evito que se limpien las cuotas al editar masivamente las promos
        */
        if(is_array($cuotas)){
            foreach($cuotas as $tc){
                if(!$tc['delete']){
                    unset($tc['delete']);
                    $cuotas_tmp[] = $tc;
                    if(is_numeric($tc['n_cuota'])){
                        if(strpos($tc['n_cuota'], ".") !== false){
                            Mage::throwException("El campo número de cuota no debe contener decimales.");
                        }
                    }else{
                        Mage::throwException("El campo número de cuota sólo acepta números enteros.");
                    }
                }
            }

            $data['cuotas'] = serialize($cuotas_tmp);
            $observer->getRule()->setData($data);
        }

    }

}