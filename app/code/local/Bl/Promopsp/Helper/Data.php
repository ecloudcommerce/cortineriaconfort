<?php

class Bl_Promopsp_Helper_Data extends Mage_Core_Helper_Abstract {

    public function simulateProductQuote(Mage_Catalog_Model_Product $product) {

        $quote = Mage::getModel('sales/quote');
        $quote->getShippingAddress()
                ->setCountryId('AR'); // Set your default shipping country here

        $product->getStockItem()->setUseConfigManageStock(false);
        $product->getStockItem()->setManageStock(false);

        $quote->addProduct($product);

        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->getShippingAddress()->collectTotals();
        
        $rates = $quote->getShippingAddress()->getShippingRatesCollection();

        // Find cheapest rate
        $cheapestrate = null;
        foreach ($rates as $rate) {
            if (is_null($cheapestrate) || $rate->getPrice() < $cheapestrate) {
                $cheapestrate = $rate->getPrice();
            }
        }
        $corehelper = Mage::helper('core');
        
        if ($cheapestrate) {
            //echo 'Shipping from: ' . $corehelper->currency($cheapestrate);
        }
        
        return $corehelper->currency($cheapestrate);
        
    }

    /*public function simulateCollectionQuote() {

        $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore()->getId());

        $attributes = Mage::getSingleton('catalog/config')
                ->getProductAttributes();

        $collection->addAttributeToSelect($attributes)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents();

        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $quote = Mage::getModel('sales/quote');
        $quote->getShippingAddress()
                ->setCountryId('AR');

        foreach ($collection as $product) {
            $product->getStockItem()->setUseConfigManageStock(false); // Disable checking of config manage stock
            $product->getStockItem()->setManageStock(false); // Pass the stock qty checking.
            $item = $quote->addProduct($product);
        }

        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->getShippingAddress()->collectTotals();
        $quote->getShippingAddress()->collectShippingRates();

        $rates = $quote->getShippingAddress()->getShippingRatesCollection();

        foreach ($rates as $rate) {
            echo $rate->getPrice();
        }
    }*/

}