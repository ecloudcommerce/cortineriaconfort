<?php

/**
 * @autor W3ITSolutions
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'merchant_id', 'varchar(255) CHARSET utf8 COLLATE utf8_general_ci NULL after `hasta_x_cuotas`');

$installer->endSetup();