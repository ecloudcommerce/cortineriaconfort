<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Promopsp_Block_Quote_Edit_Tab_Cuotas_Tier extends Mage_Adminhtml_Block_Widget implements Varien_Data_Form_Element_Renderer_Interface {

    public function __construct() {
        $this->setTemplate('promopsp/edit/tab/cuotas/tier.phtml');
    }

    protected function _prepareLayout() {
        
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
            'label'     => Mage::helper('promopsp')->__('Agregar Cuota'),
            'onclick'   => 'return tierCuotasControl.addItem()',
            'class'     => 'add'
        ));
                
        $button->setName('add_tier_cuotas_item_button');

        $this->setChild('add_button', $button);
        
        return parent::_prepareLayout();
        
    }

    public function getAddButtonHtml() {
        return $this->getChildHtml('add_button');
    }

    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        return $this->toHtml();
    }

    public function getValues() {
        return $this->getElement()->getValue();
    }

}
