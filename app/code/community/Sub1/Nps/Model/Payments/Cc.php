<?php

class Sub1_Nps_Model_Payments_Cc extends Sub1_Nps_Model_Payments implements Sub1_Nps_Model_Payments_Interface {

    protected $_sourceModel = 'npscc/cc';

    /**
     * Bl_Nps_Model_Payments_Interface Implementation
     * 
     * Devuelve los planes de cuotas para la Tarjeta en cuestion
     * 
     * @param array $params
     * @return array
     */
    public function getFees(array $params) {
        $result = array();
        $cc = $this->getCc($params['cc_id']);
        if (!$cc) {
            throw new Exception(Mage::helper('nps')->__('Invalid Credit Card'));
        }
        $fees = $cc->getFees();
        if (!count($fees) > 0) {
            throw new Exception(Mage::helper('nps')->__('The Credit Card has no fee options'));
        }
        return array(
            'data' => $fees,
        );
    }

    /**
     * Bl_Nps_Model_Payments_Interface Implementation
     *
     * @param array $params 
     */
//    public function getFeeCoef(array $params)
    public function getFeeIndex(array $params) {
        return $params['fee']['coef'];
    }

    public function preparePaymentData($data) {
        // Extra Data for Save
        if (isset($data['nps']['cc_id'])) {

            $ccData = $this->getCcData($data['nps']['cc_id']);
            $data['cc'] = $ccData;

            //@TODO REVISAR ESTO POR FAVOR!!!
            foreach ($ccData['fees'] as $fee) {
                if ($fee["payments"] == $data['nps']['cc_fees']) {
                    $data['fees'] = $fee;
                }
            }

            unset($data['cc']['fees']);
        } else {
            Mage::throwException('No se recibio informacion de la tarjeta de credito');
        }
        return $data;
    }

    /**
     * Devuelve el modelo de la tarjeta de credito segun ID
     * 
     * @param int $id
     * @return Bl_Npscc_Model_Cc
     */
    public function getCc($id) {
        return $this->_getSource()->load($id);
    }

    public function getCcBy($key, $value) {
        return $this->_getSource()->loadBy($key, $value);
    }

    /**
     * Devuelve un array con la data de la Tarjeta
     *
     * @param int $id
     * @return array
     */
    public function getCcData($id) {
        $data = $this->getCc($id);
        return ($data) ? $data->getData() : array();
    }

    /**
     * Devuelve todas las tarjetas
     * 
     * @param bool $loaded
     * @param bool $showAll
     * @return Bl_Npscc_Model_Mysql4_Cc_Collection
     */
    public function getCreditCards($loaded = true, $showAll = false) {
        $collection = $this->_getSource()->getCollection();
        if (!$showAll) {
            $collection->addFieldToFilter('fees', array('neq' => ''));
        }
        return ($loaded) ? $collection->load() : $collection;
    }

    /**
     * Filtra las Tarjetas autorizadas
     *
     * @todo Matchear contra la configuracion cuando este
     */
    public function getAllowedCreditCards() {
        $allowedIds = explode(',', $this->_getConfig('specificcreditcards'));
        $collection = $this->getCreditCards(false)
                ->addFieldToFilter('id', array('in' => $allowedIds));
        return $collection;
    }

    /**
     * 
     *
     * @return array
     */
    public function getCreditCardsOptions() {
        $result = array();
        $data = $this->getAllowedCreditCards()->getData();
        if ($data) {
            foreach ($data as $cc) {
                $result[$cc['id']] = array(
                    'value' => $cc['id'],
                    'label' => $cc['name'],
                );
            }
        }
        return $result;
    }

    /**
     *
     * 
     * @param array $data
     * @return array $result
     */
    protected function _proccessCreditCardData($data) {
        $result = array();
    }

}