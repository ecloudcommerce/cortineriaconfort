<?php
// @var $setup Mage_Eav_Model_Entity_Setup
$setup = $this;

$setup->startSetup();

$applyTo = array(
    Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
);

$setup->addAttribute('catalog_product', 'cantidad_bultos', array(
        'group'         => 'General',
        'type'          => 'int',
        'backend'       => '',
        'frontend'      => '',
        'class'         => '',
        'default'       => '1',
        'label'         => 'Cantidad de bultos',
        'input'         => 'text',
        'source'        => '',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'       => 1,
        'required'      => 0,
        'searchable'    => 0,
        'filterable'    => 1,
        'unique'        => 0,
        'comparable'    => 0,
        'visible_on_front'          => 0,
        'is_html_allowed_on_front'  => 1,
        'user_defined'  => 1,
        'apply_to'      => implode(',',$applyTo)
));

$setup->endSetup();