<?php
class Sub1_Nps_Block_Sales_Order_Total extends Mage_Sales_Block_Order_Totals
{
    /**
     * @return string
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }
    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }
    /**
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }
    /**
     * @return string
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }
    
    public function getAddress()
    {
        return $this->getSource()->getShippingAddress();
    }
    public function initTotals()
    {
        $amount     = $this->getAddress()->getData('npsfee');
        $baseAmount = $this->getAddress()->getData('base_npsfee');
        
        if ((float) $amount) {
            
            $isCreditmemeo = ($this->getSource() instanceof Mage_Sales_Model_Order_Creditmemo);
            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'       => 'sub1_nps',
                'strong'     => false,
                'value'      => $isCreditmemeo ? - $amount : $amount,
                'base_value' => $isCreditmemeo ? - $baseAmount : $baseAmount,
                'label'      => 'Costo de Financiación',
            )));
        }
        return $this;
    }
}