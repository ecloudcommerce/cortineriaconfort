<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?>
<?php
class Ecloud_Oca_Model_Carrier_Oca extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {  

    protected $_code = '';
    protected $distancia_final_txt  = '';
    protected $duracion_final       = '';
    protected $mode  = '';
    protected $envio = '';
    protected $api;

    /** 
    * Recoge las tarifas del método de envío basados ​​en la información que recibe de $request
    * 
    * @param Mage_Shipping_Model_Rate_Request $data 
    * @return Mage_Shipping_Model_Rate_Result 
    */ 

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {

        Mage::getSingleton('core/session')->unsOca();

        $datos = $this->getShippingDetails($request);

        // Seteamos las reglas
        if(isset($datos["freeBoxes"]))
            $this->setFreeBoxes($datos["freeBoxes"]);

        $datos["username"]      = $this->getOcaConfigData('carriers/ocaconfig/cliente_user');
        $datos["password"]      = $this->getOcaConfigData('carriers/ocaconfig/cliente_password');
        $datos["nro_cuenta"]    = $this->getOcaConfigData('carriers/ocaconfig/cliente_cuenta');
        $datos["cuit"]          = $this->getOcaConfigData('carriers/ocaconfig/cliente_cuit');
        $datos["cpOrigen"]      = $this->getOcaConfigData('carriers/ocaconfig/retiro_codpostal');
        $datos["tipoPedido"]    = $this->getOcaConfigData('carriers/ocaconfig/paquete_tipo');

        
        $cart   = Mage::getSingleton('checkout/cart');
        $quote  = $cart->getQuote();
        $shippingAddress        = $quote->getShippingAddress();
        $datos["cpDestino"]     = $request->getDestPostcode();
        $datos["localidad"]     = $request->getDestCity();
        $datos["provincia"]     = $request->getDestRegionCode();
        $datos["direccion"]     = $request->getDestStreet();
        $datos["nombre"]        = $shippingAddress->getData('firstname');
        $datos["apellido"]      = $shippingAddress->getData('lastname');
        $datos["telefono"]      = $shippingAddress->getData('telephone');
        $datos["email"]         = $shippingAddress->getData('email');

        $result = Mage::getModel('shipping/rate_result');

        // Optimizacion con OneStepCheckout
        $error_msg = Mage::helper('oca')->__("Completá los datos para poder calcular el costo de su pedido.");
        if ($datos["cpDestino"]=="" && $datos["localidad"]=="" && $datos["provincia"]=="" && $datos["direccion"]=="") {
            $error = Mage::getModel('shipping/rate_result_error'); 
            $error->setCarrier($this->_code); 
            $error->setCarrierTitle($this->getConfigData('title')); 
            $error->setErrorMessage($error_msg); 
            return $error;
        }

        if ($this->_code == "ocaestandardomicilio" && $this->getOcaConfigData('carriers/ocaestandardomicilio/active') == 1) {
            $response = $this->_getOcaEstandarDomicilio($datos,$request);
            if(is_string($response)){
                $error = Mage::getModel('shipping/rate_result_error'); 
                $error->setCarrier($this->_code); 
                $error->setCarrierTitle($this->getConfigData('title')); 
                $error->setErrorMessage($response); 
                return $error;
            } else {
                $result->append($response);
            }
        }

        if ($this->_code == "ocaestandarsucursal" && $this->getOcaConfigData('carriers/ocaestandarsucursal/active') == 1) {
            $response = $this->_getOcaEstandarSucursal($datos,$request);
            if(is_string($response)){
                $error = Mage::getModel('shipping/rate_result_error'); 
                $error->setCarrier($this->_code); 
                $error->setCarrierTitle($this->getConfigData('title')); 
                $error->setErrorMessage($response); 
                return $error;
            } else {
                foreach ($response as $rate) {
                    $result->append($rate);
                }
            }
        }

        if ($this->_code == "ocaprioritariodomicilio" && $this->getOcaConfigData('carriers/ocaprioritariodomicilio/active') == 1) {
            $response = $this->_getOcaPrioritarioDomicilio($datos,$request);
            if(is_string($response)){
                $error = Mage::getModel('shipping/rate_result_error'); 
                $error->setCarrier($this->_code); 
                $error->setCarrierTitle($this->getConfigData('title')); 
                $error->setErrorMessage($response); 
                return $error;
            } else {
                $result->append($response);
            }
        }

        if ($this->_code == "ocaprioritariosucursal" && $this->getOcaConfigData('carriers/ocaprioritariosucursal/active') == 1) {
            $response = $this->_getOcaPrioritarioSucursal($datos,$request);
            if(is_string($response)){
                $error = Mage::getModel('shipping/rate_result_error'); 
                $error->setCarrier($this->_code); 
                $error->setCarrierTitle($this->getConfigData('title')); 
                $error->setErrorMessage($response); 
                return $error;
            } else {
                foreach ($response as $rate) {
                    $result->append($rate);
                }
            }
        }

        return $result;
    }  

    /** 
    * Arma el precio y la información del servicio "Estandar Domicilio" de Oca según el parametro $data
    * 
    * @param Datos del usuario y el carrito de compras $data 
    * @return Los datos para armar el Método de envío $rate 
    */  
    protected function _getOcaEstandarDomicilio($datos,$request){

        $rate = Mage::getModel('shipping/rate_result_method');
        $datos["operatoria"] = $this->getOcaConfigData('carriers/ocaestandardomicilio/operatoria');

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            $price = '0.00';
            $rate->setMethodTitle(Mage::helper('oca')->__('Envío gratis.'));
        } else { 
            $cotizacion = $this->getApi()->getTarifarEnvio($datos);
            if (!is_array($cotizacion)) {
                return 'Error en la conexión con OCA';
            }
            $price = (float)@$cotizacion['Total'];
            $price = $price + ($price * $this->getOcaConfigData('carriers/ocaestandardomicilio/regla') / 100);
            $datos["precio"] = $price;
            $title = $this->getOcaConfigData('carriers/ocaestandardomicilio/description');
            $price = $this->getFinalPriceWithHandlingFee($price);
            $rate->setMethodTitle($title);
        }

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Oca");
        $rate->setMethod($this->_code);
        $rate->setPrice($price);

        Mage::getSingleton('core/session')->setOcaEstandarDomicilio($datos);
        return $rate;

    }

    /** 
    * Arma el precio y la información del servicio "Estandar Sucursal" de Oca según el parametro $data
    * 
    * @param Datos del usuario y el carrito de compras $data 
    * @return Los datos para armar el Método de envío $rate 
    */  
    protected function _getOcaEstandarSucursal($datos,$request){

        $datos["operatoria"] = $this->getOcaConfigData('carriers/ocaestandarsucursal/operatoria');

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            $price = '0.00';
        }else{
            $cotizacion = $this->getApi()->getTarifarEnvio($datos);
            if (!is_array($cotizacion)) {
                return 'Error en la conexión con OCA';
            }
            $price = (float)@$cotizacion['Total'];
            $price = $price + ($price * $this->getOcaConfigData('carriers/ocaestandarsucursal/regla') / 100);
            $datos["precio"] = $price;
            $price = $this->getFinalPriceWithHandlingFee($price);
        }
        
        $title = $this->getOcaConfigData('carriers/ocaestandarsucursal/description');
        $rates = array();
        $sucursales = $this->getApi()->getSucursalesPorCP($datos['cpDestino']);
        if(is_string($sucursales)){
            // Viene un msj de error
            return $sucursales;
        }
        foreach ($sucursales as $sucursal) {
            $rate = Mage::getModel('shipping/rate_result_method');
            $rate->setCarrier($this->_code);
            $rate->setCarrierTitle('Oca');
            $rate->setMethod($this->_code . $sucursal['idCentroImposicion']);
            if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
                $title = Mage::helper('oca')->__('Envío gratis.');
            }
            // Envio el idCentroImposicion en el codigo del metodo
            $rate->setMethodTitle($title.' Direccion sucursal: '.$sucursal['Calle'].' '.$sucursal['Numero'].' - '.$sucursal['Localidad']);
            $rate->setPrice($price);
            $rates[] = $rate;
            Mage::getSingleton('core/session')->setOcaEstandarSucursal($datos);
        }
        return $rates;
    }

    /** 
    * Arma el precio y la información del servicio "Prioritario Domicilio" de Oca según el parametro $data
    * 
    * @param Datos del usuario y el carrito de compras $data 
    * @return Los datos para armar el Método de envío $rate 
    */  
    protected function _getOcaPrioritarioDomicilio($datos,$request){

        $rate = Mage::getModel('shipping/rate_result_method');
        $datos["operatoria"] = $this->getOcaConfigData('carriers/ocaprioritariodomicilio/operatoria');

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            $price = '0.00';
            $rate->setMethodTitle(Mage::helper('oca')->__('Envío gratis.'));
        } else { 
            $cotizacion = $this->getApi()->getTarifarEnvio($datos);
            if (!is_array($cotizacion)) {
                return 'Error en la conexión con OCA';
            }
            $price = (float)@$cotizacion['Total'];
            $price = $price + ($price * $this->getOcaConfigData('carriers/ocaprioritariodomicilio/regla') / 100);
            $datos["precio"] = $price;
            $title = $this->getOcaConfigData('carriers/ocaprioritariodomicilio/description');
            $price = $this->getFinalPriceWithHandlingFee($price);
            $rate->setMethodTitle($title);
        }

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Oca");
        $rate->setMethod($this->_code);
        $rate->setPrice($price);

        Mage::getSingleton('core/session')->setOcaPrioritarioDomicilio($datos);
        return $rate;

    }

    /** 
    * Arma el precio y la información del servicio "Prioritario Sucursal" de Oca según el parametro $data
    * 
    * @param Datos del usuario y el carrito de compras $data 
    * @return Los datos para armar el Método de envío $rate 
    */  
    protected function _getOcaPrioritarioSucursal($datos,$request){

        $datos["operatoria"] = $this->getOcaConfigData('carriers/ocaprioritariosucursal/operatoria');

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            $price = '0.00';
        }else{
            $cotizacion = $this->getApi()->getTarifarEnvio($datos);
            if (!is_array($cotizacion)) {
                return 'Error en la conexión con OCA';
            }
            $price = (float)@$cotizacion['Total'];
            $price = $price + ($price * $this->getOcaConfigData('carriers/ocaprioritariosucursal/regla') / 100);
            $datos["precio"] = $price;
            $price = $this->getFinalPriceWithHandlingFee($price);
        }
        
        $title = $this->getOcaConfigData('carriers/ocaprioritariosucursal/description');
        $rates = array();
        $sucursales = $this->getApi()->getSucursalesPorCP($datos['cpDestino']);
        if(is_string($sucursales)){
            // Viene un msj de error
            return $sucursales;
        }
        foreach ($sucursales as $sucursal) {
            $rate = Mage::getModel('shipping/rate_result_method');
            $rate->setCarrier($this->_code);
            $rate->setCarrierTitle('Oca');
            $rate->setMethod($this->_code . $sucursal['idCentroImposicion']);
            if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
                $title = Mage::helper('oca')->__('Envío gratis.');
            }
            // Envio el idCentroImposicion en el codigo del metodo
            $rate->setMethodTitle($title.' Direccion sucursal: '.$sucursal['Calle'].' '.$sucursal['Numero'].' - '.$sucursal['Localidad']);
            $rate->setPrice($price);
            $rates[] = $rate;
            Mage::getSingleton('core/session')->setOcaPrioritarioSucursal($datos);
        }
        return $rates;

    }

    /**
    * Devuelve peso, volumen, valor declarado del envio
    */
    public function getShippingDetails($ship){
        $masAlto                    = 0;
        $masAncho                   = 0;
        $largoTotal                 = 0;
        $datos["peso"]              = 0;
        $datos["valorDeclarado"]    = 0;
        $datos["volumen"]           = 0;
        $datos["freeBoxes"]         = 0;
        $datos["bultos"]            = 0;
        $sku                        = '';
        $tipoPedido                 = $this->getOcaConfigData('carriers/ocaconfig/paquete_tipo');
        // Tomamos el attr "medida" segun la configuracion del cliente
        if ($this->getOcaConfigData('carriers/ocaconfig/global_medida') == "") {
            $datos["medida"] = "gramos";
        } else {
            $datos["medida"] = $this->getOcaConfigData('carriers/ocaconfig/global_medida');
        }
        if ($datos["medida"]=="kilos") {
            $datos["medida"] = 1;
        } elseif ($datos["medida"]=="gramos") {
            $datos["medida"] = 1000;
        } else {
            $datos["medida"] = 1000;
        }
        foreach ($ship->getAllItems() as $_item) {
            if($sku != $_item->getSku()) {
                $sku                     = $_item->getSku();
                $price                   = floor($_item->getPrice());
                $datos["peso"]           = ($_item->getQty() * $_item->getWeight() / $datos["medida"]) + $datos["peso"];
                $datos["valorDeclarado"] = ($_item->getQty() * $price) + $datos["valorDeclarado"];
                
                $product    = Mage::getModel('catalog/product')->loadByAttribute('sku', $_item->getSku(), array('paquete_largo','paquete_ancho','paquete_alto','cantidad_bultos'));
                $pkgQty     = (float)$product->getData('cantidad_bultos');
                $datos["bultos"] = $datos["bultos"] + ($pkgQty) ? $pkgQty : 1;

                if($tipoPedido == 2){
                    $alto       = (float)$product->getData('paquete_alto');
                    $ancho      = (float)$product->getData('paquete_ancho');
                    $largo      = (float)$product->getData('paquete_largo');
                    $masAlto    = ($masAlto < $alto) ? $alto : $masAlto;
                    $masAncho   = ($masAncho < $ancho) ? $ancho : $masAncho;
                    $largoTotal += $largo * $_item->getQty();
                }
                
                // Si la condicion de free shipping está seteada en el producto
                if ($_item->getFreeShippingDiscount() && !$_item->getProduct()->isVirtual()) {
                    $datos["freeBoxes"] += $_item->getQty();
                }
            }
        }

        if($tipoPedido == 1){
            $datos["volumen"] = $this->getOcaConfigData('carriers/ocaconfig/paquete_fix_alto')
                                    * $this->getOcaConfigData('carriers/ocaconfig/paquete_fix_ancho')
                                    * $this->getOcaConfigData('carriers/ocaconfig/paquete_fix_largo');

            $datos["bultos"]  = 1;
        }else{
            $datos["volumen"] = $masAlto * $masAncho * $largoTotal;
        }

        return $datos;
        
    }

    /**
     * Devuelve el model api para hacer las llamadas
     */
    protected function getApi(){
        if(!$this->api) {
            $this->api = Mage::getModel('oca/apicall');
        }
        return $this->api;
    }

    /**
     * Get config data for field
     *
     * @param string $field
     * @return string
     */
    protected function getOcaConfigData($field){
        return Mage::getStoreConfig($field,Mage::app()->getStore());
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods() {
        return array($this->_code    => $this->getConfigData('name'));
    }
}
?>
