<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Bl_Promopsp_Block_Product_Calculator extends Mage_Catalog_Block_Product_Abstract {

    protected $_feesAjaxUrl = 'promopsp/index/getFees';
    protected $_banks;

    public function getFeesAjaxUrl() {
        return $this->getUrl($this->_feesAjaxUrl);
    }

    public function getBanksJson() {
        return json_encode($this->getBanks());
    }

    public function getBanks() {
        if (!$this->_banks) {
            $this->_banks = $this->getPaymentsModel()->getBanks();
        }
        return $this->_banks;
    }

    public function getPaymentsModel() {
        return $this->getStandard()->getPaymentsModel();
    }

    public function getStandard() {
        //return Mage::getModel('nps/standard');
        return Mage::getModel('nps/nps');
    }

}