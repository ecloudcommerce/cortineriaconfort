<?php
/**
 * @version   0.1.0
 * @author    ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?><?php
class Ecloud_Oca_Model_Apicall extends Mage_Core_Model_Abstract
{

	protected $_client;
    /**
     * Cotiza el envio de los productos segun los parametros
     *
     * @param $params 
     * @return $result array tal cual oca or null en error
     */
    public function getTarifarEnvio($params) {
        try {
            $client = $this->getClient();

            $response = $client->Tarifar_Envio_Corporativo(
                array(
                    'Operativa'           => $params["operatoria"],
                    'CodigoPostalDestino' => $params["cpDestino"],
                    'CodigoPostalOrigen'  => $params["cpOrigen"],
                    'Cuit'                => $params["cuit"],
                    'PesoTotal'           => $params["peso"],
                    'VolumenTotal'        => $params["volumen"],
                    'CantidadPaquetes'    => $params["bultos"],
                )
            );

            $responseString = $this->_parseResponse($response->Tarifar_Envio_CorporativoResult->any);

            return $responseString;

        } catch (SoapFault $e) {
            Mage::log("Error: " . $e);
        }
        return null;
    }

    /**
     * Obtiene los centros de imposicion correspondientes a un CP
     */
    public function getSucursalesPorCP($cp){
        try {
            $client = $this->getClient();
            $response = $client->GetCentrosImposicionPorCP(
                array(
                    'CodigoPostal'        => $cp,
                )
            );

            $responseString = $this->_parseResponse($response->GetCentrosImposicionPorCPResult->any);
            if(count($responseString) < 1)
                return 'Error: El código postal es inválido o no se encontraron sucursales cercanas';

            if(!$this->isMulti($responseString)){
                $temp = $responseString;
                $responseString = array();
                $responseString[] = $temp;
            }

            return $responseString;

        } catch (SoapFault $e) {
            Mage::log("Error: " . $e);
        }
        return null;        
    }

    /**
     * get tracking de una pieza
     */
    public function getTrackingPieza($nroPieza) {
        try {
            $client = $this->getClient();

            $response = $client->Tracking_Pieza(
                array(
                    'Pieza'           => $nroPieza,
                )
            );

            $responseString = $this->_parseResponse($response->Tracking_PiezaResult->any);

            if(!$this->isMulti($responseString)){
                $temp = $responseString;
                $responseString = array();
                $responseString[] = $temp;
            }
            
            return $responseString;

        } catch (SoapFault $e) {
            Mage::log("Error: " . $e);
        }
        return null;
    }

	/**
     * Ingreso el envio a la api de OCA
     */
    public function generarIngresoOR($datos,$credentials){
		try {
			$client = $this->getClient();

			$xml_retiro = $this->generateXML_Retiro($datos);

            $params = array(
                    'usr'               => $credentials['username'],
                    'psw'               => $credentials['password'],
                    'XML_Retiro'        => $xml_retiro,
                    'ConfirmarRetiro'   => true, 
                    'DiasRetiro'        => 1, //@VERCONOCA
                    'FranjaHoraria'     => $credentials['franja_horaria'],
                );

			$response = $client->IngresoOR($params);

            $responseString = @$response->IngresoORResult->any;

            // XML
            $responseXml = simplexml_load_string($responseString);
            $responseArray = $this->_proccessXmlMultipleData((array)$responseXml);
            $result = isset($responseArray['Resultado']) ? $responseArray['Resultado'] : array('Error' => array('Descripcion' => 'XML mal formado: '.var_export($responseArray, true)));
            // Case Error
            if (isset($result['Error'])) {
                Mage::throwException(Mage::helper('oca')->__('OCA Web Service Error: %s.', $result['Error']['Descripcion']));
            }

            return $result;

		} catch (SoapFault $e) {
			Mage::log("Error: " . $e);
			Mage::throwException(Mage::helper('oca')->__('Algo ha ido mal con la conexión a OCA. Intente nuevamente.'));
		}
    }


    /**
     * Parsea la "garcha" que manda OCA
     */
    protected function _parseResponse($response){
        $xml = new SimpleXMLElement($response);
        
        if ($result = @$xml->NewDataSet->Table) {
            if (count($result) > 1) {
                $return = array();
                foreach ($result as $item) {
                    $return[] = $this->_proccessXmlMultipleData((array)$item);
                }
                return $return;
            }
            return (array) $result;
        }
        return null;
    }

    /**
     * Parser Recusrsivo para resultados con multiples objetos
     */
    protected function _proccessXmlMultipleData(array $items){
        $result = array();
        foreach ($items as $k => $item) {
            switch (true) {
                case is_string($item):
                    $result[$k] = trim($item);
                    break;
                case is_array($item):
                    $result[$k] = $this->_proccessXmlMultipleData($item);
                    break;
                case $item instanceof SimpleXMLElement:
                    $result[$k] = $this->_proccessXmlMultipleData((array)$item);
                    break;
            }
        }
        return $result;
    }

    /**
     * Genera el XMl_Retiro para el metodo "ingresoOR"
     */
    protected function generateXML_Retiro($datos){
        // FORMAT XML
        $xml = new SimpleXMLElement('<ROWS></ROWS>');
        // Cabecera
        $nro_cuenta = Mage::getStoreConfig('carriers/ocaconfig/cliente_cuenta',Mage::app()->getStore());
        $cabecera = $xml->addChild('cabecera');
        $cabecera->addAttribute('ver','1.0');
        $cabecera->addAttribute('nrocuenta',$nro_cuenta);

        // Retiro
        $retiro = $xml->addChild('retiro');
        foreach ($this->_getRetiroDatos() as $k => $v) {
            $clean = '';
            if (is_array($v)) {
                $tmp = array();
                foreach ($v as $simple) {
                    $html = htmlentities($simple, ENT_COMPAT, 'UTF-8');
                    $html = str_replace($this->htmlSpecialChars, '-', $html);
                    $tmp[] = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
                }
                $clean = implode(' ', $tmp);
            } else {
                $html = htmlentities($v, ENT_COMPAT, 'UTF-8');
                $html = str_replace($this->htmlSpecialChars, '-', $html);
                $clean = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
            }
            $retiro->addAttribute($k, $clean);
        }

        // Envios
        $envios = $xml->addChild('envios');

        foreach ($datos as $pedido) {
            // Envio
            $envio = $envios->addChild('envio');
            $envio->addAttribute('idoperativa', $pedido['operatoria']);
            $envio->addAttribute('nroremito', $pedido['order_increment_id']);
            // Destinatario
            $destinatario = $envio->addChild('destinatario');
            foreach ($this->_getDestinatarioDatos($pedido) as $k => $v) {
                $clean = '';
                if (is_array($v)) {
                    $tmp = array();
                    foreach ($v as $simple) {
                        $html = htmlentities($simple, ENT_COMPAT, 'UTF-8');
                        $html = str_replace($this->htmlSpecialChars, '-', $html);
                        $tmp[] = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
                    }
                    $clean = implode(' ', $tmp);
                } else {
                    $html = htmlentities($v, ENT_COMPAT, 'UTF-8');
                    $html = str_replace($this->htmlSpecialChars, '-', $html);
                    $clean = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
                }
                $destinatario->addAttribute($k, $clean);
            }
            // Paquetes
            $packages = $this->_getPaqueteDatos($pedido);
            $paquetes = $envio->addChild('paquetes');
            foreach ($packages as $pkg) {
                $paquete = $paquetes->addChild('paquete');
                foreach ($pkg as $k => $v) {
                    $clean = '';
                    if (is_array($v)) {
                        $tmp = array();
                        foreach ($v as $simple) {
                            $html = htmlentities($simple, ENT_COMPAT, 'UTF-8');
                            $html = str_replace($this->htmlSpecialChars, '-', $html);
                            $tmp[] = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
                        }
                        $clean = implode(' ', $tmp);
                    } else {
                        $html = htmlentities($v, ENT_COMPAT, 'UTF-8');
                        $html = str_replace($this->htmlSpecialChars, '-', $html);
                        $clean = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
                    }
                    $paquete->addAttribute($k, $clean);
                }
            }
        }
        $result = $xml->asXML();
        return trim(preg_replace('/>\s\s*</', '><', $result));
    }

    /**
     * Devuelve un array con los paramétros del nodo "retiro" 
     * para metodo "ingresoOR"
     */
    protected function _getRetiroDatos(){
        $street = $this->_splitStreet($this->getConfigData('carriers/ocaconfig/retiro_calle'));
        $result = array(
            'calle'         => $street[0],
            'nro'           => $this->getConfigData('carriers/ocaconfig/retiro_numero'),
            'piso'          => $this->getConfigData('carriers/ocaconfig/retiro_piso'),
            'depto'         => $this->getConfigData('carriers/ocaconfig/retiro_depto'),
            'cp'            => $this->getConfigData('carriers/ocaconfig/retiro_codpostal'),
            'localidad'     => $this->getConfigData('carriers/ocaconfig/retiro_localidad'),
            'provincia'     => $this->getConfigData('carriers/ocaconfig/retiro_provincia'),
            'contacto'      => $this->getConfigData('carriers/ocaconfig/cliente_contacto'),
            'email'         => $this->getConfigData('carriers/ocaconfig/cliente_email'),
            'solicitante'   => $this->getConfigData('carriers/ocaconfig/cliente_razonsocial'),
            'observaciones' => ($this->isTestMode()) ? '-- NO PROCESAR ENVIO ES UNA PRUEBA --' : @$street[1],
            'centrocosto'   => '1' //VERCONOCA,
        );
        return $result;
    }

    /**
     * Devuelve un array con los paramétros del nodo "destinatario" 
     * para metodo "ingresoOR"
     */
    protected function _getDestinatarioDatos($datos){
        $street = $this->_splitStreet($datos['direccion']);
        $result = array(
            'calle'         => $street[0],
            'apellido'      => $datos['apellido'],
            'nombre'        => $datos['nombre'],
            'nro'           => '-',
            'cp'            => $datos['cp_destino'],
            'localidad'     => $datos['localidad'],
            'provincia'     => $datos['provincia'],
            'telefono'      => $datos['telefono'],
            'email'         => $datos['email'],
            'celular'       => '', //VERCOMOIMPLEMENTAR
            'observaciones' => ($this->isTestMode()) ? '-- NO PROCESAR ENVIO ES UNA PRUEBA --' : @$street[1],
            'idci'          => $datos['sucursal_retiro'], //VERCONOCA,
        );
        return $result;
    }

    /**
     * Devuelve un array con los paramétros del nodo "paquetes" 
     * para metodo "ingresoOR"
     */

    protected function _getPaqueteDatos($datos){
        $result = array();
        if($datos['tipo_pedido'] == 1){
            // si es un paquete por pedido
            $result[] = array(
                'alto'  => $this->getConfigData('carriers/ocaconfig/paquete_fix_alto'),
                'ancho' => $this->getConfigData('carriers/ocaconfig/paquete_fix_ancho'),
                'largo' => $this->getConfigData('carriers/ocaconfig/paquete_fix_largo'),
                'peso'  => $datos['peso'],
                'valor' => $datos['valor_declarado'],
                'cant'  => 1,
            );
        }elseif ($datos['tipo_pedido'] == 2) {
            // si es un paquete por producto
            $sku   = '';
            $order = Mage::getModel('sales/order')->load($datos['id_orden']);
            $items = $order->getAllItems();
            foreach ($items as $item) {
                if($sku != $item->getSku()){
                    $sku = $item->getSku();
                    $product    = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku, array('paquete_largo','paquete_ancho','paquete_alto','weight','price','cantidad_bultos'));
                    
                    $pkgQty     = (float)$product->getData('cantidad_bultos');
                    $pkgQty     = ($pkgQty) ? $pkgQty : 1;

                    $result[] = array(
                        'alto'  => (float)$product->getData('paquete_alto'),
                        'ancho' => (float)$product->getData('paquete_ancho'),
                        'largo' => (float)$product->getData('paquete_largo'),
                        'peso'  => (float)$product->getData('weight'),
                        'valor' => $product->getData('price'),
                        'cant'  => $item->getQtyOrdered() * $pkgQty,
                    );
                }
            }
        }

        return $result;
    }

    /**
     * Corta el campo calle para no exceder el numero de caracteres limite
     */
    protected function _splitStreet($street, $limit = 30){
        $result     = array();
        $address    = is_array($street) ? implode(' ', $street) : $street;
        $html       = htmlentities($address, ENT_COMPAT, 'UTF-8');
        $html       = str_replace($this->htmlSpecialChars, '-', $html);
        $address    = preg_replace("/&([a-z])[a-z]+;/i", "$1", $html);
        $length     = $limit-1;
        $result[]   = substr($address, 0, $length);
        $result[]   = substr($address, $length);
        return $result;
    }

    /**
     * Chequea si esta habilitado el Modo testeo
     */
    public function isTestMode(){
        return (bool)$this->getConfigData('carriers/ocaconfig/global_testmode');
    }

    /**
     * Get store config for current store
     */
    protected function getConfigData($field){
        return Mage::getStoreConfig($field,Mage::app()->getStore());
    }

    /**
     * Seteo cliente soap (si no está) y lo devuelvo
     */
    protected function getClient(){
        if (!$this->client){
            $url            = $this->getConfigData('carriers/ocaconfig/url_webservice');

            $optionsClient  = array(
                'trace'         => 1,
                'exception'     => 1,
                'soap_version'  => SOAP_1_2
            );

            $this->client = new SoapClient($url, $optionsClient);
        }

        return $this->client;       
    }

    /**
     * Formatea el numero a 2 decimales
     */
    protected function format2Decimals($number){
        return number_format($number,2);
    }

    /**
     * Chequea si es array of arrays
     */
    public function isMulti($value){
        return (isset($value[0]));
    }

    protected $htmlSpecialChars = array(
        '&deg;', '&amp;', '&lt;', '&gt;', '&trade;', '&reg;', '&nbsp;', '&quot;', '&pound;', '&curren;',
        '&brvbar;', '&sect;', '&copy;', '&ordf;', '&laquo;', '&not;', '&shy;', '&macr;', '&plusmn;',
        '&sup2;', '&sup3;', '&micro;', '&para;', '&middot;', '&sup1;', '&ordm;', '&raquo;', '&frac14;',
        '&frac12;', '&frac34;', '&iquest;', '&THORN;', '&Scaron;', '&scaron;', '&fnof;', '&circ;', '&tilde;',
        '&upsih;', '&piv;', '&ensp;', '&emsp;', '&thinsp;', '&zwnj;', '&zwj;', '&lrm;', '&rlm;', '&ndash;','&mdash;',
        '&lsquo;', '&rsquo;', '&sbquo;', '&ldquo;', '&rdquo;', '&bdquo;', '&dagger;', '&Dagger;', '&bull;',
        '&hellip;', '&permil;', '&prime;', '&Prime;', '&lsaquo;', '&rsaquo;', '&frasl;', '&image;', '&weierp;',
        '&larr;', '&uarr;', '&rarr;', '&darr;', '&harr;', '&crarr;', '&lArr;', '&uArr;', '&rArr;', '&dArr;', '&hArr;',
        '&forall;', '&part;', '&empty;', '&isin;', '&notin;', '&ni;', '&prod;', '&sum;', '&minus;', '&lowast;',
        '&radic;', '&prop;', '&infin;', '&ang;', '&and;', '&or;', '&cap;', '&cup;', '&int;', '&there4;', '&sim;',
        '&cong;', '&asymp;', '&ne;', '&equiv;', '&le;', '&ge;', '&sub;', '&sup;', '&nsub;', '&sube;', '&supe;',
        '&oplus;', '&otimes;', '&perp;', '&sdot;', '&lceil;', '&rceil;', '&lfloor;', '&rfloor;', '&lang;', '&rang;',
        '&loz;', '&spades;', '&clubs;', '&hearts;', '&diams;'
    );
}
?>