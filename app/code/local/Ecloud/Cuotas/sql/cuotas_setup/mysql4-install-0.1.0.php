<?php

$installer = $this;

//$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup = Mage::getResourceModel('catalog/eav_mysql4_setup','core_setup');

$installer->startSetup();

/**
 * Adding Different Attributes
 */
 
// the attribute added will be displayed under the group/tab Special Attributes in product edit page

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY,'cuotas',
	array(
		'group'=>'General',
		'type'=>'text',
		'label'=>'Cuotas',
		'input'=>'text',
		'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'=>true,
		'required'=>false,
		'user_defined'=>true
	));
 



$installer->endSetup();