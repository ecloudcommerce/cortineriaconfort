<?php

/**
 * @autor W3ITSolutions
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'promo_code', 'varchar(255) CHARSET utf8 COLLATE utf8_general_ci NULL after `weekdays`');

$installer->endSetup();