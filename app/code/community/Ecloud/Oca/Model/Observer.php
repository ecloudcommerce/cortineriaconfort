<?php
/**
 * @version   0.1.0
 * @author	ecloud solutions http://www.ecloudsolutions.com <info@ecloudsolutions.com>
 * @copyright Copyright (C) 2010 - 2015 ecloud solutions ®
 */
?>
<?php
class Ecloud_Oca_Model_Observer extends Mage_Core_Model_Session_Abstract {

	/**
	* Llama a la funcion cuando la orden fue creada luego del
	* Checkout y almacena los datos en la tabla "oca_order"
	*/
	public function guardarDataOrden($observer) {
		try {
			//  Tomamos todos los datos de la orden
			$metodoenvio = $observer->getEvent()->getOrder()->getShippingMethod();
			if(!$datos = Mage::helper('oca')->getOcaData($metodoenvio)){
				// Orden no es enviada por OCA
				return;
			}
			// fix. setteamos datos de ship porque si la orden viene de admin, vienen vacios
			$ship = $observer->getEvent()->getOrder()->getShippingAddress();
			$datos['nombre']	= $ship->getFirstname();
			$datos['apellido']	= $ship->getLastname();
			$datos['telefono']	= $ship->getTelephone();
			$datos['email']		= $ship->getEmail();
			
			// 2. Buscamos el ID de la orden y increment id
			$OrderId	= $observer->getEvent()->getOrder()->getId();
			$OrderIncId = $observer->getEvent()->getOrder()->getIncrementId();

			// 3. Los almacenamos en la tabla "oca_order"
			$_dataSave = (array(
						'id_orden'				=> intval($OrderId),
						'order_increment_id'	=> $OrderIncId,
						'operatoria'			=> $datos['operatoria'],
						'nro_cuenta'			=> $datos['nro_cuenta'],
						'direccion'				=> $datos['direccion'],
						'localidad'				=> $datos['localidad'],
						'provincia'				=> $datos['provincia'],
						'cp_destino'			=> $datos['cpDestino'],
						'sucursal_retiro'		=> ($datos['sucursal_retiro']) ? $datos['sucursal_retiro'] : 0,
						'nombre'				=> $datos['nombre'],
						'apellido'				=> $datos['apellido'],
						'telefono'				=> $datos['telefono'],
						'email'					=> $datos['email'],
						'precio'				=> $datos['precio'],
						'valor_declarado'		=> $datos['valorDeclarado'],
						'volumen'				=> $datos['volumen'],
						'peso'					=> $datos['peso'],
						'tipo_pedido'			=> $datos['tipoPedido'],
						'estado'				=> 'Pendiente'
					));

			$newOrder = Mage::getModel('oca/order')->addData($_dataSave);
			$newOrder->save();

			} catch (Exception $e) {
				Mage::log("Error: " . $e);
			}
		}

	/**
	* Llama a la funcion cuando desde el Admin Panel
	* se ejecuta el "Ship" y luego "Submit Shipment"
	*/
	public function generarRetiro($observer) {

		// Si entramos al observer mediante imposicion masiva, no hacemos nada del observer
		// ya que generamos los retiros en una evento dedicado
		if (Mage::registry('imposicion_masiva') == 1){
			return;
		}

		// 1. Tomamos los datos de la orden segun order id en la tabla "oca_order"
		$orderId	= $observer->getEvent()->getShipment()->getOrder()->getId();
		$order_oca 	= Mage::getModel('oca/order')->loadById($orderId);
		$datos 		= $order_oca->getData();

		if (!$datos) {
			// No es envio con oca
			return;
		}

		$credentials['username'] 		= Mage::getStoreConfig('carriers/ocaconfig/cliente_user',Mage::app()->getStore());
		$credentials['password'] 		= Mage::getStoreConfig('carriers/ocaconfig/cliente_password',Mage::app()->getStore());
		$credentials['cuit']			= Mage::getStoreConfig('carriers/ocaconfig/cliente_cuit',Mage::app()->getStore());
		$credentials['franja_horaria']	= Mage::getStoreConfig('carriers/ocaconfig/retiro_franja',Mage::app()->getStore());

		// Si el envio ya tiene un cod_tracking, cortamos el observer
		if ($datos['cod_tracking'] != ""){
			return;
		}
		
		if ($credentials['username'] == "" OR $credentials['password'] == "") {
			Mage::throwException(Mage::helper('oca')->__('Oca :: no existe nombre de usuario o contraseña'));
		}

		$datos = array($datos);

		$response = Mage::getModel('oca/apicall')->generarIngresoOR($datos,$credentials);

		$cod_tracking 	= $response['DetalleIngresos']['NumeroEnvio'];
		$remito 		= $response['DetalleIngresos']['Remito'];
		$orden_retiro 	= $response['DetalleIngresos']['OrdenRetiro'];
		$constancia 	= "https://www1.oca.com.ar/ocaepak/Envios/EtiquetasCliente.asp?IdOrdenRetiro=".$response['DetalleIngresos']['OrdenRetiro']."&CUIT=".$credentials['cuit'];
		
		$shipment 	= $observer->getEvent()->getShipment();
		$track = Mage::getModel('sales/order_shipment_track')
			->setNumber($cod_tracking)
			->setCarrierCode('oca')
			->setTitle('Oca');
		$shipment->addTrack($track);

		$this->enviarEmailShipment($shipment);

		Mage::getModel('oca/order')->loadById($orderId)->setData('cod_tracking',$cod_tracking)->save();
		Mage::getModel('oca/order')->loadById($orderId)->setData('remito',$remito )->save();
		Mage::getModel('oca/order')->loadById($orderId)->setData('orden_retiro',$orden_retiro)->save();
		Mage::getModel('oca/order')->loadById($orderId)->setData('constancia',$constancia)->save();
		Mage::getModel('oca/order')->loadById($orderId)->setData('estado','Enviado')->save();

	}

	/**
	*  Esta funcion se ejecuta al imponer masivamente ordenes a OCA
	*/
	public function generarRetiroMasivo($observer) {
		
		$orderIds = $observer->getData('orders');

		$collection = Mage::getModel('oca/order')->getCollection()
			->addFieldToFilter('id_orden', $orderIds);

		$datos = array();

		$credentials['username'] 		= Mage::getStoreConfig('carriers/ocaconfig/cliente_user',Mage::app()->getStore());
		$credentials['password'] 		= Mage::getStoreConfig('carriers/ocaconfig/cliente_password',Mage::app()->getStore());
		$credentials['cuit']			= Mage::getStoreConfig('carriers/ocaconfig/cliente_cuit',Mage::app()->getStore());
		$credentials['franja_horaria']	= Mage::getStoreConfig('carriers/ocaconfig/retiro_franja',Mage::app()->getStore());

		if ($credentials['username'] == "" OR $credentials['password'] == "") {
			Mage::throwException(Mage::helper('oca')->__('Oca :: no existe nombre de usuario o contraseña'));
		}

		foreach ($collection as $orden) {
			if($orden['cod_tracking'] == "")
				$datos[] = $orden->getData();
		}

		if(!count($datos))
			return;
		
		$response = Mage::getModel('oca/apicall')->generarIngresoOR($datos,$credentials);
		if(!Mage::getModel('oca/apicall')->isMulti($response['DetalleIngresos'])){
            $temp = $response['DetalleIngresos'];
            $response['DetalleIngresos'] = array();
            $response['DetalleIngresos'][] = $temp;
        }

		foreach ($response['DetalleIngresos'] as $ingreso) {

			$cod_tracking 	= $ingreso['NumeroEnvio'];
			$remito 		= $ingreso['Remito'];
			$orden_retiro 	= $ingreso['OrdenRetiro'];
			$constancia 	= "https://www1.oca.com.ar/ocaepak/Envios/EtiquetasCliente.asp?IdOrdenRetiro=".$ingreso['OrdenRetiro']."&CUIT=".$credentials['cuit'];
			
			try{
				$orderObj 		= Mage::getModel('sales/order')->loadByIncrementId($remito);
				$itemQty		= $orderObj->getItemsCollection()->count();
				$shipment 		= Mage::getModel('sales/service_order', $orderObj)->prepareShipment($itemQty);
				$shipment 		= new Mage_Sales_Model_Order_Shipment_Api();
				$shipmentId 	= $shipment->create($orderObj->getIncrementId(), array(), 'Enviado por OCA', true, true);
				$shipment 		= Mage::getModel('sales/order_shipment')->loadByIncrementId($shipmentId);
				
				$track 			= Mage::getModel('sales/order_shipment_track')
									->setNumber($cod_tracking)
									->setCarrierCode('oca')
									->setTitle('Oca');

				$shipment->addTrack($track)->save();

				$this->enviarEmailShipment($shipment);

				Mage::getModel('oca/order')->loadByOrderIncrementId($remito)->setData('cod_tracking',$cod_tracking)->save();
				Mage::getModel('oca/order')->loadByOrderIncrementId($remito)->setData('remito',$remito)->save();
				Mage::getModel('oca/order')->loadByOrderIncrementId($remito)->setData('orden_retiro',$orden_retiro)->save();
				Mage::getModel('oca/order')->loadByOrderIncrementId($remito)->setData('constancia',$constancia)->save();
				Mage::getModel('oca/order')->loadByOrderIncrementId($remito)->setData('estado','Enviado')->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('oca')->__('La orden n° '.$remito.' ha sido impuesta correctamente en OCA'));
			}catch (Exception $e) {
				Mage::log("Error: " . $e);
			}
		}

	}

	/**
	* Ponemos el envio "Eliminado" en la tabla
	* oca_order al cancelar una orden desde magento
	*/
	public function cancelarOrden($observer) {
		$orderId	= $observer->getEvent()->getItem()->getOrder()->getId();
		Mage::getModel('oca/order')->loadById($orderId)->setData("estado","Eliminada")->save();
	}

	/**
	* Despues de guardar el shippment, enviamos el mail al comprador con su tracking code
	*/
	protected function enviarEmailShipment($shipment) {
		// enviamos el mail con el tracking code
		if($shipment){
			$shipment->sendEmail(true,'');
		}
	}


	/**
	* Agregar massAction al sales_order
	*/
	public function addMassAction($observer) {
		$block = $observer->getEvent()->getBlock();
		if(($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction || $block instanceof Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction)
			&& $block->getRequest()->getControllerName() == 'sales_order')
		{
			$block->addItem('oca', array(
				'label' => 'Generar retiros OCA',
				'url' => $block->getUrl('oca/adminhtml_orders/massImponer'),
				'confirm' => Mage::helper('sales')->__('Desea imponer las ordenes en OCA?')
			));
		}
	}
	
}
?>