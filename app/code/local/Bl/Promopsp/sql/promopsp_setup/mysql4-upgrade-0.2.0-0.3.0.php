<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'cuotas', 'text NULL after `hasta_x_cuotas`');
$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'label', 'text NULL after `weekdays`');
$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'show_refund', 'int(1) DEFAULT 0 after `tope`');

$installer->endSetup();