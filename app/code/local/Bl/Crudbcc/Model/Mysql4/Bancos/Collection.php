<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Model_Mysql4_Bancos_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('crudbcc/bancos');
    }

    public function _afterLoad() {
        parent::_afterLoad();
    }

}