<?php

class Sub1_Nps_Model_Payments extends Mage_Core_Model_Abstract {
    /**
     * 
     * @deprecated VOLAR TODAS ESTAS CONSTANTES
     *
     */

    const SOURCE_TYPE_CC = "cc";
    const SOURCE_MODEL_PATH_CC = "nps/payments_cc";
    const SOURCE_BLOCK_PATH_CC = "nps/standard_form_cc";
    const SOURCE_TYPE_BCC = "bcc";
    const SOURCE_MODEL_PATH_BCC = "nps/payments_bcc";
    const SOURCE_BLOCK_PATH_BCC = "nps/standard_form_bcc";

    /**
     * Codigo del modelo que debe cargar
     *
     * @var string
     * @deprecated REVEER IMPLEMENTACION EN LA NUEVA FORMA
     */
    protected $_sourceModel;
    protected $_quoteObject;

    /**
     * Key del payment method model
     * 
     * @var string
     * @todo REVISAR PARA OTRAS IMPLEMENTACIONES DE NPS
     */
    //protected $_paymentMethodModel = "nps/standard";
    protected $_paymentMethodModel = "nps/nps";

    /**
     * Devuelve el Coeficiente de Interes donde:
     * 
     *  1   = Sin Interes
     *  1.1 = 10% de Interes
     *  2   = 100% de interes
     *
     * @return float
     */
    public function getChargeRatio() {
        return 1;
    }

    /**
     * Devuelve el Porcentaje de Interes
     * 
     * @param bool $addSimbol | Si true agrega el "%" al final
     * @return mixed
     */
    public function getChargePercent($addSimbol = false) {
        $percent = ($this->getChargeRatio() - 1) * 100;
        return ($addSimbol) ? $percent . '%' : $percent;
    }

    /**
     * Devuelve la cantidad de cuotas
     * Implementar en la clase concreta
     * 
     * @return <type>
     */
    public function getPayments() {
        return 1;
    }

    /**
     * Devuelve el valor de cada cuota
     *
     * @param int $payments
     * @param float $index
     * @param float $total
     * @return float | Valor de la cuota
     */
    public function calculatePaymentAmount($payments, $ratio = 1, $total = null) {

        $total = ($total) ? $total : $this->_getTotal();

        return ($total * $ratio) / $payments;
    }

    /**
     * Calcula y devuelve el total de recargo por intereses
     * 
     * @param float $total
     * @return float
     */
    public function calculateChargeAmount($total) {
        //return (1 * $total) - $total;     
        return ($this->getChargeRatio() * $total) - $total;
    }

    /**
     * Valida / Procesa los datos recibidos del formularios
     *
     * @param <type> $data
     * @return <type>
     */
    public function preparePaymentFormData(array $data) {
        return $data;
    }

    /**
     * Ultima Oportunidad de Guardar algun dato en el Quote Address
     * 
     * @param array $data
     * @return array
     */
    public function preparePaymentDataForSaveQuote(array $data) {
        return $data;
    }

    /**
     * Colecta los totales y devuelve el GrandTotal
     * @return float
     */
    protected function _getGrandTotal() {

        //$grndTotal = $this->_getQuote()->collectTotals()->getGrandTotal();
        $data = $this->_getQuote()->collectTotals()->getData();
        $subtotal = $data['base_grand_total'];

        //return $this->_getQuote()->collectTotals()->getGrandTotal();
        return $subtotal;
    }

    /**
     * Colecta los totales y devuelve el BaseGrandTotal
     * @return float
     */
    protected function _getBaseGrandTotal() {
        return $this->_getQuote()->collectTotals()->getBaseGrandTotal();
    }

    /**
     * Devuelve la instancia en session del "Metodo de Pago" seteado.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    protected function _getPaymentMethodInstance() {
        $quote = $this->_getQuote();
        if ($quote->getPayment()->hasMethodInstance()
                && $quote->getPayment()->getMethodInstance()->getCode() == $this->_getPaymentMethodModel()->getCode()) {
            return $quote->getPayment()->getMethodInstance();
        }
        return null;
    }

    /**
     * Devuelve el modelo del "Metodo de Pago" seteado
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    protected function _getPaymentMethodModel() {
        return Mage::getModel($this->_paymentMethodModel);
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote() {
        if ($this->_quoteObject) {
            return $this->_quoteObject;
        }
        return $this->_getCheckout()->getQuote();
    }

    /**
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Devuelve source de datos
     * 
     * @return Mage_Core_Model_Abstract
     * @todo REVISAR
     */
    protected function _getSource() {
        return Mage::getModel($this->_sourceModel);
    }

    /**
     * Devuelve el helper Data o el hijito que le pidas
     *
     * @return mixed Bl_Promopsp_Helper_Data
     */
    protected function _helper($key = null) {
        if ($key) {
            return Mage::helper("nps/$key");
        }
        return Mage::helper("nps");
    }

}
