<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_HellothemesSettings_Model_Config_Leftright
{

   /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'left', 'label'=>Mage::helper('adminhtml')->__('Left')),
            array('value' => 'right', 'label'=>Mage::helper('adminhtml')->__('Right')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'left' => Mage::helper('adminhtml')->__('Left'),
            'right' => Mage::helper('adminhtml')->__('Right'),
        );
    }

}
