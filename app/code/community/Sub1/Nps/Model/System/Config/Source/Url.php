<?php

class Sub1_Nps_Model_System_Config_Source_Url {

    public function toOptionArray() {
        return array(
            array('value' => Sub1_Nps_Model_Psp::NPS_URL_TESTING, 'label' => Mage::helper('nps')->__("Url de Testing - (%s)", Sub1_Nps_Model_Psp::NPS_URL_TESTING)),
            array('value' => Sub1_Nps_Model_Psp::NPS_URL_PRODUCTION, 'label' => Mage::helper('nps')->__("Url de Producción - (%s)", Sub1_Nps_Model_Psp::NPS_URL_PRODUCTION)),
        );
    }

}
