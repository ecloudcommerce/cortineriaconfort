<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Bancos extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        
        $this->_controller = 'adminhtml_bancos';
        $this->_blockGroup = 'crudbcc';
        $this->_headerText = Mage::helper('crudbcc')->__('Bancos');
        $this->_addButtonLabel = Mage::helper('crudbcc')->__('Agregar Banco');
        
        parent::__construct();
    }

}