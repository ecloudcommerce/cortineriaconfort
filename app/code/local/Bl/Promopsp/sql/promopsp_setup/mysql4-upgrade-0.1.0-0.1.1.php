<?php

/**
 * @autor W3ITSolutions
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'hasta_x_cuotas', 'varchar(255) CHARSET utf8 COLLATE utf8_general_ci NULL after `promo_code`');

$installer->endSetup();