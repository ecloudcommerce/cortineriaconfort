<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Tarjetas_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        
        parent::__construct();
        
        $this->setId('crudbcc_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('crudbcc')->__('Información de la tarjeta'));
        
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section_tarjeta', array(
            'label' => Mage::helper('crudbcc')->__('Tarjeta'),
            'title' => Mage::helper('crudbcc')->__('Tarjeta'),
            'content' => $this->getLayout()->createBlock('crudbcc/adminhtml_tarjetas_edit_tab_formtarjeta')->toHtml(),
        ));
        $this->addTab('form_section_cuota', array(
            'label' => Mage::helper('crudbcc')->__('Cuotas'),
            'title' => Mage::helper('crudbcc')->__('Cuotas'),
            'content' => $this->getLayout()->createBlock('crudbcc/adminhtml_tarjetas_edit_tab_formcuotas')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}