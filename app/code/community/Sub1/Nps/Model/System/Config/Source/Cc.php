<?php

class Sub1_Nps_Model_System_Config_Source_Cc {

    public function toOptionArray() {
        return Mage::getModel('nps/nps')->getPaymentsModel()->getCreditCardsOptions();
    }

}
