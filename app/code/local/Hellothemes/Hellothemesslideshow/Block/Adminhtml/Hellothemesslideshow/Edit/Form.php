<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */
 
class Hellothemes_Hellothemesslideshow_Block_Adminhtml_Hellothemesslideshow_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form(array(
	          'id' => 'edit_form',
	          'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
	          'method' => 'post',
	          'enctype' => 'multipart/form-data'
	       )
      );

      $form->setUseContainer(true);
      $this->setForm($form);
      return parent::_prepareForm();
  }
}