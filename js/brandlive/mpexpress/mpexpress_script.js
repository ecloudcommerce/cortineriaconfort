var $j = jQuery.noConflict();

$j(document).ready(function(){

	var payment_mpexpress_expires = $j('#payment_mpexpress_expires').val();

	if(payment_mpexpress_expires == 0){

        $j("#payment_mpexpress_cancelation_cron_enabled").val(0);        

        $j("#row_payment_mpexpress_expiration_duration").hide();
        $j("#row_payment_mpexpress_cancelation_order_header").hide();
        $j("#row_payment_mpexpress_cancelation_cron_enabled").hide();
        $j("#row_payment_mpexpress_cancelation_cron_expiration").hide();
        $j("#row_payment_mpexpress_cancelation_cron_order_statuses").hide();
        $j("#row_payment_mpexpress_cancelation_cron_collection_limit").hide();
        $j("#row_payment_mpexpress_cancelation_cron_log").hide();
   	}else{
        $j("#row_payment_mpexpress_expiration_duration").show();
        $j("#row_payment_mpexpress_cancelation_order_header").show();
        $j("#row_payment_mpexpress_cancelation_cron_enabled").show();
        $j("#row_payment_mpexpress_cancelation_cron_expiration").show();
        $j("#row_payment_mpexpress_cancelation_cron_order_statuses").show();
        $j("#row_payment_mpexpress_cancelation_cron_collection_limit").show();
        $j("#row_payment_mpexpress_cancelation_cron_log").show();
   	}
   
    $j( "#payment_mpexpress_expires" ).change(function() {

    	var payment_mpexpress_expires = $j('#payment_mpexpress_expires').val();

    	if(payment_mpexpress_expires == 0){

            $j("#payment_mpexpress_cancelation_cron_enabled").val(0);      
            
            $j("#row_payment_mpexpress_expiration_duration").hide();
            $j("#row_payment_mpexpress_cancelation_order_header").hide();
            $j("#row_payment_mpexpress_cancelation_cron_enabled").hide();
            $j("#row_payment_mpexpress_cancelation_cron_expiration").hide();
            $j("#row_payment_mpexpress_cancelation_cron_order_statuses").hide();
            $j("#row_payment_mpexpress_cancelation_cron_collection_limit").hide();
            $j("#row_payment_mpexpress_cancelation_cron_log").hide();
	   	}else{
            $j("#row_payment_mpexpress_expiration_duration").show();
            $j("#row_payment_mpexpress_cancelation_order_header").show();
            $j("#row_payment_mpexpress_cancelation_cron_enabled").show();
            $j("#row_payment_mpexpress_cancelation_cron_expiration").show();
            $j("#row_payment_mpexpress_cancelation_cron_order_statuses").show();
            $j("#row_payment_mpexpress_cancelation_cron_collection_limit").show();
            $j("#row_payment_mpexpress_cancelation_cron_log").show();
	   	}
    });

    var unpayment_order = $j('#payment_mpexpress_recover_unpayment_order_enabled').val();

	if(unpayment_order == 0){
		$j("#row_payment_mpexpress_recover_unpayment_order_email_subject").hide();
   		$j("#row_payment_mpexpress_recover_unpayment_order_default_template_email").hide();
   		$j("#row_payment_mpexpress_recover_unpayment_order_statuses").hide();
   		$j("#row_payment_mpexpress_recover_unpayment_order_email_time").hide();
   		$j("#row_payment_mpexpress_recover_cron_collection_limit").hide();
   		$j("#row_payment_mpexpress_recover_cron_log").hide();	   		
   	}else{
   		$j("#row_payment_mpexpress_recover_unpayment_order_email_subject").show();
   		$j("#row_payment_mpexpress_recover_unpayment_order_default_template_email").show();
   		$j("#row_payment_mpexpress_recover_unpayment_order_statuses").show();
   		$j("#row_payment_mpexpress_recover_unpayment_order_email_time").show();
   		$j("#row_payment_mpexpress_recover_cron_collection_limit").show();
   		$j("#row_payment_mpexpress_recover_cron_log").show();	 
   	}

    $j( "#payment_mpexpress_recover_unpayment_order_enabled" ).change(function() {

    	var unpayment_order = $j('#payment_mpexpress_recover_unpayment_order_enabled').val();

    	if(unpayment_order == 0){
    		$j("#row_payment_mpexpress_recover_unpayment_order_email_subject").hide();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_default_template_email").hide();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_statuses").hide();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_email_time").hide();
	   		$j("#row_payment_mpexpress_recover_cron_collection_limit").hide();
	   		$j("#row_payment_mpexpress_recover_cron_log").hide();	   		
	   	}else{
	   		$j("#row_payment_mpexpress_recover_unpayment_order_email_subject").show();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_default_template_email").show();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_statuses").show();
	   		$j("#row_payment_mpexpress_recover_unpayment_order_email_time").show();
	   		$j("#row_payment_mpexpress_recover_cron_collection_limit").show();
	   		$j("#row_payment_mpexpress_recover_cron_log").show();	 
	   	}
    });

});