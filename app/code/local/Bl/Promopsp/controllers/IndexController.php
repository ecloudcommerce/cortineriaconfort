<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Bl_Promopsp_IndexController extends Mage_Core_Controller_Front_Action {

    function getFeesAction() {
        
        $ccid = (int) $this->getRequest()->getParam('cc_id', false);
        $bankid = (int) $this->getRequest()->getParam('bank_id', false);
        $productid = (int) $this->getRequest()->getParam('p_id', false);
        
        try {
            if ($ccid && $bankid && $productid) {
                $data = $this->_getStandard()->getPaymentsModel()->getPaymentsData(array(
                    'bank_id' => $bankid,
                    'cc_id' => $ccid,
                    'product_id' => $productid,
                        ));
                $data['status'] = 'ok';
            } else {
                throw new Exception(Mage::helper('nps')->__('No se recibieron los identificadores de banco y tarjeta.'));
            }
        } catch (Exception $e) {
            $data = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
        }

        $data['type'] = 'json';
        $this->_responseToJson($data);
        
    }
    
    protected function _getStandard() {
        //return Mage::getModel('nps/standard');
        return Mage::getModel('nps/nps');
    }

    protected function _getSource() {
        return $this->_getStandard()->getPaymentsModel();
    }    
    
    protected function _responseToJson($data) {
        $this->getResponse()
                ->setBody(Mage::helper('core')->jsonEncode($data));
        $this->loadLayout(false);
        $this->renderLayout();
    }
    
}