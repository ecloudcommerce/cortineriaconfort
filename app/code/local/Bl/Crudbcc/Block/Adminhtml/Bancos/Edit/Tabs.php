<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Bancos_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        
        parent::__construct();
        
        $this->setId('crudbcc_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('crudbcc')->__('Información del banco'));
        
    }

    protected function _beforeToHtml() {
        
        $this->addTab('form_section_banco', array(
            'label' => Mage::helper('crudbcc')->__('Banco'),
            'title' => Mage::helper('crudbcc')->__('Banco'),
            'content' => $this->getLayout()->createBlock('crudbcc/adminhtml_bancos_edit_tab_formbanco')->toHtml(),
        ));
        $this->addTab('form_section_tarjeta', array(
            'label' => Mage::helper('crudbcc')->__('Tarjeta'),
            'title' => Mage::helper('crudbcc')->__('Tarjeta'),
            'content' => $this->getLayout()->createBlock('crudbcc/adminhtml_bancos_edit_tab_formtarjeta')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}