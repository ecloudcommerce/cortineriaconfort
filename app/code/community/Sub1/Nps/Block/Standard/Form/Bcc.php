<?php

class Sub1_Nps_Block_Standard_Form_Bcc extends Sub1_Nps_Block_Form {

    protected $_templatePath = 'nps/standard/form-bcc.phtml';
    protected $_feesAjaxUrl = 'nps/standard_bcc/getFees';
    protected $_banks;

//    public function getCreditCards()
//    {
//        $data = $this->getPaymentsModel()->getCreditCardsData();
//        return (count($data) > 0) ? $data : null;
//    }

    public function getFeesAjaxUrl() {
        return $this->getUrl($this->_feesAjaxUrl, array('_secure' => true));
    }

    public function getBanksJson() {
        return json_encode($this->getBanks());
    }

    /**
     *
     * @return <type>
     */
    public function getBanks() {

        if (!$this->_banks) {
            $this->_banks = $this->getPaymentsModel()->getBanks();
        }
        return $this->_banks;
    }

}