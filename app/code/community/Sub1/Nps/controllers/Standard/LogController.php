<?php

class Sub1_Nps_Standard_LogController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        die('Access Denied');
    }

    public function transactionAction() {
        if (!$this->getStandard()->isDebug()) {
            $this->norouteAction();
            die('Access Denied');
        }

        // Mejorar el listado.
        $collection = Mage::getModel('nps/psp_log')->getCollection();
        $collection->setOrder("id", "desc");

        if ($collection) {
            $html = '';
            foreach ($collection as $log) {

                $html .= '<div>';
                $html .= '   <table>';
                $html .= '       <tr><td>Type:</td><td>' . $log->getType() . '</td></tr>';
                $html .= '       <tr><td>Descripcion:</td><td>' . $log->getDescription() . '</td></tr>';
                $html .= '       <tr><td>Referencia:</td><td>' . $log->getReferenceId() . '</td></tr>';
                $html .= '       <tr><td>Orden:</td><td>' . $log->getOrderIncrementId() . '</td></tr>';
                $html .= '       <tr><td>Fecha:</td><td>' . $log->getDate() . '</td></tr>';
                $html .= '       <tr><td colspan="2">Detalles:</td></tr>';
                $html .= '       <tr><td colspan="2"><pre>' . print_r(@unserialize($log->getParameters()), true) . '</pre></td></tr>';
                $html .= '   </table>';
                $html .= '</div>';
            }
            echo $html;
        }

        // print_r($log->getData());
        die();
    }

    public function testVisaAction() {
        $orderId = $this->getRequest()->getParam("order_id");

        if (!$orderId || $orderId == '') {
            die("Mete el order_id en la url... Capo!!!");
        }

        $order = Mage::getModel("sales/order")->load($orderId);
        /* @var $order Mage_Sales_Model_Order */
        if (!$order->getId()) {
            die("No existe una orden con ID = $orderId");
        }
        try {
            $npsData = unserialize($order->getNpsData());
            $transaction = $npsData['nps']['transaction']->psp_Transaction;

            // Order Status OK
            $order->setState($this->_getConfigData("order_status"), true)
                    ->save();

            $this->getStandard()->processVisaValidationResult($order, $transaction);

            echo "Todo Bien";
            die();
        } catch (Exception $e) {

            if ((bool) $this->getStandard()->getConfigData('cancel_order_on_failure')) {
                $order->cancel()
                        ->save();
            }

            if ((bool) $this->_getConfigData('send_mail_order_confirmation')) {
                $this->getStandard()->sendEmailOrderNotification(array(
                    "message" => $e->getMessage(),
                ));
            }

            Zend_Debug::dump($e->getMessage());
            Zend_Debug::dump($e->getTraceAsString());

            die("<br>UPS");
        }
    }

    public function squeryAction() {
        $result = $this->getStandard()->getTransactionResult();
        print_r($result);
        die();
    }

    /**
     *
     * @param <type> $data
     * @return Bl_Nps_Standard_CcController 
     */
    protected function _responseToJson($data) {
        $this->getResponse()
                ->setBody(Mage::helper('core')->jsonEncode($data));
        return $this;
    }

    /**
     *
     * @return Bl_Nps_Model_Standard
     */
    public function getStandard() {
        return Mage::getModel('nps/standard');
    }

    protected function _getSource() {
        return $this->getStandard()->getPaymentsModel();
    }

    public function _getConfigData($field) {
        return $this->getStandard()->getConfigData($field);
    }

    protected function _isDebugCall() {
        return (bool) $this->getRequest()->getParam('debug', false);
    }

    public function preDispatch() {
        parent::preDispatch();
        if ($this->_isDebugCall()) {
            echo '<pre style="border: 1px solid #CCC; background: #EEE; color: #444;">';
        }
        if (!$this->getStandard()->isDebug()) {
            $this->norouteAction();
            die('Access Denied');
        }
    }

}