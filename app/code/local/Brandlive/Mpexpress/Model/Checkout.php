<?php
/**
 *
 * @category   Brandlive
 * @package    Brandlive_Mpexpress
 * @author     Juan Pablo Mayoral <jpmayoral@brandlive.net>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Brandlive_Mpexpress_Model_Checkout extends Mpexpress_Model_Checkout{

    const RECOVER_CRON_LOG_FILE = 'mp_ordenes_no_pagadas.log';

	/*
	*	Rewirte function getCheckout
	*	Added mp_payment_link save into sales_flat_order table
	*/
    public function getCheckout($preference, $sandbox){

        $helper = Mage::helper('brandlive_mpexpress/data');
        $logEnabled = $helper->getRecoverCronLogEnabled();

        $preferenceResult = $this->mp->create_preference($preference);

        // Get increment_id according to external_reference
        $external_reference  = $preference['external_reference'];
        $order  = explode('-', $external_reference);
        $increment_id = $order[1];

        $order = Mage::getModel('sales/order')->loadByIncrementId($increment_id);

        if($this->mp->sandbox_mode()):
            $mp_payment_link = $preferenceResult["response"]["sandbox_init_point"];
        else:
            $mp_payment_link = $preferenceResult["response"]["init_point"];
        endif;

        $mp_payment_link = $preferenceResult["response"]["init_point"];
        $order->setData('mp_payment_link', $mp_payment_link);

        // Save mp_payment_link into sales_flat_order table
        if($order->save()):
            if($logEnabled):
                Mage::log('Se guardo el link de MercadoPago: '.$mp_payment_link ,null, self::RECOVER_CRON_LOG_FILE); 
            endif;
        else:
            if($logEnabled):
                Mage::log('Ocurrió un error al guardar el link de MercadoPago',null, self::RECOVER_CRON_LOG_FILE); 
            endif;
        endif;
       
        return $mp_payment_link;

    }


}

