<?php
// @var $setup Mage_Eav_Model_Entity_Setup
$setup = $this;

$setup->startSetup();

$applyTo = array(
    Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
);

$setup->addAttribute('catalog_product', 'paquete_alto', array(
        'group'         => 'General',
        'type'          => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'backend'       => '',
        'frontend'      => '',
        'class'         => '',
        'default'       => '',
        'label'         => 'Alto paquete',
        'input'         => 'text',
        'source'        => '',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'       => 1,
        'required'      => 0,
        'searchable'    => 0,
        'filterable'    => 1,
        'unique'        => 0,
        'comparable'    => 0,
        'visible_on_front'          => 0,
        'is_html_allowed_on_front'  => 1,
        'user_defined'  => 1,
        'apply_to'      => implode(',',$applyTo)
));

$setup->addAttribute('catalog_product', 'paquete_ancho', array(
        'group'         => 'General',
        'type'          => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'backend'       => '',
        'frontend'      => '',
        'class'         => '',
        'default'       => '',
        'label'         => 'Ancho paquete',
        'input'         => 'text',
        'source'        => '',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'       => 1,
        'required'      => 0,
        'searchable'    => 0,
        'filterable'    => 1,
        'unique'        => 0,
        'comparable'    => 0,
        'visible_on_front'          => 0,
        'is_html_allowed_on_front'  => 1,
        'user_defined'  => 1,
        'apply_to'      => implode(',',$applyTo)
));

$setup->addAttribute('catalog_product', 'paquete_largo', array(
        'group'         => 'General',
        'type'          => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'backend'       => '',
        'frontend'      => '',
        'class'         => '',
        'default'       => '',
        'label'         => 'Largo paquete',
        'input'         => 'text',
        'source'        => '',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'       => 1,
        'required'      => 0,
        'searchable'    => 0,
        'filterable'    => 1,
        'unique'        => 0,
        'comparable'    => 0,
        'visible_on_front'          => 0,
        'is_html_allowed_on_front'  => 1,
        'user_defined'  => 1,
        'apply_to'      => implode(',',$applyTo)
));


$setup->run("
    CREATE TABLE IF NOT EXISTS `{$setup->getTable('oca_order')}` (
        `id` int(11) NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
        `id_orden` int(11) NOT NULL,
        `operatoria` varchar(100) NOT NULL,
        `direccion` varchar(255) NOT NULL,
        `nro_cuenta` varchar(255) NOT NULL,
        `localidad` varchar(255) NOT NULL,
        `provincia` varchar(255) NOT NULL,
        `cp_destino` varchar(255) NOT NULL,
        `sucursal_retiro` int(11) NOT NULL,
        `orden_retiro` int(11) NOT NULL,
        `remito` int(20) NOT NULL,
        `nombre` varchar(255) NOT NULL,
        `apellido` varchar(255) NOT NULL,
        `telefono` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `precio` float NOT NULL,
        `valor_declarado` float NOT NULL,
        `volumen` float NOT NULL,
        `peso` float NOT NULL,
        `tipo_pedido` int(1) NOT NULL,
        `cod_tracking` VARCHAR( 255 ) NOT NULL,
        `estado` VARCHAR( 255 ) NOT NULL,
        `tracking` TEXT NOT NULL,
        `constancia` varchar(600) NOT NULL,
        `entrega` varchar(255) NOT NULL,
        `order_increment_id` varchar(50) NOT NULL,
        `fecha_retiro` varchar(255) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$setup->endSetup();