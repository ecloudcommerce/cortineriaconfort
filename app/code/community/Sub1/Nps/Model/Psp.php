<?php

class Sub1_Nps_Model_Psp extends Mage_Core_Model_Abstract {

    const NPS_URL_TESTING = "https://implementacion.nps.com.ar/ws.php?wsdl";
    const NPS_URL_PRODUCTION = "https://services2.nps.com.ar/ws.php?wsdl";

    public function _construct() {
        parent::_construct();
        $this->_init('nps/psp');
    }

    public function getMethod() {
        return Mage::getModel("nps/nps");
    }

}
