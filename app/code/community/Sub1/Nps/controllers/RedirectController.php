<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_GoogleCheckout
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


require_once(Mage::getBaseDir('lib') . '/Sub1/psp_client.php');

/**
 * @category    Mage
 * @package     Mage_GoogleCheckout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Sub1_Nps_RedirectController extends Mage_Core_Controller_Front_Action
{

    const METHOD_CODE = 'nps_code';
    const CALL_TYPE_REQUEST = 'request';
    const CALL_TYPE_RESPONSE = 'response';
    const VISA_PRODUCT_CODE = '14';
    const AMEX_PRODUCT_CODE = '1';
    const PLAN_Z_CODE = 'PlanZ';
    const TARJETA_NARANJA_PRODUCT_CODE = '9';
    const CONFIG_EMAIL_NOTIFICATION_PATH = 'email_notification';
    const CONFIG_EMAIL_FROM_EMAIL_PATH = 'trans_email/ident_general/email';
    const CONFIG_EMAIL_FROM_NAME_PATH = 'trans_email/ident_general/name';
    const NPS_DEBUG_LOG_FILE = 'nps_debug.log';

    /**
     * When a customer chooses Google Checkout on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        /**
         * obtengo la ultima orden
         */
        $order = new Mage_Sales_Model_Order();
        $order->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());

        /**
         * creo un bloque para la vista
         */
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','nps',array('template' => 'nps/redirect.phtml'));

        $pspSession = Mage::getSingleton('core/session')->getPspSession();
        if(!isset($pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]) || !count($pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]) || empty($pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()])) {

            /**
             * primero pido el transaction id a psp
             */
            switch( $this->getStandard()->getConfigData('payment_action_nps') )
            {
              case 'authorize_capture': $method_name = 'PayOnLine_3p'; break;
              case 'authorize': $method_name = 'Authorize_3p'; break;
            }

            $session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
            //$psp_NumPayments = Mage::getSingleton('core/session')->getInstallment(); -> comento para utilizar bancos/tarjetas

            $npsData = $this->_getCheckout()->getNpsData();

            Mage::log("npsData: ".print_r($npsData,true),null,self::NPS_DEBUG_LOG_FILE);

            $psp_parameters = array(
                'psp_Version'                  => '1',
                'psp_MerchantId'               => $this->getStandard()->getConfigData('merchant_id'),
                'psp_TxSource'                 => 'WEB',
                'psp_MerchTxRef'               => strtoupper(uniqid($order->getIncrementId().'.', true)),
                'psp_PosDateTime'              => date('Y-m-d H:i:s'),
                'psp_MerchOrderId'             => $order->getIncrementId(),
                //'psp_Amount'                 => $this->getStandard()->calculatePspAmount($order->getGrandTotal(), $order->getPayment()->getData('cc_type'), $psp_NumPayments),
                //'psp_Amount'                 => $this->getStandard()->toCents($order->getGrandTotal()),
                'psp_Amount'                   => $this->_getAmount($order, $npsData['coef']),
                //'psp_NumPayments'            => $psp_NumPayments, -> comento para utilizar bancos/tarjetas
                'psp_NumPayments'              => str_pad($npsData['payments'], 2, "0", STR_PAD_LEFT),
                //'psp_NumPayments'            => 1,
                'psp_Currency'                 => substr($this->getStandard()->formatCurrencyToPspCurrency( Mage::app()->getStore()->getCurrentCurrencyCode() ),0,3),
                'psp_CustomerMail'             => $order->getCustomerEmail() ? substr($order->getCustomerEmail(),0,255) : 'nomail@magento.com',
                'psp_MerchantMail'             => substr($this->getStandard()->getConfigData('merchant_email'),0,255),
                //'psp_Product'                => $this->getStandard()->formatCCTypeToPspProductId($order->getPayment()->getData('cc_type')),
                'psp_Product'                  => $npsData['nps_code'],
                //'psp_Country'                => substr($this->getStandard()->formatCountryToPspCountry(Mage::getStoreConfig('general/country/default')),0,3),
                'psp_Country'                  => "ARG", //@todo Revisar otros Paises
                'psp_ReturnURL'                => Mage::getUrl('nps/redirect/response', array('_secure' => true)),
                'psp_FrmLanguage'              => 'es_AR',
                //'psp_FrmBackButtonURL'       => Mage::getUrl('checkout/cart'), // entra en loop infinito con esto no se porque...    
                //'psp_FrmBackButtonURL'       => Mage::getUrl('nps/redirect/response'),
                'psp_FrmBackButtonURL'         => Mage::getBaseUrl() . 'checkout/onepage/', // modificacion segun promos
                'psp_PurchaseDescription'      => substr('ORDER-'.$order->getIncrementId(),0,255),
                // 'psp_ForceProcessingMethod' => "1",
            );
            
            if ($npsData['nps_code'] == self::VISA_PRODUCT_CODE && $this->useVisaDniValidation()){

                $nombre = ($customerId) ? $customer->getFirstname() : $order->getCustomerFirstname();
                $apellido = ($customerId) ? $customer->getLastname() : $order->getCustomerLastname();
                $dni = ($customerId) ? $customer->getDni() : $order->getCustomerDni();
                $dob = ($customerId) ? $customer->getDob() : $order->getCustomerDob();

                $psp_parameters['psp_VisaArg_DA_DocType']   = "1"; // Harcodeado a DNI
                $psp_parameters['psp_VisaArg_DA_DocNum']    = $dni;
                $psp_parameters['psp_VisaArg_DA_Name']      = "$nombre $apellido";
                $psp_parameters['psp_VisaArg_DA_BirthDay']  = substr($dob, 5, 2) . substr($dob, 8, 2) . substr($dob, 0, 4);
                $psp_parameters['psp_VisaArg_DA_DoorNum']   = $order->getBillingAddress()->getNumero();
                
            }

            /* Si es VISA y 12 cuotas, mando 7 para ahora12 */
            /*if ($npsData['nps_code'] == self::VISA_PRODUCT_CODE && $psp_parameters['psp_NumPayments'] == 12){
                $psp_parameters['psp_NumPayments'] = 7;
                // Seteo las cuotas a 12 para amex con ahora12
                $npsData['payments'] = 7;
                $npsData['cc_fee']   = 7;
            }*/

            /* 13255- Tarjeta AMEX */
            if ($npsData['nps_code'] == self::AMEX_PRODUCT_CODE && $psp_parameters['psp_NumPayments'] == 7){
                $psp_parameters['psp_NumPayments'] = 12;
                // Seteo las cuotas a 12 para amex con ahora12
                $npsData['payments'] = 12;
                $npsData['cc_fee'] = 12;
            }

            if ($npsData['nps_code'] == self::AMEX_PRODUCT_CODE && $psp_parameters['psp_NumPayments'] == 8){
                $psp_parameters['psp_NumPayments'] = 18;
                // Seteo las cuotas a 18 para amex con ahora18
                $npsData['payments'] = 18;
                $npsData['cc_fee'] = 18;
            }

            /* Nps Plan Z - Tarjeta Naranja*/
            if ($npsData['nps_code'] == self::PLAN_Z_CODE){
                $psp_parameters['psp_Plan'] = self::PLAN_Z_CODE;
                $psp_parameters['psp_NumPayments'] = 1;
                $psp_parameters['psp_Product']= self::TARJETA_NARANJA_PRODUCT_CODE;
            }

            // Promo Code & Merchant
            // Ver si otros campos son necesarios
            if (isset($npsData['promo'])
                    && isset($npsData['promo']['merchant_id'])
                    && isset($npsData['promo']['promo_code'])) {
                $psp_parameters['psp_MerchantId'] = $npsData['promo']['merchant_id'];
                $psp_parameters['psp_PromotionCode'] = $npsData['promo']['promo_code'];
            }      
            
            //$this->getStandard()->addOrderData($order,$npsData);
            $order->save();

            Mage::log("Datos enviados a NPS (psp_parameters): ".print_r($psp_parameters,true),null,self::NPS_DEBUG_LOG_FILE);

            $response = $this->getStandard()->sendToNPS($method_name, $psp_parameters);

            if($response){
                Mage::log("Respuesta Parcial de NPS: ".print_r($response,true),null,self::NPS_DEBUG_LOG_FILE);                    
            }            

            if(is_array($response) && count($response) && @$response['psp_TransactionId']) {
                $pspSession = is_array(Mage::getSingleton('core/session')->getPspSession()) ? Mage::getSingleton('core/session')->getPspSession() : array();
                $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()] = $response;
                Mage::getSingleton('core/session')->setPspSession($pspSession);

                foreach($response as $k => $v) {
                    $block->setData($k, $v);
                }
                $this->getStandard()->directLinkTransact($order,@$response['psp_TransactionId'], $response, Mage_Sales_Model_Order_Payment_Transaction::TYPE_PAYMENT, $method_name, $closed = 0);
            }else {
                //$this->cancelAction();
                $this->cancelAction($response);
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=>true));
            }
        }else {
            $block->setData('psp_FrontPSP_URL', $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]['psp_FrontPSP_URL']);
            $block->setData('psp_Session3p', $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]['psp_Session3p']);
            $block->setData('psp_TransactionId', $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]['psp_TransactionId']);
            $block->setData('psp_MerchantId', $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]['psp_MerchantId']);
            $block->setData('psp_MerchTxRef', $pspSession['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]['psp_MerchTxRef']);
        }

        /**
         * rendereo el form y autosubmiteo a psp
         */
        $this->loadLayout();
        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }

    public function directLinkTransactionReformat($response) {
      foreach($response as $indexA => $valueA) {
        if(is_array($valueA)) {
          foreach($valueA as $indexB => $valueB) {
            if(is_array($valueB)) {
              foreach($valueB as $indexC => $valueC) {
                if( is_array($valueC) ) {
                  foreach($valueC as $indexD => $valueD) {
                    if(is_array($valueD)) {

                    }else {
                      $response[$indexA.'.'.$indexB.'.'.$indexC.'.'.$indexD] = $valueD;
                    }
                  }
                }else {
                  $response[$indexA.'.'.$indexB.'.'.$indexC] = $valueC;
                }
              }
            }else {
              $response[$indexA.'.'.$indexB] = $valueB;
            }
          }
          unset($response[$indexA]);
        }
      }
      return $response;
    }

    // The response action is triggered when your gateway sends back a response after processing the customer's payment
    public function responseAction($retries=0) {
        if($this->getRequest()->isPost()) {

            if(is_array(@$_SESSION['queried_psp_tx_ids'])
                    && in_array($_POST['psp_TransactionId'],$_SESSION['queried_psp_tx_ids'])) {
              Mage_Core_Controller_Varien_Action::_redirect('');
              return;
            }

            /**
             * comentado 2015-06-01
             * unset($_SESSION['psp_transaction'.Mage::getSingleton('checkout/session')->getLastRealOrderId()]);
             */

            // SimpleQueryTx
            $psp_parameters_query = array(
                'psp_Version'         => '1',
                'psp_MerchantId'      => $this->getStandard()->getConfigData('merchant_id'),
                'psp_QueryCriteria'   => 'T',
                'psp_QueryCriteriaId' => $_POST['psp_TransactionId'],
                'psp_PosDateTime'     => date('Y-m-d H:i:s')
            );

            //Mage::log("psp_parameters_query: ".print_r($psp_parameters_query,true),null,self::NPS_DEBUG_LOG_FILE);                

            $response = $this->getStandard()->sendToNPS('SimpleQueryTx', $psp_parameters_query);          

            if($response === FALSE && $retries <= 10) {
              sleep(2);
              $this->responseAction($retries+1);
            }            

            /**
             * Se hace asi porque se requiere el psp_MerchOrderId
             */
            if(is_array($response) && isset($response['psp_Transaction']) )
            {

                if($response){
                    Mage::log("Respuesta Final (psp_Transaction): ".print_r($response['psp_Transaction'],true),null,self::NPS_DEBUG_LOG_FILE);                    
                }                

                $_SESSION['queried_psp_tx_ids'][] = $_POST['psp_TransactionId'];

                if($response['psp_Transaction']['psp_ResponseCod'] == 0) {

                    // Payment was successful, so update the order's state, send order email and move to the success page
                    $order = Mage::getModel('sales/order');
                    $order->loadByIncrementId($response['psp_Transaction']['psp_MerchOrderId']);

                    $this->getStandard()->directLinkTransact($order,@$response['psp_Transaction']['psp_TransactionId'], $this->directLinkTransactionReformat($response), Mage_Sales_Model_Order_Payment_Transaction::TYPE_PAYMENT, 'SimpleQueryTx', $closed = 0);

                    // Guardo los datos de respuesta de NPS en la orden
                    //$this->getStandard()->setOrderData($order, $response['psp_Transaction']);
                    $this->getStandard()->saveTransactionDataToOrder($response['psp_Transaction'], $order);

                    $order->addStatusHistoryComment('NPS autorizó el pago.');

                    if ((int) $this->getStandard()->getConfigData('send_mail_order_confirmation') == 1) {
                        $order->sendNewOrderEmail();
                        $order->setEmailSent(true);
                    }                    

                    // Actualizo estado de la orden
                    $order->setState($this->getStandard()->getConfigData("order_status_ok"), true);
                    $order->save();

                    Mage::getSingleton('checkout/session')->unsQuoteId();

                    if($this->getStandard()->getConfigData('payment_action_nps') == 'authorize_capture') {

                        try {
                            
                            if(!$order->canInvoice()) {
                                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
                            }
                            
                            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                            
                            if(!$invoice->getTotalQty()) {
                                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                            }
                            
                            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                            $invoice->register();
                            $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder());
                            $transactionSave->save();
                        }catch (Mage_Core_Exception $e) {

                        }

                    }

                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=>true));
                }else {
                    $this->cancelAction($response);
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=>true));
                }
            }else {
                $order = new Mage_Sales_Model_Order();
                $order->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
                $order->hold()->setState(Mage_Sales_Model_Order::STATE_HOLDED, true, 'NPS no ha respondido, la orden debe ser revisada manualmente.');
                $order->save();
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=>true));
            }

        }else {
            Mage_Core_Controller_Varien_Action::_redirect('');
        }
    }

    // The cancel action is triggered when an order is to be cancelled
    public function cancelAction($response) {
        if (Mage::getSingleton('checkout/session')->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
            if($order->getId()) {

                if($response){
                    Mage::log("Transacción Cancelada: ".print_r($response,true),null,self::NPS_DEBUG_LOG_FILE);    
                    
                    // Guardo los datos de respuesta de NPS en la orden
                    $this->getStandard()->saveTransactionDataToOrder($response, $order);
                }   

                if ((bool) $this->getStandard()->getConfigData("cancel_order_on_failure")) {
                    // Flag the order as 'cancelled' and save it
                    $order->cancel()->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'NPS rechazó el pago.');
                }
                
                if(is_array($response) && count($response)) {

                  $this->getStandard()->directLinkTransact($order,@$response['psp_Transaction']['psp_TransactionId'], $this->directLinkTransactionReformat($response), Mage_Sales_Model_Order_Payment_Transaction::TYPE_PAYMENT);

                  if($response['psp_Transaction']['psp_TransactionId']) {
                    $order->getPayment()->setCcTransId($response['psp_Transaction']['psp_TransactionId']);
                    $order->getPayment()->setTransactionId(@$response['psp_Transaction']['psp_TransactionId']);
                    $order->getPayment()->setIsTransactionClosed(0);
                    $order->getPayment()->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$response);
                  }
                  
                  if ($response['psp_Transaction']['psp_ResponseMsg']){
                    Mage::getSingleton('checkout/session')->setErrorMessage( $response['psp_Transaction']['psp_ResponseMsg'] );
                  }
                  
                }
                $order->save();
            }
        }
    }

    /**
     * JSON retrieve installments
     */
    public function query_installmentAction() {
      if($this->getStandard()->getConfigData('enable_installment')) {
        /*
        $cc_type = 'VI';
        $country = 'AR';
        $currency = 'ARS';
        */

        /**
         * producto viene siempre por parametro
         */
        $cc_type = mysql_escape_string($_REQUEST['cc_type']);


        /**
         * country lo saco de la secion del store
         */
        $country = Mage::getStoreConfig('general/country/default');


        /**
         * currency puede venir por parametro, si no viene uso el del store
         */
        $currency = isset($_REQUEST['currency']) ? mysql_escape_string($_REQUEST['currency']) : Mage::app()->getStore()->getBaseCurrencyCode();


        /**
         * se filtra por product(cc_type)+country+currency
         */
        $table = Mage::getSingleton('core/resource')->getTableName('installment/installment');
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $query = 'SELECT ii.* FROM '.$table.' ii
            INNER JOIN installment_installment_store iis ON ii.entity_id = iis.installment_id
            WHERE ii.cc_type = \''.$cc_type.'\'
            AND ii.country = \''.$country.'\' AND ii.currency = \''.$currency.'\'
            AND iis.store_id = '.Mage::app()->getStore()->getId().'
            ORDER BY ii.qty';
        $installments = $readConnection->fetchAll($query);

        echo json_encode($installments);
      }
    }

    /**
     * PASAR A MODELO "SERVICE"
     *
     * Calcula el Total y lo devuelve en el formato correcto y con intereses
     *
     * @param $order
     * @return int valor de la compra incluidos intereses.
     *
     * @deprecated Calcular en funcion del grandtotal o solicitar al PaymentsModel
     */
    protected function _getAmount($order) {
        $rate = $this->_getCurrencyRate($order->getOrderCurrencyCode());
        // SOLO USAR ESTE SI NO SE APLICA EL INTERES EN EL GRANDTOTAL DE LA ORDEN FINAL
        $amount = $order->getBaseTotalDue() / $rate;
        return round($amount * 100, 0);
    }

    /**
     * PASAR A MODELO "SERVICE"
     * 
     * Devuelve el indice de conversion de la moneda base a la actual
     *
     * @param string $currency_code
     * @return float
     */
    protected function _getCurrencyRate($currency_code) {
        $rate = Mage::getModel('directory/currency')->getRate($currency_code);
        return ($rate == 0) ? 1 : $rate;
    }

    /**
     * Get singleton with NPS strandard Model
     *
     * @return Sub1_Nps_Model_Standard
     */
    public function getStandard() {
        return Mage::getSingleton('nps/nps');
    }

    /**
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

    public function useVisaDniValidation() {
        return (bool) $this->getStandard()->getConfigData("use_visa_dni_validation");
    }

}
