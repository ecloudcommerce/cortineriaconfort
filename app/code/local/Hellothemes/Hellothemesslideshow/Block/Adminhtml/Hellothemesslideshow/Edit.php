<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_Hellothemesslideshow_Block_Adminhtml_Hellothemesslideshow_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'hellothemesslideshow';
        $this->_controller = 'adminhtml_hellothemesslideshow';
        
        $this->_updateButton('save', 'label', Mage::helper('hellothemesslideshow')->__('Save Slide'));
        $this->_updateButton('delete', 'label', Mage::helper('hellothemesslideshow')->__('Delete Slide'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('hellothemesslideshow_data') && Mage::registry('hellothemesslideshow_data')->getId() ) {
            return Mage::helper('hellothemesslideshow')->__("Edit Slide");
        } else {
            return Mage::helper('hellothemesslideshow')->__('Add Slide');
        }
    }
}