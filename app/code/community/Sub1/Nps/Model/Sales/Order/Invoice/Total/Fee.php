<?php

class Sub1_Nps_Model_Sales_Order_Invoice_Total_Fee extends Mage_Sales_Model_Order_Invoice_Total_Abstract {

    /**
     * 
     * @var <type> 
     */
    protected $_paymentMethodCode = "nps/nps";

    /**
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return Bl_Nps_Model_Sales_Order_Invoice_Total_Fee 
     */
    public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
        $order = $invoice->getOrder();
        $data = $this->_getPaymentMethodModel()->getOrderData($order);

        Mage::log(__METHOD__ . " " . print_r($data, true), 7, "debug-invoice.log");
        return $this;
    }

    /**
     * Devuelve el Codigo del Payment Method
     * 
     * @return string
     */
    protected function _getPaymentCode() {
        return $this->_getPaymentMethodModel()->getCode();
    }

    /**
     * @return Bl_Nps_Model_Payments
     */
    protected function _getPaymentsModel() {
        return $this->_getPaymentMethodModel()->getPaymentsModel();
    }

    /**
     * Devuelve el Modelo del Metodo de pago "Nps Standard"
     * @return Bl_Nps_Model_Standard
     */
    protected function _getPaymentMethodModel() {
        return Mage::getModel($this->_paymentMethodCode);
    }

    /**
     * Devuelve modelo de session del checkout
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

}