<?php
/**
 * @author W3ITSolutions
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($installer->getTable('salesrule'), 'is_everyday')) {
    $connection->addColumn($installer->getTable('salesrule'), 'is_everyday', "tinyint(1) NOT NULL default '1'");
} else {
	// > log error "column `is_everyday` should not exist"
	$connection->modifyColumn($installer->getTable('salesrule'), 'is_everyday', "tinyint(1) NOT NULL default '1'");
}

if (!$connection->tableColumnExists($installer->getTable('salesrule'), 'weekdays')) {
    $connection->addColumn($installer->getTable('salesrule'), 'weekdays', "mediumtext");
} else {
	// > log error "column `weekdays` should not exist"
	$connection->modifyColumn($installer->getTable('salesrule'), 'weekdays', "mediumtext");
}

if ($connection->tableColumnExists($installer->getTable('salesrule'), 'from_date')) {
	$connection->modifyColumn($installer->getTable('salesrule'), 'from_date', "datetime");
} else {
	// > log error "column `from_date` should not have been deleted"
	$connection->addColumn($installer->getTable('salesrule'), 'from_date', "datetime");
}

if ($connection->tableColumnExists($installer->getTable('salesrule'), 'to_date')) {
	$connection->modifyColumn($installer->getTable('salesrule'), 'to_date', "datetime");
} else {
	// > log error "column `to_date` should not have been deleted"
	$connection->addColumn($installer->getTable('salesrule'), 'to_date', "datetime");
}


$installer->endSetup();