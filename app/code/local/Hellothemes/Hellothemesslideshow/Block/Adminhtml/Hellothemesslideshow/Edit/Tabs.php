<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_Hellothemesslideshow_Block_Adminhtml_Hellothemesslideshow_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('hellothemesslideshow_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('hellothemesslideshow')->__('Slide Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('hellothemesslideshow')->__('Slide Information'),
          'title'     => Mage::helper('hellothemesslideshow')->__('Slide Information'),
          'content'   => $this->getLayout()->createBlock('hellothemesslideshow/adminhtml_hellothemesslideshow_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}