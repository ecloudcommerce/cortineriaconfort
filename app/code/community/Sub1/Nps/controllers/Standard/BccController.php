<?php

class Sub1_Nps_Standard_BccController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        die('Access Denied');
    }

    public function getFeesAction() {

        $ccid = (int) $this->getRequest()->getParam('cc_id', false);
        $bankid = (int) $this->getRequest()->getParam('bank_id', false);

        try {
            if ($ccid && $bankid) {
                $data = $this->_getStandard()->getPaymentsModel()->getPaymentsData(array(
                    'bank_id' => $bankid,
                    'cc_id' => $ccid,
                    'product_id' => null,
                        ));
                $data['status'] = 'ok';
            } else {
                throw new Exception(Mage::helper('nps')->__('No se recibieron los identificadores de banco y tarjeta.'));
            }
        } catch (Exception $e) {
            $data = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
        }
        $data['type'] = 'json';
        $this->_responseToJson($data);
    }

    public function logsAction() {
        if (!$this->_getStandard()->isDebug()) {
            $this->norouteAction();
            die('Access Denied');
        }

        $collection = Mage::getModel('nps/psp_log')->getCollection();
        if ($collection) {
            $html = '';
            foreach ($collection as $log) {

                $html .= '<div>';
                $html .= '   <table>';
                $html .= '       <tr><td>Type:</td><td>' . $log->getType() . '</td></tr>';
                $html .= '       <tr><td>Descripcion:</td><td>' . $log->getDescription() . '</td></tr>';
                $html .= '       <tr><td>Referencia:</td><td>' . $log->getReferenceId() . '</td></tr>';
                $html .= '       <tr><td>Orden:</td><td>' . $log->getOrderIncrementId() . '</td></tr>';
                $html .= '       <tr><td>Fecha:</td><td>' . $log->getDate() . '</td></tr>';
                $html .= '       <tr><td colspan="2">Detalles:</td></tr>';
                @$html .= '       <tr><td colspan="2"><pre>' . print_r(unserialize($log->getParameters()), true) . '</pre></td></tr>';
                $html .= '   </table>';
                $html .= '</div>';
            }
            echo $html;
        }
        // print_r($log->getData());
        die();
    }

    public function getOrderDataAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParams("order_id"));
        if ($order->getId()) {
            Zend_Debug::dump(unserialize($order->getNpsData()));
        }
        die("-------------------------------");
    }

    public function squeryAction() {
        $result = $this->_getStandard()->getTransactionResult();
        print_r($result);
        die();
    }

    /**
     *
     * @param <type> $data
     * @return Bl_Nps_Standard_CcController 
     */
    protected function _responseToJson($data) {
        $this->getResponse()
                ->setBody(Mage::helper('core')->jsonEncode($data));
        $this->loadLayout(false);
        $this->renderLayout();
    }

    /**
     *
     * @return Bl_Nps_Model_Standard
     */
    protected function _getStandard() {
        return Mage::getModel('nps/nps');
    }

    protected function _getSource() {
        return $this->_getStandard()->getPaymentsModel();
    }

    public function preDispatch() {
        parent::preDispatch();
        if ($this->_isDebugCall()) {
            echo '<pre style="border: 1px solid #CCC; background: #EEE; color: #444;">';
        }
    }

    protected function _isDebugCall() {
        return (bool) $this->getRequest()->getParam('debug', false);
    }

}