<?php
/**
 * @version   1.0 19.07.2013
 * @author    Hellothemes http://www.hellothemes.com <support@hellothemes.com>
 * @copyright Copyright (C) 2009 - 2013 Hellothemes
 */

class Hellothemes_Hellothemesslideshow_Block_Adminhtml_Hellothemesslideshow extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_hellothemesslideshow';
		$this->_blockGroup = 'hellothemesslideshow';
		$this->_headerText = Mage::helper('hellothemesslideshow')->__('Slides Manager');
		$this->_addButtonLabel = Mage::helper('hellothemesslideshow')->__('Add Slide');
		parent::__construct();
	}
}