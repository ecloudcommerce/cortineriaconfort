<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'descbco', 'int(8)');
$installer->getConnection()->addColumn($installer->getTable('salesrule'), 'tope', 'varchar(255)');

$installer->endSetup();