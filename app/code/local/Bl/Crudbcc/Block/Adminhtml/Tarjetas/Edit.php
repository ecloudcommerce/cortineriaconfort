<?php

/**
 * @autor W3ITSolutions
 */
class Bl_Crudbcc_Block_Adminhtml_Tarjetas_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'crudbcc';
        $this->_controller = 'adminhtml_tarjetas';

        $this->_updateButton('save', 'label', Mage::helper('crudbcc')->__('Salvar Item'));
        $this->_updateButton('delete', 'label', Mage::helper('crudbcc')->__('Borrar Item'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Salvar y continuar editando'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('crudbcc_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'crudbcc_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'crudbcc_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText() {
        if (Mage::registry('crudbcc_data') && Mage::registry('crudbcc_data')->getId()) {
            return Mage::helper('crudbcc')->__("Editando Tarjeta '%s'", $this->htmlEscape(Mage::registry('crudbcc_data')->getNombre()));
        } else {
            return Mage::helper('crudbcc')->__('Agregar Tarjeta');
        }
    }

}