<?php

class Bl_Promopsp_Block_Quote_Edit_Tab_Cuotas extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Return Tab Title
     *
     * @return string
     */
    public function getTabTitle(){
        return Mage::helper('promopsp')->__('Cuotas');
    }

    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Return Tab Label
     *
     * @return string
     */
    public function getTabLabel(){
        return Mage::helper('promopsp')->__('Cuotas');
    }

    public function canShowTab(){
        return true;
    }

    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Tab is Hidden
     *
     * @return boolean
     */
    public function isHidden(){
        return false;
    }

    /**
     * Defines after which tab this tab should come like you asked you need it below Manage Coupon codes
     *
     * @return string
     */
    public function getAfter(){
        return 'main_section';
    }

    public function _prepareForm(){
        
        $form = new Varien_Data_Form();

        $model = Mage::registry('current_promo_quote_rule');

        $form->setHtmlIdPrefix('rule_');

        $fieldset_cuotas = $form->addFieldset('cuotas_fieldset', array(
            'legend'=>Mage::helper('promopsp')->__('Cuotas')
        ));

        $fieldset_cuotas->addField('cuotas', 'text', array(
            'name'      => 'cuotas',
            'required'  => true,
            'label'     => Mage::helper('promopsp')->__('Cuotas'),
        ))->setRule($model)->setRenderer($this->getLayout()->createBlock('promopsp/quote_edit_tab_cuotas_tier'));
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}