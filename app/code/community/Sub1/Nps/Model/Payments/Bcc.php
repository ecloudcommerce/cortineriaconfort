<?php

//    implements Bl_Nps_Model_Payments_Interface
class Sub1_Nps_Model_Payments_Bcc extends Sub1_Nps_Model_Payments {

    protected $_sourceModel = 'promopsp/payments';

    /**
     *
     * @todo Revisar
     *
     * @param array $params
     */
    public function getFeeIndex(array $params) {
        return $params['fee']['coef'];
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function getFees(array $params) {
        $productId = (isset($params['product_id'])) ? $params['product_id'] : null;
        $fees = array(
            "data" => $this->getSource()->getFees($params['bank_id'], $params['cc_id'], $productId)
        );
        return $fees;
    }

    /**
     *
     * @return array
     */
    public function getCreditCardsOptions() {
        $creditCards = array();
        foreach ($this->getSource()->getCreditCards() as $cc) {
            $creditCards[] = array(
                'value' => $cc->getId(),
                'label' => $cc->getNombre()
            );
        }
        return $creditCards;
    }

//    public function getCcData($id)
//    {
//        $data = $this->getSource()->getCreditCardBy('id', $id);
//        return ($data) ? $data->getData() : array();
//    }

    public function getBanks() {
        return $this->_getSource()->getBanks();
    }

    public function getPaymentsData($params) {

        $data       = array();
        $hasAhora12 = false;
        $hasAhora18 = false;
        $productId  = (isset($params['product_id'])) ? $params['product_id'] : null;

        $fees       = $this->_getSource()->getFees($params["bank_id"], $params["cc_id"]);

        //Mage::log("fees en getPaymentsData Bcc: ".print_r($fees,true),null,"cuotas.log");

        if ($productId) {
            $this->createProductQuote($productId);
        }

        if (isset($fees) && is_array($fees)) {

            if (!count($fees) > 0) {
                return array(
                    "data" => array(
                        "0" => array(
                            "value" => "",
                            "label" => "No existen planes disponibles.",
                        )
                    )
                );
            }

            foreach ($fees as $fee) {

                if(isset($fee['rules'])){
                    // check if is promo day
                    $is_promo_day = $this->isPromoDay($fee['rules']['is_everyday'], $fee['rules']['weekdays']);
                }               

                $payments   = $fee['payments'];

                $coef       = $fee['coef'];
                $discount   = (isset($fee['rules']) && $is_promo_day) ? (double) $fee['rules']['discount'] : null;
                
                // Si el coeficiente es 1 y esta habilitado mostrar sin interes
                $labelInt  = ($coef == 1 && $fee['sin_interes'] == 1) ? $this->_helper()->__('s/interés ') : '';

                $feeAmount = $this->calculatePaymentAmount($payments, $coef, null, $discount);

                $discountAmount     = $this->calculateDiscountAmount($feeAmount, $discount);
                $discountedAmount   = $this->calculateDiscountAmount($this->_getGrandTotal(), $discount);

                $totalAmount    = $feeAmount - $discountAmount;
                $total          = Mage::helper("core")->formatCurrency($totalAmount, false, false);
                $final          = $totalAmount * $payments;

                // Bank refunds
                $reimbursement = null;
                $reimbursementMax = null;                

                if (isset($fee['promo']) && $is_promo_day) {
                    if (isset($fee['promo']['bank_discount'])) {
                        $reimbursement = $fee['promo']['bank_discount'];
                    }
                    if (isset($fee['promo']['max_refund'])) {
                        $reimbursementMax = $fee['promo']['max_refund'];
                    }
                }

                if (isset($fee['rules']) && $fee['rules']['action'] == 'by_percent') {
                    if ($discount > 0) {
                        $discountAmount = Mage::helper("core")->formatCurrency($discountAmount, false, false);
                        $discountedAmount = Mage::helper("core")->formatCurrency($discountedAmount, false, false);
                    }
                }

                // Extra info
                $info = array(
                    'fee' => $total,
                    'discount' => array('perc' => $discount, 'amount' => $discountedAmount),
                );

                if ($reimbursement > 0) {

                    $refund = ($totalAmount * $payments) * $reimbursement / 100;
                    $info['reimbursement']['maxedOut'] = 0;

                    if ($reimbursementMax != null && $reimbursementMax > 0 && $refund > $reimbursementMax) {
                        $refund = $reimbursementMax;
                        $info['reimbursement']['maxedOut'] = 1;
                    }

                    $final = ($totalAmount * $payments) - $refund;

                    $info['reimbursement']['perc'] = (double) $reimbursement;
                    $info['reimbursement']['amount'] = $refund;
                    $info['reimbursement']['amountFormatted'] = Mage::helper("core")->formatCurrency($refund, false, false);
                }

                $info['finalAmount'] = Mage::helper("core")->formatCurrency($final, false, false);

                // Select label
                $label = ($payments > 1) ? $this->_helper()->__('%s Cuotas %s de %s ', $payments, $labelInt, $total) : $this->_helper()->__('%s Cuota %s de %s ', $payments, $labelInt, $total);

                if($fee['id'] === "ahora12" ){                    
                    $label      = $this->_helper()->__('AHORA 12 - %s Cuotas de %s', $payments, $total);
                    $payments   = 7;     
                    $hasAhora12 = true;
                }

                if($fee['id'] === "ahora18" ){                    
                    $label      = $this->_helper()->__('AHORA 18 - %s Cuotas de %s', $payments, $total);
                    $payments   = 8;     
                    $hasAhora18 = true;
                }

                if($fee['nps_code'] == 'PlanZ'){
                    $label = Mage::helper('nps')->getTNLabel();
                }
                
                // Payment Info
                $data[$payments] = array(
                    'value'     => $payments,
                    'label'     => $label,
                    'payments'  => $payments,
                    'cft'       => $fee['cft'],
                    'tea'       => $fee['tea'],
                    'tna'       => $fee['tna'],
                    'tem'       => $fee['tem'],
                    'coef'      => $coef,
                    'info'      => $info,
                );

                // Promo Info
                /* Si la promo tiene descuento de magneto y NO es la cuota 1 */
                if ($discount){
                    $desc = $this->_helper()->__(' (%s%% de descuento)', (int)$fee['rules']['discount']);
                    $data[$payments]['label'] = $data[$payments]['label'] . $desc;
                }

                
                if (isset($fee['promo']) && $is_promo_day) {
                    /* Si la promo tiene descuento del banco (reintegro), 
                    * si esta habilitado el mostrar reintegro y si NO es la cuota 1
                    */
                    if (isset($fee['promo']['bank_discount'])
                            && $fee['promo']['bank_discount'] > 0 && $fee['promo']['show_refund'] == 1) {
                        $reint = $this->_helper()->__(' + (%s%% de reintegro)', $fee['promo']['bank_discount']);
                        $data[$payments]['label'] = $data[$payments]['label'] . $reint;
                    }
                    
                    $data[$payments]['promo'] = $fee['promo'];
                }
            }
            $result = array(
                "data" => $data,
                "message" => (isset($fees['message'])) ? $fees['message'] : '',
            );
        }

        //Mage::log("Cuotas en Bcc: ".print_r($data,true),null,"cuotas.log");

        if ($productId) {
            $this->_quoteObject = null;
        }
        if($hasAhora12){
            unset($result['data'][12]); 
        }
        if($hasAhora18){
            unset($result['data'][18]); 
        }

        return $result;
    }

    public function calculatePaymentAmount($payments, $ratio = 1, $total = null) {

        if (is_null($total)) {
            
            $total = ($total) ? $total : $this->_getGrandTotal();

            if ($ratio > 1) {
                $cTotals = $this->_getQuote()->getTotals();
                if (isset($cTotals['npsfee'])) {
                    $total = $total - $cTotals['npsfee']->getValue();
                }
            }
        }
        $amount = round($total * $ratio / $payments, 2);

        return $amount;
    }

    public function calculateDiscountAmount($total = 0, $discount = null) {
        if ($discount && $discount > 0) {
            $amount = $total * $discount / 100;
            return $amount;
        }
        return 0;
    }

    public function createProductQuote($productId) {

        $product = Mage::getModel('catalog/product')->load($productId);

        if ($product->getTypeId() == 'configurable') {
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
            if (is_array($childProducts)) {
                if (isset($childProducts[0]) && $childProducts[0]->getId()) {
                    $id = $childProducts[0]->getId();
                    $product = Mage::getModel('catalog/product')->load($id);
                }
            }
        }

        $quote = Mage::getModel('sales/quote');
        $quote->getShippingAddress()
                ->setCountryId('AR'); // Set your default shipping country here

        $product->getStockItem()->setUseConfigManageStock(false);
        $product->getStockItem()->setManageStock(false);

        $quote->addProduct($product);
        $this->_quoteObject = $quote;

        return $quote;
    }

    /**
     * Ultima Oportunidad de Guardar algun dato en el Quote Address
     *
     * @param array $data
     * @return array
     */
    public function preparePaymentFormData(array $data) {
        if (!isset($data['nps']['bank_id']) || $data['nps']['bank_id'] == '') {
            Mage::throwException("No se recibieron datos del Banco");
        }
        if (!isset($data['nps']['cc_id']) || $data['nps']['cc_id'] == '') {
            Mage::throwException("No se recibieron datos de la Tarjeta de Credito");
        }
        if (!isset($data['nps']['cc_fee']) || $data['nps']['cc_fee'] == '') {
            Mage::throwException("No se recibieron datos de Plan de Cuotas");
        }
        // Credit Card Info
//        $cc = $this->_getSource()->getCreditCardBy("id", $data['nps']['cc_id']);
        // Levantar la data de:
        $result = array_merge(
                $data['nps'], $this->_getSource()->getFee($data['nps']['bank_id'], $data['nps']['cc_id'], $data['nps']['cc_fee'])
        );
        return $result;
    }

    public function getChargeRatio() {
        $data = $this->_getPaymentMethodModel()->getCheckoutData();
        return $data['coef'];
    }

    protected function _getTotal() {
        $totals = $this->_getQuote()->getTotals();
    }

    private function isPromoDay($is_everyday, $weekdays) {
        if (!$is_everyday) {
            $days = explode(',', $weekdays);
            $current_day = date("w", Mage::getModel('core/date')->timestamp(time()));
            return in_array($current_day, $days);
        }
        return true;
    }

}
