<?php

/**
 * @author W3ITSolutions
 */
class Bl_Promo_Model_Config {

    const DATETIME_INPUT_FORMAT = 'yyyy-MM-dd HH:mm:ss';
    const DATETIME_LOCAL_FORMAT = 'dd/MM/yyyy HH:mm';
    const LOG_FILE = 'blpromo/logfile';

}
