<?php

/**
 *
 * @author Brandlive . Core Team
 * @copyright Brandlive
 * @package Bl_Nps
 *
 */
class Sub1_Nps_Model_Observer {

    protected $_isDebug;
    const AMEX_PRODUCT_CODE = '1';

    public function paymentImportDataBeforeEv($observer) {
        $data = $observer->getEvent()->getInput();
        if ($data["method"] == "nps") {
            // Guardando la data en el checkout
            Mage::getModel("nps/nps")->importData($data);
        }
        return;
    }

    /*
    * Guardo datos de bancos/tarjetas en el campo nps_data
    */
    public function setNpsDataOrderPlaceAfter($observer){
    	$order = $observer->getEvent()->getOrder();
    	$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
    	if($payment_method_code == 'nps'){
    		$npsData = $this->_getCheckout()->getNpsData();
    		/* 13255- Tarjeta AMEX */
            if ($npsData['nps_code'] == self::AMEX_PRODUCT_CODE && 
            	$npsData['payments'] == 7){                
                // Seteo las cuotas a 12 para amex con ahora12
                $npsData['payments'] = 12;
                $npsData['cc_fee'] = 12;            
            
            } elseif ($npsData['nps_code'] == self::AMEX_PRODUCT_CODE && 
                $npsData['payments'] == 8) {
                
                // Seteo las cuotas a 18 para amex con ahora18
                $npsData['payments'] = 18;
                $npsData['cc_fee'] = 18;
            }         
	    	
	    	$order->setData("nps_data",serialize($npsData));
	    	$order->save();
    	}
    }

    /**
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

}


