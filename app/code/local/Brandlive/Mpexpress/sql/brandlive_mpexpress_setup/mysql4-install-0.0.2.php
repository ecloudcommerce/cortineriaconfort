<?php 

/*
 *	Agrega un campo "Link MercadoPago" a la tabla orders 
 */

$installer = $this;
$installer->startSetup();

// Eliminar columnas 
//$installer->getConnection()->dropColumn($installer->getTable('sales/order'),'mp_payment_link');
//$installer->getConnection()->dropColumn($installer->getTable('sales/order'),'mp_email_sended');

// Agregar columna mp_payment_link
$installer->getConnection()
->addColumn($installer->getTable('sales/order'),'mp_payment_link', 
	array(
	    'type'              		=> Varien_Db_Ddl_Table::TYPE_TEXT,
	    'length'					=> 255,
	    'label'                		=> 'Link MercadoPago',
	    'comment'					=> 'Campo para almacenar link de pago de MercadoPago',
	    'global'            		=> 1,
	    'visible'           		=> 1,
	    'required'          		=> 0,
	    'user_defined'      		=> 1,
	    'searchable'        		=> 0,
	    'filterable'        		=> 0,
	    'comparable'        		=> 0,
	    'visible_on_front'  		=> 1,
	    'visible_in_advanced_search'=> 0,
	    'unique'            		=> 0,
	    'is_configurable'   		=> 0,
	    'position'          		=> 1
    )
); 

$installer->getConnection()
->addColumn($installer->getTable('sales/order'),'mp_email_sended', 
	array(
	    'type'              		=> Varien_Db_Ddl_Table::TYPE_SMALLINT,
	    'default'					=> 0,
	    'label'                		=> 'MercadoPago Email Sended for unpayment orders',
	    'comment'					=> 'Campo para verificar si fue enviado un mail al usuario',
	    'global'            		=> 1,
	    'visible'           		=> 1,
	    'required'          		=> 0,
	    'user_defined'      		=> 1,
	    'searchable'        		=> 0,
	    'filterable'        		=> 0,
	    'comparable'        		=> 0,
	    'visible_on_front'  		=> 1,
	    'visible_in_advanced_search'=> 0,
	    'unique'            		=> 0,
	    'is_configurable'   		=> 0,
	    'position'          		=> 1
    )
);

$installer->endSetup(); 
