var pixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;
jQuery(window).on("load", function() {
    //cucarda GRILLAS
    jQuery('.products-grid li.item, .products-list li.item').each(function(){
        jQuery(this).find('.cucarda-sale').remove();
        if( jQuery(this).find('.old-price .price-viejo').length >0 ){
            old_price  = parseFloat(jQuery(this).find('.old-price .price-viejo').text().replace('AR$','').replace(',','')); 
            sale_price = parseFloat(jQuery(this).find('.special-price .price').text().replace('AR$','').replace(',',''));
            
            if (old_price > sale_price) {
                discount  = Math.round((sale_price * 100)/old_price);
                discount  = 100 - discount;
                jQuery(this).find('.catalog-image .product-image').append('<div class="cucarda-sale">'+discount+'%</div>');
            }
        }
    })

    if (jQuery('body').hasClass('catalog-product-view')) {
        //cucarda PDP
        jQuery('.cucarda-sale').remove();
        if(jQuery('.product-shop .old-price .price-viejo').length>0){
            old_price  = parseFloat(jQuery('.product-shop .old-price .price-viejo').text().replace('AR$','').replace(',','')); 
            sale_price = parseFloat(jQuery('.product-shop .special-price .price').text().replace('AR$','').replace(',',''));

            if (old_price > sale_price) {
                discount  = Math.round((sale_price * 100)/old_price);
                discount  = 100 - discount;
                jQuery('.product-img-box').prepend('<div class="cucarda-sale">'+discount+'%</div>');
            }
        }
    }

    //ipad and iphone fix
    if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
        jQuery("#hellothemesNav li a").on({
            click: function () {
                if ( !jQuery(this).hasClass('touched') && jQuery(this).siblings('div') ) {
                    jQuery('#hellothemesNav a').removeClass('touched');
                    jQuery(this).parents('li').children('a').addClass('touched');
                    jQuery(this).addClass('touched');
                    return false;
                }
            }
        });
        jQuery("#nav li a").on({
            click: function () {
                if ( !jQuery(this).hasClass('touched') && jQuery(this).siblings('ul') ) {
                    jQuery('#nav a').removeClass('touched');
                    jQuery(this).parents('li').children('a').addClass('touched');
                    jQuery(this).addClass('touched');
                    return false;
                }
            }
        });
        jQuery('.header-switch, .toolbar-switch').on({
            click: function (e) {
                jQuery(this).addClass('over');
                return false;
            }
        });

    }
	
	jQuery("#store-content ul li").mouseenter(function() {
		jQuery('.hover-store', this).css('opacity' , '1');
	}).mouseleave(function() {
		jQuery('.hover-store', this).css('opacity' , '0');
	});

    jQuery(window).resize().scroll();
	if (jQuery('#tabss').length > 0) {
        jQuery('#tabs').tabs();
    };

	
     if (jQuery('body').hasClass('cms-home')) {
         jQuery('.cms-home .new-related .home-grid-products .products-grid').addClass('no-carrousel');    
     }

    // si queremos armar una lista de productos sin carrousel, usamos la clase no-carrousel
    jQuery('.products-grid, #color-content').not('.no-carrousel').jcarousel({
		horizontal: true,
        scroll: 1,
		itemFallbackDimension: 300,
	});

	jQuery('#color-content').jcarousel({
		horizontal: true,
        scroll: 1,
		wrap: 'circular',
		itemFallbackDimension: 300,
	})
	
	//Cart add/remove buttons
	jQuery("#cart_add").click(function(){
		cart_qty=parseInt(jQuery("#qty").val());
		jQuery("#qty").val(cart_qty+1);
		return false;
	});
	jQuery("#cart_remove").click(function(){
		cart_qty=parseInt(jQuery("#qty").val());
		if (cart_qty>0)
			jQuery("#qty").val(cart_qty-1);
		return false;
	});

	jQuery(".sticker-top-right").mouseenter(function() {
		 jQuery(this).children('.specialto').show();
	}).mouseleave(function() {
		jQuery(this).children('.specialto').hide();
	});
	
	
	jQuery(".products-grid li, #upsell-product-table li").each(function() {
		var view = jQuery(this).find(".quick-view span");
		jQuery(".catalog-image", this).hover(function() {
			view.css("display","block").animate({
				opacity: 1
			}, 200);
		}, function() {
			view.animate({
				opacity: 0
			}, 200, function() {
				jQuery(this).css("display","none");
			});
		});
		view.hide();
	});
	
});

jQuery.fn.extend({
    scrollToMe: function () {
        var x = jQuery(this).offset().top - 100;
        jQuery('html,body').animate({scrollTop: x}, 500);
    }
});

//actual width
;(function(a){a.fn.extend({actual:function(b,k){var c,d,h,g,f,j,e,i;if(!this[b]){throw'$.actual => The jQuery method "'+b+'" you called does not exist';}h=a.extend({absolute:false,clone:false,includeMargin:undefined},k);d=this;if(h.clone===true){e=function(){d=d.filter(":first").clone().css({position:"absolute",top:-1000}).appendTo("body");};i=function(){d.remove();};}else{e=function(){c=d.parents().andSelf().filter(":hidden");g=h.absolute===true?{position:"absolute",visibility:"hidden",display:"block"}:{visibility:"hidden",display:"block"};f=[];c.each(function(){var m={},l;for(l in g){m[l]=this.style[l];this.style[l]=g[l];}f.push(m);});};i=function(){c.each(function(m){var n=f[m],l;for(l in g){this.style[l]=n[l];}});};}e();j=/(outer)/g.test(b)?d[b](h.includeMargin):d[b]();i();return j;}});})(jQuery);

var sw, sh, scroll_critical,
    breakpoint = 959,
    mobile = false,
    resizeLimits = [0,479,767,959,1199,9999],
    _resizeLimit = {};

isResize = function(limitName){
    var current, w = jQuery(window).width();
    for( i=0; i<resizeLimits.length; i++ ){
        if (w > resizeLimits[i]) {
            current = i;
        } else {
            break;
        }
    }
    if ( _resizeLimit[limitName] === undefined || current != _resizeLimit[limitName] ) {
        _resizeLimit[limitName] = current;
        return true;
    }
    return false;
}

jQuery(function($){
    if (Hellothemes.totop) {
        $().UItoTop({scrollSpeed:400});
    }

    //.page-title-bg
    var page_title_height = '';
    if ( $('.main-container .page-title').length ) {
        var $p = $('<div class="page-title-bg" />').css({height: $('.main-container .page-title').height()+60 });
        $('.main-container').prepend($p);
        page_title_height = 'title';
    } else if ( $('.main-container .breadcrumbs:visible').length ) {
        var $p = $('<div class="page-title-bg" />').css({height: $('.main-container .breadcrumbs').height() });
        $('.main-container').prepend($p);
        page_title_height = 'breadcrumbs';
        if ( $('.product-category-title').length ) {
            $('.main-container .page-title-bg').css({height: $('.main-container .breadcrumbs').height() + $('.product-category-title').outerHeight() });
            page_title_height = 'breadcrumbs_category';
        }
    }

    $(window).resize(function(){

        if ( !isResize('page_title') ) return;

        if ( page_title_height == '' ) return;
        var h = 0;
        switch (page_title_height) {
            case 'title' :
                h = $('.main-container .page-title').height() + 60;
                break;
            case 'breadcrumbs' :
                h = $('.main-container .breadcrumbs').height();
                break;
            case 'breadcrumbs_category' :
                h = $('.main-container .breadcrumbs').height() + $('.product-category-title').outerHeight();
                break;
        }
        $('.main-container .page-title-bg').css({height: h });
    });

    $(window).resize(function(){
        sw = $(window).width();
        sh = $(window).height();
        mobile = (sw > breakpoint) ? false : true;
        if ( !Hellothemes.responsive ) {
            mobile = false;
        }

        //menu_transform
        if (!($("header").hasClass("fixed"))) $(".header-wrapper").height($("header").height());
        scroll_critical = parseInt($(".header-container").height());

        //header_transform();
        if ( !isResize('grid_header') ) return;
        fixGridHeight();
	
    });
	
	var hgt = $('.flex-viewport').height();
	$('.img-right-slide').height(hgt+'px');
	
	//cart dropdown
	var config = {
	     over: function(){
            if (mobile) return;
            $('.cart-top-container .details').animate({opacity:1, height:'toggle'}, 200);
        },
	     timeout: 200, // number = milliseconds delay before onMouseOut
	     out: function(){
            if (mobile) return;
            $('.cart-top-container .details').animate({opacity:0, height:'toggle'}, 200);
         }
	};
	$("div.cart-top-container").hoverIntent( config );

    $('#hellothemesNav li').hover(
        function(){
            $(this).addClass('over');
            var div = $(this).children('div');
            div.addClass('shown-sub');
            if ( div.actual('width') + $(this).offset().left > $(document).width()  ) {
                div.css('left', -($(this).offset().left + div.actual('width') + 5 - $(document).width())+'px' );
            } else {
                div.css('left', '0px');
            }
        },
        function(){
            $(this).removeClass('over');
            $(this).children('div').removeClass('shown-sub').css('left', '-10000px');
        }
    );

	//fix grid items height
    function fixGridHeight() {
        $('.products-grid').each(function(){
            var items_in_row = Math.floor($(this).width() / $('li.item', this).width());
            var height = [], row = 0;
            $('li.item', this).each(function(i,v){
                var h = $(this).height();
                if ( !height[row] ) {
                    height[row] = h;
                } else if ( height[row] && h > height[row] ) {
                    height[row] = h;
                }
                if ( (i+1)/items_in_row == 1 ) row++;

            });
            row = 0;
            $('li.item', this).each(function(i,v){
                $(this).height( height[row] );
                if ( (i+1)/items_in_row == 1 ) row++;
            });
        });
    }
    fixGridHeight();

    var config = {
        over: function(){
            if (mobile) return;
            if ($(this).hasClass('.toolbar-dropdown')){
                $(this).parent().addClass('over');
                $('.toolbar-dropdown').css({width: $(this).parent().width()+50});
            } else {
                $(this).addClass('over');
                $('.toolbar-dropdown', this).css({width: $(this).width()+50});
            }

            $('.toolbar-dropdown', this).animate({opacity:1, height:'toggle'}, 100);
        },
        timeout: 0, // number = milliseconds delay before onMouseOut
        out: function(){
            if (mobile) return;
            var that = this;
            $('.toolbar-dropdown', this).animate({opacity:0, height:'toggle'}, 100, function(){
                if ($(this).hasClass('.toolbar-dropdown')){
                    $(that).parent().removeClass('over');
                } else {
                    $(that).removeClass('over');
                }
            });
        }
    };
    $('.toolbar-switch, .toolbar-switch .toolbar-dropdown').hoverIntent( config );

    var config = {
        over: function(){
            $('.back_img', this).css('opacity',0).show().animate({opacity:1}, 200);
        },
        timeout: 100, // number = milliseconds delay before onMouseOut
        out: function(){
            $('.back_img', this).animate({opacity:0}, 200);
        }
    };
    $('.products-list .product-image').hoverIntent( config );

    $('.products-grid .item').live({
        mouseenter: function(){
            if (mobile) return;
            $('.hover .price-box', this).css({
                'opacity':0
            });
            if (Hellothemes.price_circle) {
                if ( !$(this).hasClass('calc-price-box') ) {
                    var padding = Math.floor( ($('.hover .price-box', this).actual('width') - $('.hover .price-box', this).actual('height'))/2 + 15 );
                    $('.hover .price-box', this).css({
                        'padding':padding+'px 15px',
                        'margin':(-(25+padding*2+$('.hover .price-box', this).actual('height')))+'px 0 0 0',
                    });
                    $(this).addClass('calc-price-box');
                }
                var that = this;
                $('.hover', this).show(0, function(){ $('.hover .price-box', that).animate({opacity:1}, 600) } );
            } else {
                $('.hover', this).show();
            }

            $(this).addClass('no-shadow');

        },
        mouseleave: function(){
            if (mobile) return;
            $('.hover', this).hide();
            $(this).removeClass('no-shadow');
        }
    });

    var $flexslider = $("#flexslider"),
        $flexslides = $flexslider.find('ul.slides').children('li');

    if ( $flexslider.length ) {
        $(window).load(function(){

            var timeline = {
                    width: 0,
                    interval: CONFIG_SLIDESHOW.slideshowSpeed
                },
                slideshowPause = false;

            vericalCenterSlideContent = function($slide) {
                var $content = $('div.content', $slide);
                var $contentH = $content.actual('height')
                    + parseInt( $content.css('marginTop') )
                    + parseInt( $content.css('marginBottom') );
                if ( $slide.height() > $contentH ) {
                    $content.css('marginTop', Math.floor( ($slide.height() - $contentH)/2 + 30 ) + 'px');
                }
            }

            setSlideHeight = function() {
                //update slides to include cloned li
                $flexslides = $("#flexslider").find('ul.slides').children('li');
                if (_resizeLimit['slideshow'] <= 1 && Hellothemes.responsive ) {
                    //iphone resolution ( <= 767 ). hide content and show small image
                    $('div.content', $flexslides).hide();
                    $('img.small_image', $flexslides).show();
                    var maxSlideHeight = null;
                    $flexslides.each(function(i,v){
                        if ( $('img.small_image', this).length ) {
                            $(this).css('background-image', 'none');
                            $(this).height($('img.small_image', this).height());
                            maxSlideHeight = Math.max(maxSlideHeight, $(this).height());
                        }
                    });
                    //auto height - by tallest slide
                    $flexslides.height(maxSlideHeight);
                } else {
                    $('img.small_image', $flexslides).hide();
                    $('div.content', $flexslides).show();
                    //restore original content margin top
                    $('div.content', $flexslides).css('marginTop', '30px');
                    //restore bg image
                    $flexslides.each(function(i,v){
                        $(this).css('background-image', $(this).attr('data-bg'));
                    });

                    if ( CONFIG_SLIDESHOW.height != 'auto' ) {
                        $flexslides.height(CONFIG_SLIDESHOW.height);
                    } else {
                        var maxSlideHeight = null;
                        //set slide height according to height of content and image
                        $flexslides.each(function(i,v){
                            var $imgH = $(this).attr('data-img-height');
                            //count content height
                            var $contentH = $('div.content', this).actual('height') + parseInt($('div.content', this).css('marginTop')) + parseInt($('div.content', this).css('marginBottom'));
                            $(this).height(Math.max($imgH, $contentH)+'px');
                            maxSlideHeight = Math.max(maxSlideHeight, $(this).height());
                        });

                        if ( CONFIG_SLIDESHOW.smoothHeight ) {
                            //smooth height
                        } else {
                            //auto height - by tallest slide
                            $flexslides.height(maxSlideHeight);
                        }
                    }
                    //adjust content vertical center
                    $flexslides.each(function(i,v){
                        vericalCenterSlideContent( $(this) );
                    });
                }
            }

            //backup original images for slides
            $flexslides.each(function(i,v){
                $(this).attr('data-bg', $(this).css('background-image'));
            });

            slideshowResize = function() {
                timeline.width = $flexslider.width();
                var resize = isResize('slideshow');
                if (resize || _resizeLimit['slideshow'] <= 1) {
                    setSlideHeight();
                }
            }
            slideshowResize();

            $(window).resize(function(){
                var interval = (timeline.width - $('#slide-timeline').width() ) / ( timeline.width / timeline.interval );
                runTimeline(interval);
                slideshowResize();
            });

            runTimeline = function( interval ) {
                if ( slideshowPause
                    || interval == 0
                    || CONFIG_SLIDESHOW.slideshow == false
                    || CONFIG_SLIDESHOW.timeline == false
                    || $flexslides.length < 2) {return;}
                $('#slide-timeline')
                    .show()
                    .animate(
                        {width: timeline.width + 'px'},
                        interval,
                        'linear',
                        function(){
                            $(this).hide().width(0);
                            $('#flexslider').flexslider("next");
                        }
                    );
            }

            $flexslider.on({
                mouseenter: function () {
                    slideshowPause = true;
                    $('#slide-timeline').stop(true);
                },
                mouseleave: function () {
                    slideshowPause = false;
                    var interval = (timeline.width - $('#slide-timeline').width() ) / ( timeline.width / timeline.interval );
                    runTimeline(interval);
                },
                touchstart: function () {
                    slideshowPause = true;
                    $('#slide-timeline').stop(true);
                },
                touchend: function () {
                    slideshowPause = false;
                    var interval = (timeline.width - $('#slide-timeline').width() ) / ( timeline.width / timeline.interval );
                    runTimeline(interval);
                }
            });

            var defaults = {
                slideshow: ( CONFIG_SLIDESHOW.slideshow && CONFIG_SLIDESHOW.timeline == false ? true : false),
                initDelay:200,
                start: function(slider){
                    //line up direction nav
                    if (CONFIG_SLIDESHOW.smoothHeight) {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('li.flex-active-slide', slider).height()/2 -40) );
                    } else {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('.flexslider').height()/2 -40) );
                    }
                    runTimeline(timeline.interval);
                },
                before: function(slider){
                    $('#slide-timeline').hide().width(0);
                    $('.flex-direction-nav a', slider).hide();
                },
                after: function(slider){
                    if (CONFIG_SLIDESHOW.smoothHeight) {
                        $('.flex-direction-nav a', slider).css('marginTop', (-$('li.flex-active-slide', slider).height()/2 -40));
                    }
                    $('.flex-direction-nav a', slider).show();
                    $('#slide-timeline').stop(true);
                    $('#slide-timeline').hide().width(0);
                    runTimeline(timeline.interval);
                }
            }
            vars = $.extend({}, CONFIG_SLIDESHOW, defaults);

            if ( $('.col-main .homepage-banners').length ) {
                $('.slider').animate({paddingBottom: '52px'}, 200);
            }
            if ( $('.col-main .home-right').length ) {
                $('.slider').animate({paddingBottom: '20px'}, 200);
            }

            $flexslider.flexslider(vars);
        });
    }

    if ( $(".block-slideshow .block-slider").length ) {
        $(".block-slideshow .block-slider")
            .flexslider({
                animation: "slide",
                slideshow: true,
                useCSS: false,
                animationLoop: true,
                mousewheel: false,
                smoothHeight: false,
                slideshowSpeed: 7000,
                animationSpeed: 600,
                pauseOnAction: true,
                pauseOnHover: true,
                controlNav: true,
                directionNav: false
            });
    }
    if ( $(".block-login .block-slider").length) {
        $(".block-login .block-slider")
            .flexslider({
                animation: "slide",
                slideshow: false,
                useCSS: false,
                animationLoop: false,
                smoothHeight: false,
                animationSpeed: 600,
                controlNav: false,
                directionNav: false
            });
        $('#forgot-password').click(function(){ $(".block-login .block-slider").flexslider("next"); return false; });
        $('#back-login').click(function(){ $(".block-login .block-slider").flexslider("prev"); return false; });
        if ( $('body').hasClass('customer-account-forgotpassword') ) {
            $('#forgot-password').click();
        }
    }

   $(window).load(function(){
        setTimeout(function(){ if ($('.col-left').length) $('.col-left').masonry({itemSelector : '.block', isResizable:true}); }, 600);
    });

    if ( Hellothemes.anystretch_bg != '' ) {
        jQuery('.main-container').anystretch( Hellothemes.anystretch_bg );
    }

    if ( $('body').hasClass('customer-account-login') || $('body').hasClass('customer-account-forgotpassword') ) {
        function positionFooter() {
            if (mobile) return;
            if (!$("#sticky-footer-push").length) {
                $(".footer-container").before('<div id="sticky-footer-push"></div>');
            }
            var docHeight = $(document.body).height() - $("#sticky-footer-push").height();
            if(docHeight < $(window).height()){
                var diff = $(window).height() - docHeight - 5;
                $("#sticky-footer-push").height(diff);
            }
        }
        $(window).scroll(positionFooter).resize(positionFooter).load(positionFooter);
    }
});

var $jQ = jQuery.noConflict();

$jQ(document).ready(function(){

	$jQ(".category span").click(function() {
		var open = $jQ(this).parent('.category').attr('lang');
		$jQ(".subcategory_" + open).slideToggle('medium');
		$jQ(".subcategory_" + open).parent().prev().toggleClass('openn');
	}); 
	$jQ("#left-nav li.category, #left-nav li.cate").mouseenter(function() {
		$jQ(this).addClass('over');
	}).mouseleave(function() {
		$jQ(this).removeClass('over');
	}); 
	
	$jQ('#left-nav .cate.active').parents('#left-nav').find('.category.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#999999'});
	$jQ('#left-nav .cat.active').parents('#left-nav').find('.category.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#999999'})
	$jQ('#left-nav .cat.active').parents('#left-nav').find('.cate.active a').css({'background': 'none repeat scroll 0 -2px transparent', 'color': '#666666'})
	
	$jQ('.header-switch span.current').click(function(){
		$jQ('.header-dropdown').slideToggle('medium');
	});

	$jQ('.cart-top-container span.open').click(function(){
		$jQ('.details').slideToggle('medium');
	});
	
	var select = $jQ('.currency-switch li.selected').html();
	$jQ('.selectSwitch2').append(select);
	$jQ(".selectSwitch2").click(function() {
		$jQ('.selectSwitch2').html('');
		var select = $jQ('.currency-switch li.selected').html();
		$jQ('.selectSwitch2').append(select);
		$jQ('.ulSelect2').slideToggle('medium');
	});
    if (jQuery(window).width() < 991){
        jQuery('#narrow-by-list dt').on('click',function(){
            jQuery(this).next('dd').find('ol').toggle();  
            if(!jQuery(this).hasClass('active')){
                jQuery(this).addClass('active');
            }else{
                jQuery(this).removeClass('active');
            }
        });
        jQuery('.block-title').on('click',function(){
            if(!jQuery('.block-content').hasClass('active')){
                jQuery('.block-content').addClass('active');
            }else{
                jQuery('.block-content').removeClass('active');
            }
        });
    }
});
