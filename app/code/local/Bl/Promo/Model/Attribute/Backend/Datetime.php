<?php

/**
 * @author w3itsolutions
 */
class Bl_Promo_Model_Attribute_Backend_Datetime extends Mage_Eav_Model_Entity_Attribute_Backend_Datetime {

    public function formatDate($date) {
        if (preg_match('#^\d{2}\/\d{2}\/\d{4}( \d{2}:\d{2})?$#', $date)) {
            $zendDate = new Zend_Date();
            $tmp = explode(' ', $date);
            $dateArr = split('/', $tmp[0]);
            $dateStr = $dateArr[2] . '-' . $dateArr[1] . '-' . $dateArr[0];
            $hourStr = isset($tmp[1]) ? $tmp[1] . ':00' : '00:00:00';
            $date = $zendDate->setIso($dateStr . ' ' . $hourStr);
            return $date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        }
        
        return parent::formatDate($date);
        
    }

}
